/*  Cambios por Japheth Calzada
 *  Se ingresa los campos de latitud y longitud, asi como los datos*/
ALTER TABLE cat_estado add  `latitud` varchar(15);
ALTER TABLE cat_estado add  `longitud` varchar(15);
/*Data for the table `cat_estado` */
update `cat_estado` set `latitud` = '21.8823395',`longitud` ='-102.2825928' where `idestado` = 1;
update `cat_estado` set `latitud` = '32.5027008',`longitud` ='-117.0037079' where `idestado` = 2;
update `cat_estado` set `latitud` = '24.1443691',`longitud` ='-110.300499'where `idestado` = 3;
update `cat_estado` set `latitud` = '19.8438606',`longitud` ='-90.5255432' where `idestado` = 4;
update `cat_estado` set `latitud` = '16.7597294',`longitud` ='-93.1130829' where `idestado` = 5;
update `cat_estado` set `latitud` = '28.6353',`longitud` ='-106.4608383' where `idestado` = 6;
update `cat_estado` set `latitud` = '25.4232101',`longitud` ='-101.0089024' where `idestado` = 7;
update `cat_estado` set `latitud` = '19.2433',`longitud` ='-103.725' where `idestado` = 8;
update `cat_estado` set `latitud` = '19.4284706',`longitud` ='-99.1276627' where `idestado` = 9;
update `cat_estado` set `latitud` = '24.0203209',`longitud` ='-104.6575623' where `idestado` = 10;
update `cat_estado` set `latitud` = '21.1290798',`longitud` ='-101.6737366' where `idestado` = 11;
update `cat_estado` set `latitud` = '16.8494205',`longitud` ='-99.9089127' where `idestado` = 12;
update `cat_estado` set `latitud` = '20.1169701',`longitud` ='-98.7332916' where `idestado` = 13;
update `cat_estado` set `latitud` = '20.6668205',`longitud` ='-103.3918228'where `idestado` = 14;
update `cat_estado` set `latitud` = '19.2878609',`longitud` ='-99.6532364' where `idestado` = 15;
update `cat_estado` set `latitud` = '19.7007809',`longitud` ='-101.184433' where `idestado` = 16;
update `cat_estado` set `latitud` = '18.9260998',`longitud` ='-99.230751' where `idestado` = 17;
update `cat_estado` set `latitud` = '21.50951',`longitud` ='-104.8956909'where `idestado` = 18;
update `cat_estado` set `latitud` = '25.6750698',`longitud` ='-100.3184662' where `idestado` = 19;
update `cat_estado` set `latitud` = '17.0654202',`longitud` ='-96.7236481' where `idestado` = 20;
update `cat_estado` set `latitud` = '19.0379295',`longitud` ='-98.2034607' where `idestado` = 21;
update `cat_estado` set `latitud` = '20.5880604',`longitud` ='-100.3880615' where `idestado` = 22;
update `cat_estado` set `latitud` = '21.1742897',`longitud` ='-86.8465576' where `idestado` = 23;
update `cat_estado` set `latitud` = '22.1891193',`longitud` ='-100.9379196' where `idestado` = 24;
update `cat_estado` set `latitud` = '23.2329006',`longitud` ='-106.4061966' where `idestado` = 25;
update `cat_estado` set `latitud` = '29.1026001',`longitud` ='-110.9773178' where `idestado` = 26;
update `cat_estado` set `latitud` = '17.9868908',`longitud` ='-92.9302826' where `idestado` = 27;
update `cat_estado` set `latitud` = '26.0806103',`longitud` ='-98.288353' where `idestado` = 28;
update `cat_estado` set `latitud` = '19.4167',`longitud` ='-98.2481' where `idestado` = 29;
update `cat_estado` set `latitud` = '19.1809502',`longitud` ='-96.1428986' where `idestado` = 30;
update `cat_estado` set `latitud` = '21.2125',`longitud` ='-87.7417' where `idestado` = 31;
update `cat_estado` set `latitud` = '22.7684300',`longitud` ='-102.5814100' where `idestado` = 32;
/*
 * Cambios tabla cuestionario
 */

ALTER TABLE `cuestionario` CHANGE COLUMN `fecha_registro` `fecha_hecho` DATE NOT NULL;
ALTER TABLE `cuestionario` MODIFY COLUMN `nota_sobre_medio_comunicacion` text;
ALTER TABLE `cuestionario` MODIFY COLUMN `comentario_medio_comunicacion` text;
ALTER TABLE `cuestionario`
	ADD COLUMN `porque_se_trato_adecuadamente` TEXT NULL DEFAULT NULL AFTER `trato_adecuado`;
ALTER TABLE `cuestionario` MODIFY COLUMN `tipo_arma` VARCHAR(50);
ALTER TABLE `cuestionario` MODIFY COLUMN `causa_muerte` VARCHAR(50);

/*
 * Cambios tabla cat_pais
 */

update cat_pais set idpais = 250 where idpais = 98;
update cat_pais set idpais = 251 where idpais = 99;

/*
 * Cambios tabla usuario
 */

ALTER TABLE usuario ADD COLUMN organizacion varchar(150) after fecha;


/*
 * Se agrega tabla noticias
 */
create table if not exists `noticia` (
  `idnoticia` MEDIUMINT(6) NOT NULL AUTO_INCREMENT,
  `idusuario_fk` smallint(6) NOT NULL,
  `idestado_fk` tinyint(4) NOT NULL,
  `titulo` varchar(150) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `descripcion` text COLLATE utf8_spanish2_ci,
  `fecha_publicacion` datetime NOT NULL,
  PRIMARY KEY (`idnoticia`),
  KEY `fk_NOTICIA_CAT_ESTADO1` (`idestado_fk`),
  KEY `fk_NOTICIA_USUARIO` (`idusuario_fk`),
  CONSTRAINT `fk_NOTICIA_CAT_ESTADO1` FOREIGN KEY (`idestado_fk`) REFERENCES `cat_estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NOTICIA_USUARIO` FOREIGN KEY (`idusuario_fk`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

create table if not exists evento(
    idevento mediumint unsigned not null primary key auto_increment,
    idestado_fk tinyint(4) not null,
    idusuario_fk smallint(6) not null,
    titulo varchar(50) not null,
    fecha_evento datetime not null,
    fecha_publicacion datetime not null,
    descripcion varchar(250) not null,
		constraint fk_EVENTO_ESTADO foreign key (idestado_fk) references cat_estado(idestado) on delete no action on update no action,
		constraint fk_EVENTO_USUARIO foreign key (idusuario_fk) references usuario(idusuario) on delete no action on update no action,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/* Tabla mes_grafica para generar el array que necesita la gráfica de estados
jcl
*/

CREATE TABLE `mes_grafica` (
  `mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Datos  mes_grafica */

insert  into `mes_grafica`(`mes`) values (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12);

/*se actualiza estados*/

update `cat_estado` set `latitud` = '28.6353',`longitud` ='-106.4608383' where `idestado` = 6;
update `cat_estado` set `latitud` = '31.5027008',`longitud` ='-116.0037079' where `idestado` = 2;
update `cat_estado` set `latitud` = '26.5443691',`longitud` ='-112.200499'where `idestado` = 3;

ALTER TABLE cuestionario MODIFY COLUMN pais_nacimiento char(3);


/*  SE CAMBIA EL ORDEN DE LAS COLUMNAS EN CUESTIONARIO */
ALTER TABLE cuestionario MODIFY interpuso_denuncia tinyint(4) NOT NULL DEFAULT '98' AFTER quien_hallo_victima;
ALTER TABLE cuestionario MODIFY quien_interpuso_denuncia tinyint(4) NOT NULL DEFAULT '98' AFTER interpuso_denuncia;
ALTER TABLE cuestionario MODIFY comentario_medio_comunicacion TEXT AFTER nota_sobre_medio_comunicacion;
ALTER TABLE cuestionario MODIFY nombre_archivo_comunicacion VARCHAR(249) AFTER divulgacion_noticia;



/**** INSERT  PARA LOS MOVIMIENTOS DE BITACORA ****/
INSERT INTO `cat_movimiento` (`idmovimiento`, `descripcion`)
VALUES
	(1, 'Nuevo usuario'),
	(2, 'Edita usuario'),
	(3, 'Elimina usuario'),

	(4, 'Nuevo cuestionario'),
	(5, 'Edita cuestionario'),
	(6, 'Elimina cuestionario'),

	(7, 'Nueva noticia'),
	(8, 'Edita noticia'),
	(9, 'Elimina noticia'),

	(10, 'Nuevo evento'),
	(11, 'Edita evento'),
	(12, 'Elimina evento'),
	(13, 'Acceso al sistema'),
  (14, 'Reenvio de token');


	ALTER TABLE `bitacora` ADD COLUMN `meta_data` TEXT NULL AFTER `fecha`;
	ALTER TABLE `bitacora` ADD COLUMN `ip` VARCHAR(20) NULL AFTER `meta_data`;
