-- phpMyAdmin SQL Dump
-- version 4.7.8
-- https://www.phpmyadmin.net/
--
-- Servidor: 10.123.0.64:3307
-- Tiempo de generación: 03-03-2019 a las 18:21:54
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.0.33-0+deb9u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gcareaga_faa`
--
CREATE DATABASE IF NOT EXISTS `gcareaga_faa` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `gcareaga_faa`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `idbitacora` smallint(6) NOT NULL,
  `idusuario_fk` smallint(6) NOT NULL,
  `idmovimiento_fk` smallint(6) NOT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_ciudad`
--

CREATE TABLE `cat_ciudad` (
  `idciudad` mediumint(9) NOT NULL,
  `idmunicipio_fk` smallint(6) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_estado`
--

CREATE TABLE `cat_estado` (
  `idestado` tinyint(4) NOT NULL,
  `estado` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `clave_estado` char(3) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

--
-- Volcado de datos para la tabla `cat_estado`
--

INSERT INTO `cat_estado` (`idestado`, `estado`, `clave_estado`) VALUES
(1, 'Aguascalientes', 'ag'),
(2, 'Baja California', 'bc'),
(3, 'Baja California Sur', 'bs'),
(4, 'Campeche', 'cm'),
(5, 'Chiapas', 'cs'),
(6, 'Chihuahua', 'ch'),
(7, 'Coahuila de Zaragoza', 'co'),
(8, 'Colima', 'cl'),
(9, 'Ciudad de México', 'cm'),
(10, 'Durango', 'dg'),
(11, 'Guanajuato', 'gj'),
(12, 'Guerrero', 'gr'),
(13, 'Hidalgo', 'hg'),
(14, 'Jalisco', 'ja'),
(15, 'México', 'mx'),
(16, 'Michoacán', 'mi'),
(17, 'Morelos', 'mo'),
(18, 'Nayarit', 'na'),
(19, 'Nuevo León', 'nl'),
(20, 'Oaxaca', 'oa'),
(21, 'Puebla', 'pu'),
(22, 'Querétaro', 'qt'),
(23, 'Quintana Roo', 'qr'),
(24, 'San Luis Potosí', 'sl'),
(25, 'Sinaloa', 'si'),
(26, 'Sonora', 'so'),
(27, 'Tabasco', 'tb'),
(28, 'Tamaulipas', 'tm'),
(29, 'Tlaxcala', 'tl'),
(30, 'Veracruz', 've'),
(31, 'Yucatán', 'yu'),
(32, 'Zacatecas', 'za');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_movimiento`
--

CREATE TABLE `cat_movimiento` (
  `idmovimiento` smallint(6) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_municipio`
--

CREATE TABLE `cat_municipio` (
  `idmunicipio` smallint(6) NOT NULL,
  `idestado_fk` tinyint(4) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_pais`
--

CREATE TABLE `cat_pais` (
  `idpais` smallint(4) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `clave_pais` char(3) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

--
-- Volcado de datos para la tabla `cat_pais`
--

INSERT INTO `cat_pais` (`idpais`, `descripcion`, `clave_pais`) VALUES
(1, 'Afganistán', 'AF'),
(2, 'Albania', 'AL'),
(3, 'Alemania', 'DE'),
(4, 'Andorra', 'AD'),
(5, 'Angola', 'AO'),
(6, 'Anguila', 'AI'),
(7, 'Antigua y Barbuda', 'AG'),
(8, 'Antártida', 'AQ'),
(9, 'Arabia Saudita', 'SA'),
(10, 'Argelia', 'DZ'),
(11, 'Argentina', 'AR'),
(12, 'Armenia', 'AM'),
(13, 'Aruba', 'AW'),
(14, 'Australia', 'AU'),
(15, 'Austria', 'AT'),
(16, 'Azerbaiyán', 'AZ'),
(17, 'Bahamas (las)', 'BS'),
(18, 'Bangladés', 'BD'),
(19, 'Barbados', 'BB'),
(20, 'Baréin', 'BH'),
(21, 'Belice', 'BZ'),
(22, 'Benín', 'BJ'),
(23, 'Bermudas', 'BM'),
(24, 'Bielorrusia', 'BY'),
(25, 'Bolivia, Estado Plurinacional de', 'BO'),
(26, 'Bonaire, San Eustaquio y Saba', 'BQ'),
(27, 'Bosnia y Herzegovina', 'BA'),
(28, 'Botsuana', 'BW'),
(29, 'Brasil', 'BR'),
(30, 'Brunéi Darussalam', 'BN'),
(31, 'Bulgaria', 'BG'),
(32, 'Burkina Faso', 'BF'),
(33, 'Burundi', 'BI'),
(34, 'Bután', 'BT'),
(35, 'Bélgica', 'BE'),
(36, 'Cabo Verde', 'CV'),
(37, 'Camboya', 'KH'),
(38, 'Camerún', 'CM'),
(39, 'Canadá', 'CA'),
(40, 'Catar', 'QA'),
(41, 'Chad', 'TD'),
(42, 'Chile', 'CL'),
(43, 'China', 'CN'),
(44, 'Chipre', 'CY'),
(45, 'Colombia', 'CO'),
(46, 'Comoras', 'KM'),
(47, 'Congo', 'CG'),
(48, 'Congo (la República Democrática del)', 'CD'),
(49, 'Corea (la República Democrática Popular de)', 'KP'),
(50, 'Corea (la República de)', 'KR'),
(51, 'Costa Rica', 'CR'),
(52, 'Cote d Ivoire', 'CI'),
(53, 'Croacia', 'HR'),
(54, 'Cuba', 'CU'),
(55, 'Curaçao', 'CW'),
(56, 'Dinamarca', 'DK'),
(57, 'Dominica', 'DM'),
(58, 'Ecuador', 'EC'),
(59, 'Egipto', 'EG'),
(60, 'El Salvador', 'SV'),
(61, 'Emiratos Árabes Unidos (Los)', 'AE'),
(62, 'Eritrea', 'ER'),
(63, 'Eslovaquia', 'SK'),
(64, 'Eslovenia', 'SI'),
(65, 'España', 'ES'),
(66, 'Estados Unidos (los)', 'US'),
(67, 'Estonia', 'EE'),
(68, 'Etiopía', 'ET'),
(69, 'Filipinas (las)', 'PH'),
(70, 'Finlandia', 'FI'),
(71, 'Fiyi', 'FJ'),
(72, 'Francia', 'FR'),
(73, 'Gabón', 'GA'),
(74, 'Gambia (La)', 'GM'),
(75, 'Georgia', 'GE'),
(76, 'Georgia del sur y las islas sandwich del sur', 'GS'),
(77, 'Ghana', 'GH'),
(78, 'Gibraltar', 'GI'),
(79, 'Granada', 'GD'),
(80, 'Grecia', 'GR'),
(81, 'Groenlandia', 'GL'),
(82, 'Guadalupe', 'GP'),
(83, 'Guam', 'GU'),
(84, 'Guatemala', 'GT'),
(85, 'Guayana Francesa', 'GF'),
(86, 'Guernsey', 'GG'),
(87, 'Guinea', 'GN'),
(88, 'Guinea Ecuatorial', 'GQ'),
(89, 'Guinea-Bisáu', 'GW'),
(90, 'Guyana', 'GY'),
(91, 'Haití', 'HT'),
(92, 'Honduras', 'HN'),
(93, 'Hong Kong', 'HK'),
(94, 'Hungría', 'HU'),
(95, 'India', 'IN'),
(96, 'Indonesia', 'ID'),
(97, 'Irak', 'IQ'),
(98, 'Irlanda', 'IE'),
(99, 'Irán (la República Islámica de)', 'IR'),
(100, 'Isla Bouvet', 'BV'),
(101, 'Isla Heard e Islas McDonald', 'HM'),
(102, 'Isla Norfolk', 'NF'),
(103, 'Isla de Man', 'IM'),
(104, 'Isla de Navidad', 'CX'),
(105, 'Islandia', 'IS'),
(106, 'Islas Caimán (las)', 'KY'),
(107, 'Islas Cocos (Keeling)', 'CC'),
(108, 'Islas Cook (las)', 'CK'),
(109, 'Islas Feroe (las)', 'FO'),
(110, 'Islas Malvinas [Falkland] (las)', 'FK'),
(111, 'Islas Marianas del Norte (las)', 'MP'),
(112, 'Islas Marshall (las)', 'MH'),
(113, 'Islas Salomón (las)', 'SB'),
(114, 'Islas Turcas y Caicos (las)', 'TC'),
(115, 'Islas Vírgenes (Británicas)', 'VG'),
(116, 'Islas Vírgenes (EE.UU.)', 'VI'),
(117, 'Islas de Ultramar Menores de Estados Unidos (las)', 'UM'),
(118, 'Islas Åland', 'AX'),
(119, 'Israel', 'IL'),
(120, 'Italia', 'IT'),
(121, 'Jamaica', 'JM'),
(122, 'Japón', 'JP'),
(123, 'Jersey', 'JE'),
(124, 'Jordania', 'JO'),
(125, 'Kazajistán', 'KZ'),
(126, 'Kenia', 'KE'),
(127, 'Kirguistán', 'KG'),
(128, 'Kiribati', 'KI'),
(129, 'Kuwait', 'KW'),
(130, 'Lao, (la) República Democrática Popular', 'LA'),
(131, 'Lesoto', 'LS'),
(132, 'Letonia', 'LV'),
(133, 'Liberia', 'LR'),
(134, 'Libia', 'LY'),
(135, 'Liechtenstein', 'LI'),
(136, 'Lituania', 'LT'),
(137, 'Luxemburgo', 'LU'),
(138, 'Líbano', 'LB'),
(139, 'Macao', 'MO'),
(140, 'Macedonia (la antigua República Yugoslava de)', 'MK'),
(141, 'Madagascar', 'MG'),
(142, 'Malasia', 'MY'),
(143, 'Malaui', 'MW'),
(144, 'Maldivas', 'MV'),
(145, 'Malta', 'MT'),
(146, 'Malí', 'ML'),
(147, 'Marruecos', 'MA'),
(148, 'Martinica', 'MQ'),
(149, 'Mauricio', 'MU'),
(150, 'Mauritania', 'MR'),
(151, 'Mayotte', 'YT'),
(152, 'Micronesia (los Estados Federados de)', 'FM'),
(153, 'Moldavia (la República de)', 'MD'),
(154, 'Mongolia', 'MN'),
(155, 'Montenegro', 'ME'),
(156, 'Montserrat', 'MS'),
(157, 'Mozambique', 'MZ'),
(158, 'Myanmar', 'MM'),
(159, 'México', 'MX'),
(160, 'Mónaco', 'MC'),
(161, 'Namibia', 'NA'),
(162, 'Nauru', 'NR'),
(163, 'Nepal', 'NP'),
(164, 'Nicaragua', 'NI'),
(165, 'Nigeria', 'NG'),
(166, 'Niue', 'NU'),
(167, 'Noruega', 'NO'),
(168, 'Nueva Caledonia', 'NC'),
(169, 'Nueva Zelanda', 'NZ'),
(170, 'Níger (el)', 'NE'),
(171, 'Omán', 'OM'),
(172, 'Pakistán', 'PK'),
(173, 'Palaos', 'PW'),
(174, 'Palestina, Estado de', 'PS'),
(175, 'Panamá', 'PA'),
(176, 'Papúa Nueva Guinea', 'PG'),
(177, 'Paraguay', 'PY'),
(178, 'Países Bajos (los)', 'NL'),
(179, 'Perú', 'PE'),
(180, 'Pitcairn', 'PN'),
(181, 'Polinesia Francesa', 'PF'),
(182, 'Polonia', 'PL'),
(183, 'Portugal', 'PT'),
(184, 'Puerto Rico', 'PR'),
(185, 'Reino Unido (el)', 'GB'),
(186, 'República Centroafricana (la)', 'CF'),
(187, 'República Checa (la)', 'CZ'),
(188, 'República Dominicana (la)', 'DO'),
(189, 'Reunión', 'RE'),
(190, 'Ruanda', 'RW'),
(191, 'Rumania', 'RO'),
(192, 'Rusia, (la) Federación de', 'RU'),
(193, 'Sahara Occidental', 'EH'),
(194, 'Samoa', 'WS'),
(195, 'Samoa Americana', 'AS'),
(196, 'San Bartolomé', 'BL'),
(197, 'San Cristóbal y Nieves', 'KN'),
(198, 'San Marino', 'SM'),
(199, 'San Martín (parte francesa)', 'MF'),
(200, 'San Pedro y Miquelón', 'PM'),
(201, 'San Vicente y las Granadinas', 'VC'),
(202, 'Santa Helena, Ascensión y Tristán de Acuña', 'SH'),
(203, 'Santa Lucía', 'LC'),
(204, 'Santa Sede[Estado de la Ciudad del Vaticano] (la)', 'VA'),
(205, 'Santo Tomé y Príncipe', 'ST'),
(206, 'Senegal', 'SN'),
(207, 'Serbia', 'RS'),
(208, 'Seychelles', 'SC'),
(209, 'Sierra Leona', 'SL'),
(210, 'Singapur', 'SG'),
(211, 'Sint Maarten (parte holandesa)', 'SX'),
(212, 'Siria, (la) República Árabe', 'SY'),
(213, 'Somalia', 'SO'),
(214, 'Sri Lanka', 'LK'),
(215, 'Suazilandia', 'SZ'),
(216, 'Sudáfrica', 'ZA'),
(217, 'Sudán (el)', 'SD'),
(218, 'Sudán del Sur', 'SS'),
(219, 'Suecia', 'SE'),
(220, 'Suiza', 'CH'),
(221, 'Surinam', 'SR'),
(222, 'Svalbard y Jan Mayen', 'SJ'),
(223, 'Tailandia', 'TH'),
(224, 'Taiwán (Provincia de China)', 'TW'),
(225, 'Tanzania, República Unida de', 'TZ'),
(226, 'Tayikistán', 'TJ'),
(227, 'Territorio Británico del Océano Índico (el)', 'IO'),
(228, 'Territorios Australes Franceses (los)', 'TF'),
(229, 'Timor-Leste', 'TL'),
(230, 'Togo', 'TG'),
(231, 'Tokelau', 'TK'),
(232, 'Tonga', 'TO'),
(233, 'Trinidad y Tobago', 'TT'),
(234, 'Turkmenistán', 'TM'),
(235, 'Turquía', 'TR'),
(236, 'Tuvalu', 'TV'),
(237, 'Túnez', 'TN'),
(238, 'Ucrania', 'UA'),
(239, 'Uganda', 'UG'),
(240, 'Uruguay', 'UY'),
(241, 'Uzbekistán', 'UZ'),
(242, 'Vanuatu', 'VU'),
(243, 'Venezuela, República Bolivariana de', 'VE'),
(244, 'Viet Nam', 'VN'),
(245, 'Wallis y Futuna', 'WF'),
(246, 'Yemen', 'YE'),
(247, 'Yibuti', 'DJ'),
(248, 'Zambia', 'ZM'),
(249, 'Zimbabue', 'ZW');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_perfil`
--

CREATE TABLE `cat_perfil` (
  `idperfil` tinyint(4) NOT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

--
-- Volcado de datos para la tabla `cat_perfil`
--

INSERT INTO `cat_perfil` (`idperfil`, `descripcion`) VALUES
(1, 'Administrador'),
(2, 'Informante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionario`
--

CREATE TABLE `cuestionario` (
  `idcuestionario` int(11) NOT NULL,
  `idusuario_fk` smallint(6) NOT NULL,
  `fecha_registro` date NOT NULL,
  `nombre_social` varchar(249) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_legal` varchar(249) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `sexo` tinyint(1) NOT NULL,
  `edad` tinyint(2) NOT NULL,
  `rango_edad` tinyint(4) NOT NULL DEFAULT '98',
  `orientacion_sexual` tinyint(4) NOT NULL DEFAULT '98',
  `identidad_genero` tinyint(4) NOT NULL DEFAULT '98',
  `origen_etnico` tinyint(4) NOT NULL DEFAULT '98',
  `estado_civil` tinyint(4) NOT NULL DEFAULT '98',
  `numero_hijos` tinyint(4) NOT NULL DEFAULT '98',
  `nacionalidad_mexicana` tinyint(4) NOT NULL DEFAULT '98',
  `localidad_nacimiento` tinyint(4) NOT NULL DEFAULT '98',
  `municipio_nacimiento` varchar(249) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `pais_nacimiento` tinyint(4) NOT NULL DEFAULT '98',
  `situacion_migratoria` tinyint(4) NOT NULL DEFAULT '98',
  `estado_residencia` tinyint(4) NOT NULL DEFAULT '98',
  `nivel_estudios` tinyint(4) NOT NULL DEFAULT '98',
  `actividad_laboral` tinyint(4) NOT NULL DEFAULT '98',
  `trabajo_de_defensa` tinyint(4) NOT NULL DEFAULT '98',
  `trabajo_de_periodismo` tinyint(4) NOT NULL DEFAULT '98',
  `pertenece_a_organizacion` tinyint(4) NOT NULL DEFAULT '98',
  `espacio_lgbt` tinyint(4) NOT NULL DEFAULT '98',
  `localidad_hecho` tinyint(4) NOT NULL DEFAULT '98',
  `municipio_hecho` varchar(249) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `tipo_crimen` tinyint(4) NOT NULL DEFAULT '98',
  `relacion_victima_agresor` tinyint(4) NOT NULL DEFAULT '98',
  `utilizo_arma` tinyint(4) NOT NULL DEFAULT '98',
  `tipo_arma` tinyint(4) NOT NULL DEFAULT '98',
  `causa_muerte` tinyint(4) NOT NULL DEFAULT '98',
  `presencia_agresiones` tinyint(4) NOT NULL DEFAULT '98',
  `lugar_sucedio_hecho` tinyint(4) NOT NULL DEFAULT '98',
  `victima_acompanada` tinyint(4) NOT NULL DEFAULT '98',
  `lugar_cuerpo_hallado` tinyint(4) NOT NULL DEFAULT '98',
  `quien_hallo_victima` tinyint(4) NOT NULL DEFAULT '98',
  `numero_denuncia` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `persona_detenida` tinyint(4) NOT NULL DEFAULT '98',
  `interpuso_denuncia` tinyint(4) NOT NULL DEFAULT '98',
  `quien_interpuso_denuncia` tinyint(4) NOT NULL DEFAULT '98',
  `situacion_del_caso` tinyint(4) NOT NULL DEFAULT '98',
  `resultado_proceso` tinyint(4) NOT NULL DEFAULT '98',
  `abandono_proceso` tinyint(4) NOT NULL DEFAULT '98',
  `medio_comunicacion` tinyint(4) NOT NULL DEFAULT '98',
  `nota_sobre_medio_comunicacion` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_archivo_comunicacion` varchar(249) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comentario_medio_comunicacion` varchar(249) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `trato_adecuado` tinyint(4) NOT NULL DEFAULT '98',
  `divulgacion_noticia` tinyint(4) NOT NULL DEFAULT '98',
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

--
-- Volcado de datos para la tabla `cuestionario`
--

INSERT INTO `cuestionario` (`idcuestionario`, `idusuario_fk`, `fecha_registro`, `nombre_social`, `nombre_legal`, `sexo`, `edad`, `rango_edad`, `orientacion_sexual`, `identidad_genero`, `origen_etnico`, `estado_civil`, `numero_hijos`, `nacionalidad_mexicana`, `localidad_nacimiento`, `municipio_nacimiento`, `pais_nacimiento`, `situacion_migratoria`, `estado_residencia`, `nivel_estudios`, `actividad_laboral`, `trabajo_de_defensa`, `trabajo_de_periodismo`, `pertenece_a_organizacion`, `espacio_lgbt`, `localidad_hecho`, `municipio_hecho`, `tipo_crimen`, `relacion_victima_agresor`, `utilizo_arma`, `tipo_arma`, `causa_muerte`, `presencia_agresiones`, `lugar_sucedio_hecho`, `victima_acompanada`, `lugar_cuerpo_hallado`, `quien_hallo_victima`, `numero_denuncia`, `persona_detenida`, `interpuso_denuncia`, `quien_interpuso_denuncia`, `situacion_del_caso`, `resultado_proceso`, `abandono_proceso`, `medio_comunicacion`, `nota_sobre_medio_comunicacion`, `nombre_archivo_comunicacion`, `comentario_medio_comunicacion`, `trato_adecuado`, `divulgacion_noticia`, `fecha_creacion`) VALUES
(19, 95, '2019-02-28', 'María Torres Serrano', 'Maquinarias Inc.', 2, 36, 7, 3, 99, 5, 4, 3, 2, 5, 'TUXTLA', 98, 2, 5, 2, 7, 2, 1, 2, 2, 12, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 4, 'DFP/38485-98', 2, 1, 4, 1, 3, 2, 2, '', '', '', 2, 2, '2019-02-28 15:58:57'),
(20, 98, '2019-02-28', 'Eliseo Garza Salinas', 'Eliseo Garza Salinas', 1, 0, 13, 4, 1, 2, 2, 99, 1, 19, 'MONTERREY', 98, 98, 19, 8, 4, 2, 2, 2, 2, 19, 'MONTERREY', 1, 3, 1, 5, 4, 99, 4, 1, 3, 98, '', 98, 98, 98, 98, 98, 98, 98, '', '', '', 98, 99, '2019-02-28 21:19:51'),
(21, 111, '2019-02-28', '', 'Carlos Gabriel Guerrero Bolaños', 1, 0, 1, 99, 1, 99, 1, 1, 98, 98, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 21, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 98, '', 98, 98, 98, 98, 98, 98, 98, '', '', '', 98, 99, '2019-02-28 21:16:54'),
(22, 107, '2019-02-28', '', 'Filiberto García', 98, 35, 7, 2, 1, 1, 1, 1, 1, 6, 'CIUDAD JUáREZ', 98, 1, 6, 99, 99, 2, 2, 2, 2, 6, 'CIUDAD JUáREZ', 1, 3, 2, 98, 4, 1, 5, 1, 6, 99, '', 98, 1, 3, 3, 1, 98, 1, '', '', 'En Ciudad Juárez, Chihuahua, un par de hombres mataron a otro que les había pedido sexo a cambio de dinero.  Al momento de su detención los hombres lucían divertidos, bromearon con los reporteros y llamaron a su víctima “jotito”.', 2, 99, '2019-02-28 22:27:17'),
(23, 112, '2019-02-28', 'Jessica Patricia González Tovar', '', 2, 21, 3, 3, 2, 1, 5, 99, 1, 7, 'MONCLOVA', 98, 1, 7, 99, 4, 99, 99, 99, 99, 7, 'MONCLOVA', 1, 99, 1, 1, 2, 2, 3, 2, 6, 4, '', 2, 1, 2, 99, 99, 98, 1, '', '', 'Hubo notas subsecuentes sobre el proceso penal.', 1, 99, '2019-02-28 21:39:09'),
(24, 108, '2019-02-28', 'Alessa Flores', 'Alessa Flores', 2, 27, 4, 1, 3, 1, 98, 98, 98, 98, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 9, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 98, '', 98, 98, 98, 98, 98, 98, 98, '', '', '', 98, 99, '2019-02-28 21:39:50'),
(25, 106, '2019-02-28', 'Manuel Luna', 'Manuel JR.', 1, 31, 6, 2, 1, 1, 1, 1, 98, 98, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 16, '', 98, 98, 98, 98, 98, 98, 98, 98, 98, 98, '', 98, 98, 98, 98, 98, 98, 98, '', '', '', 98, 99, '2019-02-28 21:40:14'),
(26, 113, '2019-02-28', 'ADRIAN', 'ADRIAN LOPEZ TOVAR', 1, 52, 10, 2, 1, 98, 1, 98, 1, 7, 'SALTILLO', 98, 98, 7, 5, 11, 2, 2, 2, 2, 7, 'SALTILLO', 1, 3, 1, 2, 6, 1, 1, 1, 2, 6, '', 2, 98, 2, 99, 6, 10, 1, '', '', 'Tras el deceso del estilista de la colonia Satélite Sur, autoridades ministeriales confirmaron que se trató de un homicidio, ya que el cadáver presentaba cuatro puñaladas en diferentes partes del cuerpo.', 1, 99, '2019-02-28 21:48:50'),
(27, 104, '2019-02-28', '', 'marbella ibarra', 2, 0, 8, 3, 2, 1, 99, 99, 1, 12, 'ACAPULCO', 98, 98, 2, 7, 5, 2, 2, 2, 2, 2, 'ROSARITO', 1, 99, 1, 99, 98, 1, 99, 99, 6, 8, '', 2, 1, 2, 1, 98, 98, 1, '', '', '', 98, 99, '2019-02-28 22:14:05'),
(28, 114, '2019-02-28', '', 'Marbella Ibarra', 2, 0, 8, 3, 2, 1, 99, 1, 1, 12, 'ACAPULCO', 98, 1, 2, 7, 5, 99, 99, 99, 99, 2, 'ROSARITO', 1, 99, 1, 99, 99, 1, 98, 98, 6, 98, '', 98, 98, 98, 98, 98, 98, 98, '', '', '', 98, 99, '2019-02-28 22:16:13'),
(29, 115, '2019-02-28', 'Nashly', 'Jesús Manuel Delgado', 1, 22, 3, 1, 3, 1, 1, 1, 1, 6, 'ALDAMA', 98, 98, 6, 99, 99, 2, 2, 2, 2, 6, 'ALDAMA', 1, 99, 1, 1, 2, 1, 3, 1, 1, 99, '', 2, 1, 1, 1, 98, 98, 1, '', '', '', 2, 99, '2019-02-28 22:23:26'),
(30, 105, '2019-02-28', 'Alaska Contreras bout', 'Jose Luis Contreras Ponce', 2, 25, 98, 1, 3, 3, 4, 1, 1, 30, 'MARTINEZ DE LA TORRE', 98, 98, 30, 98, 11, 98, 98, 98, 2, 30, 'MARTINEZ DE LA TORRE', 1, 6, 98, 2, 3, 1, 5, 1, 1, 6, '', 2, 1, 8, 1, 98, 98, 1, '', '', '', 1, 99, '2019-03-01 04:39:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionario_otro`
--

CREATE TABLE `cuestionario_otro` (
  `idcuestionario_fk` int(11) NOT NULL,
  `nombre_campo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

--
-- Volcado de datos para la tabla `cuestionario_otro`
--

INSERT INTO `cuestionario_otro` (`idcuestionario_fk`, `nombre_campo`, `descripcion`) VALUES
(19, 'origen_etnico', 'AfroAmericana'),
(19, 'actividad_laboral', 'Contador'),
(20, 'tipo_arma', 'Lapidación y Estrangulamiento'),
(23, 'estado_civil', 'Noviazgo'),
(27, 'actividad_laboral', 'ministerio publico y entrenadora de futbol'),
(28, 'actividad_laboral', 'Ministerio publico y en tiempo libre funbolista y entrenadora'),
(22, 'lugar_sucedio_hecho', 'Casa del agresor'),
(30, 'lugar_sucedio_hecho', 'Orillas del rio bobos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` smallint(6) NOT NULL,
  `idperfil_fk` tinyint(4) NOT NULL,
  `idestado_fk` tinyint(4) NOT NULL,
  `usuario` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidopat` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidomat` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('activo','pendiente','eliminado') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date NOT NULL,
  `token_password` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci TABLESPACE `gcareaga_faa`;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `idperfil_fk`, `idestado_fk`, `usuario`, `password`, `nombre`, `apellidopat`, `apellidomat`, `estatus`, `email`, `fecha`, `token_password`) VALUES
(1, 1, 31, 'angelica', '$2y$10$JpPQns2FX9pW1PHHbJ2Sv.yata9EsJytWzLFcq6yLf.pwLqiHEmPG', 'Angelica', 'Perez', 'Serrano', 'activo', 'perezosa790@gmail.com', '2019-02-17', '$2y$10$tyQNCIsUkXP0lyJOl.cCEeqfBYcRF8m0G5yfiF4EzhDYlbYHOKnyG'),
(2, 1, 5, 'davidmtz', '$2y$10$UMFEnYxgOkpAoWwUHQSP8ehqYRL.1rhwgL.QsREL0Q9b0GwXXFAMa', 'David de Jesús', 'Martínezz', 'Mejía', 'activo', 'dave_revolution@hotmail.com', '2019-02-19', NULL),
(95, 2, 12, 'angelicapese1', '$2y$10$YMp6EelPE7wT7.Y1fv92OuKNeLEI.O7pbF9D/6bW8r3R2mzn8CaCm', 'Zaira', 'Aceves', 'Parra', 'activo', 'angelicapese@yahoo.com.mx', '2019-02-28', NULL),
(96, 2, 9, 'patriaj', NULL, '', '', '', 'pendiente', 'patriaj@hotmail.com', '2019-02-28', '$2y$10$Ll4QIq5qx.PlL2TEBBHtweE7TtP33gKUCvTa1IM6Tm7GBt9el986y'),
(97, 2, 19, 'gessac', '$2y$10$RthOP3yTLEXpDNBzyDVrcuARKHYGuQCYIuxEbMmVYjIczXjtAcWjy', 'Mariaurora', 'Mota', 'Bravo', 'activo', 'gessac@gmail.com', '2019-02-28', NULL),
(98, 2, 19, 'mario', '$2y$10$1ORKHAadywkDr/jMxcIMJeCmymaFFoqyFg8XKt1ePkZbCBxaKBHam', 'Mario', 'González', 'Martínez', 'activo', 'mario@explorat.org.mx', '2019-02-28', NULL),
(99, 2, 14, 'teto89', '$2y$10$c1Xq.Q5i/gmFwvDjBsOQseaNIZsiiYkAO4SFXP104uzduvEiryNL2', 'Hector Gabriel', 'Ramirez ', 'Betancourt', 'activo', 'teto89@hotmail.com', '2019-02-28', NULL),
(100, 2, 14, 'luis.guzman', '$2y$10$aEX6PW91BzgATSgq43hxdeBfEI6jFsxjEyFHQp6ENSdkOe7gdchOS', 'Luis', 'Guzmán', 'García', 'activo', 'luis.guzman@codise.org.mx', '2019-02-28', NULL),
(101, 2, 12, 'terelarumbe', '$2y$10$j5gpWEy8FZ9JPYYAWsiFN.exSmlg4GZgygnSs60ZvsSDOgNVm3W/.', 'Teresa ', 'Molina', 'Larumbe', 'activo', 'teresalarumbe53@outlook.com', '2019-02-28', NULL),
(102, 2, 9, 'david91', '$2y$10$SAzemtkdrveymARIRd3X1.CKU9sfQXU4.s86IeyVmx8c4wZH5.CGG', 'Jesus', 'Martínez', 'Mejía', 'activo', 'davymtz91@gmail.com', '2019-02-28', NULL),
(103, 2, 12, 'Gabyss', '$2y$10$BYFviAvKbI7KNhcfFJfbcOooGbwdpkPt2vjmulu.IPiPzuTqZ2sUu', 'Gabyss', 'Sarabia', 'Soberanis', 'activo', 'gabyssmx@hotmail.com', '2019-02-28', NULL),
(104, 2, 2, 'clausulalopez', '$2y$10$sAE0SThA3YLD3d/HBWnCruKQJrRC2LSplTYWMt1CqQwVnKPmK0EoC', 'claudia ', 'lopez', 'sanz', 'activo', 'clausulalopez@gmail.com', '2019-02-28', NULL),
(105, 2, 30, 'luisgp', '$2y$10$VRJGyrb5KzrYOKMbSwHyk.iXMgpqH652FyjRu7YAMzaN3tJaA9i06', 'Geovani ', 'Perez', 'Chico', 'activo', 'luisgp@hotmail.com', '2019-02-28', NULL),
(106, 2, 16, 'jazm17011980', '$2y$10$4O1Yq/psgizJNSHagohWAek9IIPscii/uAcwPRwiiSnl/Q1ZzAOlC', 'José Antonio', 'Zavala', 'Meza', 'activo', 'jazm17011980@gmail.com', '2019-02-28', NULL),
(107, 2, 6, 'jcarlosmedel', '$2y$10$cEyaLhLo5McZWFJuypNF6.P3JbH.aBGPfCNP9Qnfx82jPDXKD8EEm', 'Juan Carlos', 'Medel ', 'Cabrera', 'activo', 'jcarlosmedel@hotmail.com', '2019-02-28', NULL),
(108, 2, 9, 'natalia.lane', '$2y$10$3QyBZ4GDr87jVcJdhKykw.iZTOcpNGW1Rf8SASvc6UhWIyJSW0gf.', 'Centro de Apoyo ', 'a las Identidades', 'Trans A.C. ', 'activo', 'natalia.lane@hotmail.com', '2019-02-28', NULL),
(109, 2, 16, 'Raùl Martìnez', '$2y$10$.JjTYWMXmjkqVF2x.eMyiu9phj5ZD57R3kXl026xdJzyAnHRTHyCq', 'Mauro Raùl', 'Martìnez', 'Rojas', 'activo', 'mrmr_781@hotmail.com', '2019-02-28', NULL),
(110, 2, 30, 'jazzamor88', '$2y$10$CcKR6IVHMjl7XXD4NgBLTODNZ05rr6e4Eg7SimCD3.qnrpTIQesai', 'jazz ', 'Bustamante ', 'Hernàndez ', 'activo', 'jazzamor88@gmail.com', '2019-02-28', NULL),
(111, 2, 21, 'vidaplena', '$2y$10$rr3.G0bGx1jVaKqCkX5hTOd/7OSmMOwklCRBMrLgD2KkqHsvTHfyy', 'Alma Laura ', 'del Castillo ', 'Cerrillos', 'activo', 'mandy.delcast@gmail.com', '2019-02-28', NULL),
(112, 2, 7, 'morquechode', '$2y$10$679xPj/x5MCL0knZ5NZYDOEgpZbk5v2dlCIceTlw3.a1VYckTmSi2', 'Edwin Ariel', 'Morquecho ', 'Ramirez', 'activo', 'morquechode@gmail.com', '2019-02-28', NULL),
(113, 2, 7, 'sanaelredoac', '$2y$10$KKzNMu6Oq8rtVedCCyinF.VTG3mpxc2GxJz0CRxJn2eT9lliwPtbO', 'NOE LEONARDO', 'RUIZ', 'MALACARA', 'activo', 'sanaelredoac@gmail.com', '2019-02-28', NULL),
(114, 2, 2, 'SaraíCano', '$2y$10$7nvhHMjG5q9Uk74fSOoz9OWy9U785gNox9M4TrxRnVZv5pMU.gcoS', 'Saraí ', 'Cano', 'Sáenz', 'activo', 'antropofagia83@gmail.com', '2019-02-28', NULL),
(115, 2, 6, 'direccion', '$2y$10$76UMFTKF71W6pp6reJ1j/.fN3ol1887CQmHaElAOYdgrHi911/NAu', 'Sara Raquel', 'Quiralte', 'Quiralte', 'activo', 'direccion@fatimaibp.org', '2019-02-28', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`idbitacora`),
  ADD KEY `fk_BITACORA_USUARIO` (`idusuario_fk`),
  ADD KEY `fk_BITACORA_CAT_MOVIMIENTO` (`idmovimiento_fk`);

--
-- Indices de la tabla `cat_ciudad`
--
ALTER TABLE `cat_ciudad`
  ADD PRIMARY KEY (`idciudad`),
  ADD KEY `fk_CAT_CIUDAD_CAT_MUNICIPIO` (`idmunicipio_fk`);

--
-- Indices de la tabla `cat_estado`
--
ALTER TABLE `cat_estado`
  ADD PRIMARY KEY (`idestado`);

--
-- Indices de la tabla `cat_movimiento`
--
ALTER TABLE `cat_movimiento`
  ADD PRIMARY KEY (`idmovimiento`);

--
-- Indices de la tabla `cat_municipio`
--
ALTER TABLE `cat_municipio`
  ADD PRIMARY KEY (`idmunicipio`),
  ADD KEY `fk_CAT_MUNICIPIO_CAT_ESTADO` (`idestado_fk`);

--
-- Indices de la tabla `cat_pais`
--
ALTER TABLE `cat_pais`
  ADD PRIMARY KEY (`idpais`);

--
-- Indices de la tabla `cat_perfil`
--
ALTER TABLE `cat_perfil`
  ADD PRIMARY KEY (`idperfil`);

--
-- Indices de la tabla `cuestionario`
--
ALTER TABLE `cuestionario`
  ADD PRIMARY KEY (`idcuestionario`),
  ADD KEY `fk_CUESTIONARIO_USUARIO` (`idusuario_fk`);

--
-- Indices de la tabla `cuestionario_otro`
--
ALTER TABLE `cuestionario_otro`
  ADD KEY `fk_CUESTIONARIO` (`idcuestionario_fk`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `fk_USUARIO_CAT_PERFIL` (`idperfil_fk`),
  ADD KEY `fk_USUARIO_CAT_ESTADO1` (`idestado_fk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `idbitacora` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cat_ciudad`
--
ALTER TABLE `cat_ciudad`
  MODIFY `idciudad` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cat_estado`
--
ALTER TABLE `cat_estado`
  MODIFY `idestado` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `cat_movimiento`
--
ALTER TABLE `cat_movimiento`
  MODIFY `idmovimiento` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cat_municipio`
--
ALTER TABLE `cat_municipio`
  MODIFY `idmunicipio` smallint(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cat_pais`
--
ALTER TABLE `cat_pais`
  MODIFY `idpais` smallint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT de la tabla `cat_perfil`
--
ALTER TABLE `cat_perfil`
  MODIFY `idperfil` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cuestionario`
--
ALTER TABLE `cuestionario`
  MODIFY `idcuestionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD CONSTRAINT `fk_BITACORA_CAT_MOVIMIENTO` FOREIGN KEY (`idmovimiento_fk`) REFERENCES `cat_movimiento` (`idmovimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_BITACORA_USUARIO` FOREIGN KEY (`idusuario_fk`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cat_ciudad`
--
ALTER TABLE `cat_ciudad`
  ADD CONSTRAINT `fk_CAT_CIUDAD_CAT_MUNICIPIO` FOREIGN KEY (`idmunicipio_fk`) REFERENCES `cat_municipio` (`idmunicipio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cat_municipio`
--
ALTER TABLE `cat_municipio`
  ADD CONSTRAINT `fk_CAT_MUNICIPIO_CAT_ESTADO` FOREIGN KEY (`idestado_fk`) REFERENCES `cat_estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuestionario`
--
ALTER TABLE `cuestionario`
  ADD CONSTRAINT `fk_CUESTIONARIO_USUARIO` FOREIGN KEY (`idusuario_fk`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_USUARIO_CAT_ESTADO1` FOREIGN KEY (`idestado_fk`) REFERENCES `cat_estado` (`idestado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_USUARIO_CAT_PERFIL` FOREIGN KEY (`idperfil_fk`) REFERENCES `cat_perfil` (`idperfil`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
