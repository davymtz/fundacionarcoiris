<?php

	/*ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);*/


    include_once("lib/xlswriter/xlsxwriter.class.php");

	$newArrayCuestionario  = array();
	$layItemOtros          = array('orientacion_sexual','identidad_genero','origen_etnico','estado_civil','nivel_estudios'
	                              ,'actividad_laboral','relacion_victima_agresor','tipo_arma','lugar_sucedio_hecho'
											          ,'lugar_cuerpo_hallado','quien_hallo_victima');

			foreach( $layCuestionario as $idCuestionario => $info)
			{
					unset( $info['idcuestionario'], $info['idusuario_fk'], $info['fecha_creacion'], $info['nombre_archivo_comunicacion'] ); //Eliminamos algunos elementos
					foreach( $info as $keys => $info2 ){
							$newArrayCuestionario[ $idCuestionario ][$keys] = $info2;
							if(in_array($keys, $layItemOtros) ){
								  $newArrayCuestionario[ $idCuestionario ]["otros_".$keys] = $layOtrosCuestionarios[ $idCuestionario ][ $keys ];
							}
					}
			}

$writer = new XLSXWriter();
$writer->setAuthor('Reporte Fundación Arcoiris');

$styles1 = array( 'font'=>'Arial','font-size'=>12,'font-style'=>'bold', 'color' => '#000', 'fill'=>'#fabf8f', 'halign'=>'center', 'border'=>'left,right,top,bottom');
$styles2 = array( 'font'=>'Arial','font-size'=>11,'color' => '#fff',  'fill'=>'#60497a', 'wrap_text' => true,'halign'=> 'center' ,'valign'=> 'center' , 																										    'border'=>'left,right,top,bottom');
$styles3 = array( 'font'=>'Arial','font-size'=>9,'border'=>'left,right,top,bottom', 'wrap_text' => true,'halign'=> 'center' ,'valign'=> 'center' );

$stylesC = array( 'font'=>'Arial','font-size'=>18, 'font-style'=>'bold', 'color' => '#000');
$stylesC2 = array( 'font'=>'Arial','font-size'=>14, 'font-style'=>'bold', 'color' => '#000');

$col_width = array('20','30','20','10','10','15','20','20','20','20',
                    '20','20','20','20','20','30','30','20','20','20',
                     '45','40','40','20','45','20','45','40','40','40',
                     '40','40','40','40','40','40','40','40','40','40',
                     '40','40','40','40','40','40','40','40','40','40',
                     '40','40','40','40','40','40','40','40','40','40');



$header   = array("" => "string");
$cabecera  = array('','','','Listado de Observatorios Fundación Arcoiris');
$cabecera2 = array('','','','','Registros de Enero a Diciembre del año '.$anioFiltro);

$layRows1 = array(     'Datos personales de la víctima','','','','','','','','','','','','',''
		            ,  'Datos demográficos de la víctima','','','','','','','','','','','','',''
		            ,  'Características del hecho','','','','','','','','','','','','','',''
		            ,  'Seguimiento al caso','','','','','','','','','','','','','');


$layRows2 = array('Fecha Registro'
	                , 'Nombre Social'
					, 'Nombre Legal'
					, 'Sexo'
					, 'Edad'
					, 'Rango edad'
					, 'Orientación sexual'
					, 'Otro'
					, 'Identidad género'
					, 'Otro'
					, 'Origen étnico'
					, 'Otro'
					, 'Estado civil'
					, 'Otro'
					, 'Num. hijos de la víctima'
					
					
					, 'Víctima nacionalidad mexicana'
					, 'Estado dónde nació'
					, 'Municipo'
					, 'País nacimiento'
					, 'Situación migratoria en México'
					, 'Estado donde reside o residía la víctima'
					, 'Nivel de estudios de la víctima'
					, 'Otro tipo de educación'
					, 'Actividad laboral principal a la que se dedica/dedicaba la víctima'
					, 'Otra actividad laboral'
					, '¿Trabajos relacionados con acciones de defensa o promoción de DH?'
					, '¿La víctima realiza o realizaba trabajos relacionados con periodismo?'
					, '¿La víctima pertenecía o colaboraba con alguna organización relacionada con la visibilización y defensa de los derechos de la comunidad LGBTI?'
					, '¿La víctima estaba relacionada laboralmente con algún espacio de convivencia dirigido a la comunidad LGBTI (antro, bar, sauna, club de encuentros, etc.)?'
					
					
					
					, 'Localidad dónde sucedió el hecho'
					, 'Municipio'
					, 'Tipo de crimen'
					, 'Relación de la víctima con el agresor'
					, 'Otra relación'
					, '¿Se utilizó algún tipo de arma en el crimen?'
					, 'Tipo de arma utilizada en el hecho'
					, 'Otro tipo de arma'
					, 'Causa de la muerte:'
					, 'Presencia de violencia y/o agresiones sexuales'
					, 'Lugar donde sucedió el hecho'
					, 'Otro lugar dónde sucedió el hecho'
					, '¿La víctima estaba sola o acompañada?'
					, '¿En dónde fue hallada la persona o el cuerpo? '
					, 'Otro lugar dónde hallaron la persona o al cuerpo'
					
					
					, '¿Quién halló a la víctima?'
					, 'Otro, quién halló a la víctima'
					, '¿Se interpuso denuncia formal?'
					, '¿Quién interpuso la denuncia formal?'
					, 'Número de investigación (averiguación previa o carpeta de investigación)'
					, '¿Hay alguna persona detenida?'
					
					
					, '¿Cuál es la situación del caso, en el marco de la denuncia formal?'
					, '¿Cuál fue el resultado del proceso?'
					, '¿Por qué decidió la víctima abandonar el proceso?'
					, '¿La noticia de la agresión apareció en algún medio de comunicación?'
					, 'Anote la información referente a la nota'
					, 'Comentario sobre la nota'
					, '¿Considera que la nota trató adecuadamente el caso?'
					, '¿Por qué?'
					, 'Debido a que la noticia de la agresión no apareció en algún medio de comunicación, ¿Cómo se divulgó la noticia?');

$sheet1 = 'Reporte Agresiones';
$sheet2 = 'Códigos';
$sheet3 = 'Códigos - Países';
$sheet4 = 'Códigos - Estados';

	$writer->writeSheetHeader( $sheet1 , $header, $col_options = ['widths'=>$col_width] );
	$writer->writeSheetRow(    $sheet1 , array(), array() );
	$writer->writeSheetRow(    $sheet1 , $cabecera, $stylesC );
	$writer->writeSheetRow(    $sheet1 , $cabecera2, $stylesC2 );
	$writer->writeSheetRow(    $sheet1 , array(), array() );
	$writer->writeSheetRow(    $sheet1 , array(), array() );
	$writer->markMergedCell(   $sheet1 , $start_row=6, $start_col=0, $end_row=6, $end_col=14);
	$writer->markMergedCell(   $sheet1 , $start_row=6, $start_col=15, $end_row=6, $end_col=28);
	$writer->markMergedCell(   $sheet1 , $start_row=6, $start_col=29, $end_row=6, $end_col=43);
	$writer->markMergedCell(   $sheet1 , $start_row=6, $start_col=44, $end_row=6, $end_col=58	);
	$writer->writeSheetRow(    $sheet1 , $layRows1, $styles1 );
	$writer->writeSheetRow(    $sheet1 , $layRows2, $styles2 );

	foreach($newArrayCuestionario as $idC => $infoC)
	{
		$writer->writeSheetRow($sheet1, $infoC, $styles3);
	}

////========================================
////====================SEGUNDA PESTAÑA
////========================================
$col_width11 = array('10','45','10','60','10');
$styles11    = array( 'font'=>'Arial','font-size'=>12,'font-style'=>'bold', 'color' => '#000', 'fill'=>'#fabf8f', 'halign'=>'center', 'border'=>'left,right,top,bottom');
$styles22    = array( 'font'=>'Arial','font-size'=>11,'color' => '#fff',  'fill'=>'#60497a', 'wrap_text' => true,'halign'=> 'center' ,'valign'=> 'center' , 																			    'border'=>'left,right,top,bottom');
$styles33    = array( 'font'=>'Arial','font-size'=>9,'border'=>'left,right,top,bottom', 'wrap_text' => true,'halign'=> 'left' ,'valign'=> 'center' );

$layRows11   = array(  '','LISTADO DE EQUIVALENCIAS DE CÓDIGOS', '','' );
$layRows22   = array(     '', 'Parámetro','Valor','Equivalencia','' );
$layRows33   = 
	array(
			array( '','','98','No se encuentra especificado / sin información' ),
			array( '','','99','No fue posible identificarla'),
			array( '','','5','Otro'),
			array( '','','',''),
			array( '','','',''),
			array( '','Sexo','1','Hombre'),
			array( '','','2','Mujer'),
			array( '','','',''),
			array( '','Rango Edad','1','5 - 9'),
			array( '','','2','10 - 14'),
			array( '','','3','15 - 19'),
			array( '','','4','20 - 24'),
			array( '','','6','25 - 29'),
			array( '','','7','30 - 34'),
			array( '','','8','35 - 39'),
			array( '','','9','40 - 44'),
			array( '','','10','45 - 49'),
			array( '','','11','50 - 54'),
			array( '','','12','55 - 59'),
			array( '','','13','60 - 64'),
			array( '','','14','65 - 69'),
			array( '','','15','70 - 74'),
			array( '','','16','75- 79'),
			array( '','','',''),
			array( '','Orientación Sexual','1','Heterosexual'),
			array( '','','2','Homosexual/ Gay'),
			array( '','','3','Lesbiana'),
			array( '','','4','Bisexual'),
			array( '','','',''),
			array( '','Identidad de Género','1','Hombre'),
			array( '','','2','Mujer'),
			array( '','','3','Mujer Trans.'),
			array( '','','4','Hombre Trans.'),
			array( '','','',''),
			array( '','Origen Étnico','1','Mestiza'),
			array( '','','2','Blanca'),
			array( '','','3','Indígena'),
			array( '','','4','Mulata'),
			array( '','','6','Afrodescendiente'),
			array( '','','',''),
			array( '','Estado civil','1','Soltera(o)'),
			array( '','','2','Casada(o)'),
			array( '','','3','Divorciada(o)'),
			array( '','','4','Unión libre / concubinato'),
			array( '','','',''),
			array( '','Número de hijos de la víctima','1','Ninguno'),
			array( '','','2','Uno'),
			array( '','','3','Dos'),
			array( '','','4','Tres'),
			array( '','','6','Cuatro o más'),
			
			
			array( '','','',''),		
			array( '','La víctima era de nacionalidad mexicana','1','Sí, era mexicano o mexicana de nacimiento'),
			array( '','','2','Sí, era una persona extranjera nacionalizada mexicana'), 
			array( '','','3','No, era una persona extranjera sin nacionalidad mexicana'),
			array( '','','',''),
			array( '','Situación migratoria en México','1','Visa humanitaria'),
			array( '','','2','En proceso de refugio'),
			array( '','','3','Refugiados'),
			array( '','','4','Residente'),
			array( '','','6','Regular'),
			array( '','','7','Irregular'),
			array( '','','',''),
			array( '','Nivel de estudios de la víctima	1	No fue a la escuela'),
			array( '','','2','Primaria'),
			array( '','','3','Secundaria'),
			array( '','','4','Preparatoria o bachillerato'),
			array( '','','6','Carrera técnica o comercial'),
			array( '','','7','Licenciatura'),
			array( '','','8','Maestría o Doctorado'),
			array( '','','',''),
			array( '','Actividad laboral principal a la que se dedica/dedicaba la víctima','1','Ninguna'),
			array( '','','2','Hogar'),
			array( '','','3','Estudiante'),
			array( '','','4','Empleado Privado o Público'),
			array( '','','6','Docente'),
			array( '','','7','Profesionista Independiente'),
			array( '','','8','Autoempleo'),
			array( '','','9','Trabajo sexual'),
			array( '','','10','Actividades artísticas'),
			array( '','','11','Estética y belleza'),
			array( '','','',''),
			array( '','La víctima realiza o realizaba trabajos relacionados con acciones de defensa o promoción de DH','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','La víctima realiza o realizaba trabajos relacionados con periodismo','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','La víctima pertenecía o colaboraba con alguna organización relacionada con la visibilización y defensa de los derechos de la comunidad LGBTI','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','La víctima estaba relacionada laboralmente con algún espacio de convivencia dirigido a la comunidad LGBTI (antro, bar, sauna, club de encuentros, etc.)','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			
			
			
			array( '','Tipo de crimen','1','Asesinato'),
			array( '','','2','Desaparición'),
			array( '','','',''),
			array( '','Relación de la víctima con el agresor','1','Pareja'),
			array( '','','2','Cliente'),
			array( '','','3','Conocido'),
			array( '','','4','Amistad'),
			array( '','','6','Desconocidos'),
			array( '','','',''),
			array( '','Se utilizó algún tipo de arma en el crimen','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','Tipo de arma utilizada en el hecho','1','Arma de fuego'),
			array( '','','2','Arma punzo cortante'),
			array( '','','3','Lapidación'),
			array( '','','4','Objeto constrictor'),
			array( '','','6','Mecanismos asfixiante'),
			array( '','','',''),
			array( '','Causa de la muerte','1','Golpiza'),
			array( '','','2','Impactos de bala'),
			array( '','','3','Tortura'),
			array( '','','4','Asfixia'),
			array( '','','6','Arma blanca'),
			array( '','','7','Violaciones/Agresiones Sexuales'),
			array( '','','8','Atropellamiento'),
			array( '','','',''),
			array( '','Presencia de violencia y/o agresiones sexuales','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','Lugar donde sucedió el hecho','1','Casa de la víctima (donde vive)'),
			array( '','','2','Casa de algún conocido o familiar'),
			array( '','','3','Calle'),
			array( '','','4','Lugar de trabajo'),
			array( '','','6','Hotel'),
			array( '','','7','Lugar de encuentros'),
			array( '','','8','Antro/bar'),
			array( '','','',''),
			array( '','La víctima estaba sola o acompañada','1','Sola'),
			array( '','','2','Acompañada'),
			array( '','','',''),
			array( '','En dónde fue hallada la persona o el cuerpo','1','Terreno baldío'),
			array( '','','2','Su casa'),
			array( '','','3','Su trabajo'),
			array( '','','4','Hotel '),
			array( '','','6','Calle'),
			array( '','','7','Bar'),
			array( '','','',''),
			
			
			
			array( '','Quién halló a la víctima','1','Madre/padre'),
			array( '','','2','Familiar'),
			array( '','','3','Amig@'),
			array( '','','4','Novi@'),
			array( '','','6','Vecin@'),
			array( '','','7','Desconocido'),
			array( '','','8','Cuerpo policial'),
			array( '','','9','Espos@'),
			array( '','','',''),
			array( '','Se interpuso denuncia formal de la desaparición','1','Si'),
			array( '','','2','No'),
			array( '','','3','Está en trámite'),
			array( '','','',''),
			array( '','Quién interpuso la denuncia formal','1','Madre/padre de la víctima'),
			array( '','','2','Familiar de la víctima'),
			array( '','','3','Amig@ de la víctima'),
			array( '','','4','Novi@ de la víctima'),
			array( '','','6','Espos@ de la víctima'),
			array( '','','7','Vecin@ de la víctima'),
			array( '','','8','Desconocido'),
			array( '','','',''),
			array( '','Hay alguna persona detenida','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','Cuál es la situación del caso, en el marco de la denuncia formal?','1','En proceso'),
			array( '','','2','Abandonado'),
			array( '','','3','Concluido'),
			array( '','','',''),
			array( '','Cuál fue el resultado del proceso?','1','Existió una resolución a favor de la víctima'),
			array( '','','2','Existió una resolución desfavorable para la víctima '),
			array( '','','3','Se identificó que no existían suficiente pruebas para emitir una resolución'),
			array( '','','4','El caso se clasificó como cerrado por la autoridad'),
			array( '','','6','El caso no procedió '),
			array( '','','',''),
			array( '','Por qué decidió la víctima abandonar el proceso','1','La víctima fue discriminada'),
			array( '','','2','El proceso era complicado'),
			array( '','','3','El proceso duraba demasiado tiempo'),
			array( '','','4','La víctima no tenía recursos económicos / el proceso era muy costoso'),
			array( '','','6','La víctima no fue apoya de manera legal por el Estado'),
			array( '','','7','El caso se clasificó como cerrado por la autoridad'),
			array( '','','8','La víctima prefirió cerrar el caso'),
			array( '','','9','Amenazas'),
			array( '','','10','El caso no procedió'),
			array( '','','',''),
			array( '','La noticia de la agresión apareció en algún medio de comunicación','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','Considera que la nota trató adecuadamente el caso','1','Si'),
			array( '','','2','No'),
			array( '','','',''),
			array( '','Debido a que la noticia de la agresión no apareció en algún medio de comunicación, ¿Cómo se divulgó la noticia?','1','Testimonio del agredido'),
			array( '','','2','Redes Sociales'),
			array( '','','3','Información de calle/ comunicación vecinal'),
			array( '','','4','Comunicación de activistas')
					);


$writer->writeSheetHeader( $sheet2 , $header, $col_options = ['widths'=>$col_width11]   );
$writer->markMergedCell(   $sheet2 , $start_row=1, $start_col=1, $end_row=1, $end_col=4 );
$writer->writeSheetRow(    $sheet2 , $layRows11,$styles11 );
$writer->writeSheetRow(    $sheet2 , $layRows22,$styles22 );
foreach( $layRows33 as $row )
{
	$writer->writeSheetRow(    $sheet2 , $row, $styles33 );
}	

////========================================
////====================TERCERA PESTAÑA
////========================================
$col_width111 = array('10','15', '45','10');
$styles111    = array( 'font'=>'Arial','font-size'=>12,'font-style'=>'bold', 'color' => '#000', 'fill'=>'#fabf8f', 'halign'=>'center', 'border'=>'left,right,top,bottom');
$styles222    = array( 'font'=>'Arial','font-size'=>11,'color' => '#fff',  'fill'=>'#60497a', 'wrap_text' => true,'halign'=> 'center' ,'valign'=> 'center' , 																			    'border'=>'left,right,top,bottom');
$styles333    = array( 'font'=>'Arial','font-size'=>9,'border'=>'left,right,top,bottom', 'wrap_text' => true,'halign'=> 'left' ,'valign'=> 'center' );

$layRows111   = array(  '','LISTADO DE EQUIVALENCIAS DE CÓDIGOS PAISES', '','' );
$layRows222   = array(  '', 'Código País','Descripción País','' );
$writer->writeSheetHeader( $sheet3 , $header, $col_options = ['widths'=>$col_width111]   );
$writer->markMergedCell(   $sheet3 , $start_row=1, $start_col=1, $end_row=1, $end_col=3 );
$writer->writeSheetRow(    $sheet3 , $layRows111,$styles111 );
$writer->writeSheetRow(    $sheet3 , $layRows222,$styles222 );
foreach($paises as $idPais => $descripcion)
{
	
	$writer->writeSheetRow($sheet3, array('',$idPais, $descripcion), $styles333);
	if($idPais == 97) $writer->writeSheetRow($sheet3, array('',98, 'No se encuentra especificado / sin información'), $styles333);
}

////========================================
////====================CUARTA PESTAÑA
////========================================
$col_width1111 = array('10','15', '45','10');
$styles1111    = array( 'font'=>'Arial','font-size'=>12,'font-style'=>'bold', 'color' => '#000', 'fill'=>'#fabf8f', 'halign'=>'center', 'border'=>'left,right,top,bottom');
$styles2222    = array( 'font'=>'Arial','font-size'=>11,'color' => '#fff',  'fill'=>'#60497a', 'wrap_text' => true,'halign'=> 'center' ,'valign'=> 'center' , 																			    'border'=>'left,right,top,bottom');
$styles3333    = array( 'font'=>'Arial','font-size'=>9,'border'=>'left,right,top,bottom', 'wrap_text' => true,'halign'=> 'left' ,'valign'=> 'center' );

$layRows1111   = array(  '','LISTADO DE EQUIVALENCIAS DE CÓDIGOS ESTADOS', '','' );
$layRows2222   = array(  '', 'Código Estado','Descripción Estado','' );
$writer->writeSheetHeader( $sheet4 , $header, $col_options = ['widths'=>$col_width1111]   );
$writer->markMergedCell(   $sheet4 , $start_row=1, $start_col=1, $end_row=1, $end_col=3 );
$writer->writeSheetRow(    $sheet4 , $layRows1111,$styles1111 );
$writer->writeSheetRow(    $sheet4 , $layRows2222,$styles2222 );
foreach($estados as $idEstado => $descripcion)
{
	$writer->writeSheetRow($sheet4, array('',$idEstado, $descripcion), $styles3333);
}
$writer->writeSheetRow($sheet4, array('',98, 'No se encuentra especificado / sin información'), $styles3333);



	//die();

	$filename = "Reporte_Agresiones.xlsx";
	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');


  $writer->writeToStdOut();
  
exit(0);
