/** pines  */
var pinOrange	=  'assets/img/marcadores/naranja.png'
var pinBlue  	=  'assets/img/marcadores/azul.png'
var pinYellow	=  'assets/img/marcadores/amarillo.png'
var pinPink     =  'assets/img/marcadores/rosa.png'
var pinRed 		=  'assets/img/marcadores/rojo.png'
var pinPurple   =  'assets/img/marcadores/morado.png'
var pinGreen  	=  'assets/img/marcadores/verde.png'
var pintblueC   =  'assets/img/marcadores/azul-claro.png'
var pintblueM   =  'assets/img/marcadores/azul-marino.png'
var pinCoral    =  'assets/img/marcadores/coral.png'
var pinPurpleC  =  'assets/img/marcadores/morado-claro.png'
var pinMoztaza  =  'assets/img/marcadores/mostaza.png'
var pinOrangeC  =  'assets/img/marcadores/naranja-claro.png'
var pinBlack    =  'assets/img/marcadores/negro.png'
var pinTurquesa =  'assets/img/marcadores/turquesa.png'
var pinTurquesaC=  'assets/img/marcadores/turquesa-claro.png'
var pintGreenF  =  'assets/img/marcadores/verde-fuerte.png'
var pinGreenL   =  'assets/img/marcadores/verde-limon.png'


/* Se definen los icons para el mapa */
var myIconOrange = L.icon({
	iconUrl: pinOrange,
	iconSize: [30,30]
});
var myIconBlue = L.icon({
	iconUrl: pinBlue,
	iconSize: [30,30]
});
var myIconYellow = L.icon({
	iconUrl: pinYellow,
	iconSize: [30,30]
});
var myIconPink = L.icon({
	iconUrl: pinPink,
	iconSize: [30,30]
});
var myIconRed = L.icon({
	iconUrl: pinRed,
	iconSize: [30,30]
});
var myIconPurple = L.icon({
	iconUrl: pinPurple,
	iconSize: [30,30]
});
var myIconGreen = L.icon({
	iconUrl: pinGreen,
	iconSize: [30,30]
});
var myIconltBlue = L.icon({
	iconUrl: pinBlue,
	iconSize: [30,30]
});


var myIconltBlueC = L.icon({
	iconUrl: pintblueC,
	iconSize: [30,30]
});
var myIconltBlueM = L.icon({
	iconUrl: pintblueM,
	iconSize: [30,30]
});
var myIconltCoral = L.icon({
	iconUrl: pinCoral,
	iconSize: [30,30]
});
var myIconltPurpleC = L.icon({
	iconUrl: pinPurpleC ,
	iconSize: [30,30]
});
var myIconltMoztaza = L.icon({
	iconUrl: pinMoztaza,
	iconSize: [30,30]
});
var myIconlOrangeC = L.icon({
	iconUrl: pinOrangeC,
	iconSize: [30,30]
});
var myIconltBlack = L.icon({
	iconUrl: pinBlack,
	iconSize: [30,30]
});
var myIconltTurquesa = L.icon({
	iconUrl: pinTurquesa,
	iconSize: [30,30]
});
var myIconltTurquesaC = L.icon({
	iconUrl: pinTurquesaC,
	iconSize: [30,30]
});
var myIconltGreenF = L.icon({
	iconUrl: pintGreenF ,
	iconSize: [30,30]
});
var myIconltGreenL = L.icon({
	iconUrl: pinGreenL ,
	iconSize: [30,30]
});
