$( document ).ready(function() {

  $('#listNoticias').DataTable({
    //"bSort" : false,
    "columns": [
        { "orderable": true },
        { "orderable": true },
        { "orderable": true },
        { "orderable": false },
      ],
      "language": {
            "lengthMenu": "Registros _MENU_",
            "zeroRecords": "No hay informacion disponible",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay informacion disponible",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search":         "Búsqueda:",
            "paginate": {
              "first":      "First",
        "last":       "Last",
            "next":       "Siguiente",
            "previous":   "Anterior"
          },

      }
    });


    //Inicializamos calendario
	new Pikaday(
    {
        field: document.getElementById('fecha_publicacion'),
        //field: $('.fecha_publicacion_n'),
        firstDay: 1,
        i18n: {
            previousMonth : 'Anterior',
            nextMonth     : 'Siguiente',
            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb']
        },
        format: 'DD/MM/YYYY',
        theme: 'triangle-theme'
    });

     var msg_valida = '';
    //validamos los campos
    validar_campos = function(){
	    msg_valida = '';

	    if( $.trim($('#titulo').val()) == '' ){
		    msg_valida =  'Ingrese un título a la noticia';
		    return false;
	    }

	    if( $.trim($('#fecha_publicacion').val()) == '' ){
		    msg_valida =  'Ingrese una fecha de publicación';
		    return false;
	    }

	    if( $('#estado').val() == 98 ){
		    msg_valida =  'Ingrese un estado';
		    return false;
	    }

	    if( $.trim($('#descripcion').val()) == '' ){
		    msg_valida = 'Ingrese una descripción de la noticia';
		    return false;
	    }

    };


    //GUARDAR NOTICIA
    $("#noticia").submit(function(event)
	{

		validar_campos();
		if( msg_valida.length > 1){
			notificacion('top','left', msg_valida , 4, "ti-alert");
			return false;
		}

		event.preventDefault(); //detenemos la accion
		var post_url       = $(this).attr("action"); //get form action url
		var request_method = $(this).attr("method"); //get form GET/POST method
		var form_data      = $(this).serialize(); //Encode form elements for submission
		var newForData     = new FormData( this ); //Para que nos envie el input file

		$.ajax({
			type:request_method,
			enctype: 'multipart/form-data',
			processData: false,  // Important!
			contentType: false,
			cache: false,
			url:post_url,
			data:newForData,
			dataType: "json",
			beforeSend: function(){
				$('#guardarNoticia').val("Guardando..."); // Para input de tipo button
				$('#guardarNoticia').prop("disabled","disabled");
			},
			complete:function(){
				$('#guardarNoticia').val("Guardar"); // Para input de tipo button
				$('#guardarNoticia').prop("disabled","");
			},
			success: function(data){

        //Validamos la sesion
        if(typeof data.session != 'undefined' && data.session == 'TIMEOUT'){
              logOutFundacionArcoiris();
        }

				$('#idNoticia').val( data.noticia );
				if(data.msgErr != ''){
                    notificacion('top','left',data.msgErr, 4, "ti-alert");
                }else{
                    notificacion('top','left','La información se ha guardado correctamente', 2, "ti-check");
                }
			},
			error: function(data){
				notificacion('top','left','Hay un problema al guardar la información', 4, "ti-alert");
			}
		});
	});


	//ELIMINAR NOTICIA
	$('#listNoticias tbody').on( 'click', '.eliminaRegistro', function () {
		var rowDeleted = $(this).parents('tr');
		if(confirm("¿Esta seguro de eliminar el registro?"))
		{
				$.ajax({
						 type:'POST',
						 url:"eliminaNoticia",
						 data:{'idNoticia': this.id},
						 beforeSend: function(){},
						 complete:function(){},
						 success: function(data){
						 		notificacion('top','left','La noticia se ha eliminado correctamente', 2, "ti-check");

								var table = $('#listNoticias').DataTable();
								table
							    	.row( rowDeleted )
							        .remove()
							        .draw();
						},
						error: function(data){
								 notificacion('top','left','Hay un problema al eliminar la información', 4, "ti-alert");
						}
				 });
		}
	});







});
