/* Variables globales*/
let arrayInfo = [];
var arrayDatosValue;
let conteoData = 0;
var mymap = ''



/* Entrada del documento */
$(document).ready(function() {

    /*Entrada de la Gráfica */
    var fecha   = new Date();
    var ano     = fecha.getFullYear()   ;
    $("#year").val(ano)

    if ($('#weathermap').length > 0) {
        if ($("#tipo_mapa").val() == 1) {
            getMapa();
        }
        $("#estado_agresion").change(function() {
            fnMapa($("#tipo_mapa").val());
        });

        $("#tipo_incidente").change(function() {
            fnMapa($("#tipo_mapa").val());
        });
        $("#tipo_mapa").change(function() {
            fnMapa($("#tipo_mapa").val());
        });
        $("#year").change(function() {
            fnMapa($("#tipo_mapa").val());
            fnGetyear()
        });
        $("#filtro").change(function() {
            conteoData = 0;
            fnMapa($("#tipo_mapa").val());
        });
    }
});
/* Función que manda a llamar el tipo de mapa que se presentará */
function fnMapa(tipoMapa) {
    document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'></div>";

    switch (tipoMapa) {
        case "1":
            getMapa()
            $("#iconIformation").show()
            break;

        case "2":
            getGraficaEstados();
            $("#iconIformation").hide()
            break;
    }

}
/* Función para obtener el mapa */
function getMapa() {

    document.getElementById('weathermap').innerHTML = "<div id='mapid' class='divMapid'></div>";
    // Se pinta el mapa
    mymap = L.map('mapid').setView([23.031491, -102.272216], 4.5);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiamFwaCIsImEiOiJjanNucWFrNjAwZjI3NDlwMWRpYTE1cHQxIn0.4pcNgOGcUTAlZ6xW5KEfzg', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.dark',
        accessToken: 'pk.eyJ1IjoiamFwaCIsImEiOiJjanNucWFrNjAwZjI3NDlwMWRpYTE1cHQxIn0.4pcNgOGcUTAlZ6xW5KEfzg',
        noUpdate: false
    }).addTo(mymap);

    if (mymap.scrollWheelZoom) {
        mymap.scrollWheelZoom.disable();
    }

    if ($("#filtro").val() != '')
        fnListaGrafica();
    else {
        fnListaGraficaGral();
    }

}

/* Función para tener las gráficas por estado*/

function getGraficaEstados() {
    conteoData = 0;
    if ($("#filtro").val() != '')
        fnFilroSelect();
    else {
        tipoCrimenGral();
    }
}
/* Obtenemos la info gral de la consulta */
function tipoCrimenGral() {
    $.ajax({
        type: "POST",
        url: "tipoCrimenGral",
        dataType: "json",
        data: {
            "estado_agresion": $("#estado_agresion").val(),
            "tipo_incidente": $("#tipo_incidente").val(),
            "year": $("#year").val(),
            "filtro": $("#filtro").val()
        },
        success: function(data) {
            let dataInfo = data.message
            var arrayData = []

            let crimen = ''

            for (let info in dataInfo) {

                var arrayDatos = dataInfo[info]
                if (arrayDatos.length > 0) {
                    for (let key in arrayDatos) {

                        var datosRecibidos = arrayDatos[key]

                        let tipo_crimen = datosRecibidos["tipo_crimen"]
                        switch (tipo_crimen) {
                            case "1":
                                crimen = "Asesinato"
                                break;
                            case "2":
                                crimen = "Desaparición"
                                break;
                            case "98":
                                crimen = "No se específica"
                                break;
                            case "99":
                                crimen = "No fue posible identificarlo "
                                break;
                        }
                        document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'></div>";
                        fnValueData(tipo_crimen);
                        arrayData[key] = { name: crimen, data: arrayDatosValue }
                        fnGrafica(arrayData)

                    }
                } else {
                    fnGrafica(arrayData)
                    document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1> 0 CASOS</h1></div>";
                }
            }
        },
        error: function(data) {
            console.log("Error en información")
        }
    });
}

/* Se obtiee la información de los filtros */
function fnFilroSelect() {

    $.ajax({
        type: "POST",
        url: "estadosGrafica",
        dataType: "json",
        data: {
            "estado_agresion": $("#estado_agresion").val(),
            "tipo_incidente": $("#tipo_incidente").val(),
            "year": $("#year").val(),
            "tipo_mapa": $("#tipo_mapa").val(),
            "filtro": $("#filtro").val()
        },
        success: function(data) {
            let dataInfo = data.message
            var name = '';
            var arrayData = []

            if ($("#filtro").val() == 'Tipo de Arma') {
                fnProcesoTipoArma(dataInfo)
                return;
            }

            for (let info in dataInfo) {
                let arrayInfo = dataInfo[info]
                var arrayDatos = []
                if (arrayInfo.length > 0) {
                    for (let key in arrayInfo) {

                        let valor = arrayInfo[key]["valor"]
                        name = fnName($("#filtro").val(), valor)
                        fnValueData(valor);

                        arrayData[key] = { name: name, data: arrayDatosValue }
                        fnGrafica(arrayData)
                    }
                } else {
                    fnGrafica(arrayData)
                    document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1> 0 CASOS</h1></div>";
                }
            }
        },
        error: function(data) {
            console.log("Error en información")
        }
    });
}

/* Función que pinta la gráfica de estados por Highcharts */
function fnGrafica(arrayData) {
	
	console.log(arrayData);
    /* etiquetas en la gráfica */
    let titulo = ''
    let colorTitulo = '#ffffff'
    if ($("#filtro").val() == '') {
        if ($("#estado_agresion :selected").text() == 'Seleccione')
            titulo = 'Republica Méxicana'
        else
            titulo = $("#estado_agresion :selected").text();
    } else {
        titulo = $("#filtro").val()
    }
    if (arrayData.length == 0) {
        titulo += " <br> No existe información en la busqueda seleccionada "
        colorTitulo = '#ffffff'
        arrayData = []
    }
    /* Sé pinta la gráfica con la información ingresada */
    Highcharts.chart('mapid', {
	    
        chart: {
            type: 'line'
        },
        title: {
            style: {
                color: colorTitulo,

            },
            text: titulo
        },

        subtitle: {
            style: {
                color: '#ffffff',

            },
            text: $("#year").val()
        },

        credits: {
            enabled: false
        },

        xAxis: {
            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        
        series: arrayData,
        
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
            }
        },
    });

    Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
        '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
        backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
            stops: [
                [0, '#2a2a2b'],
                [1, '#3e3e40']
            ]
        },
        style: {
            fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063'
    },
   /* title: {
        style: {
            color: '#E0E0E3',
            textTransform: 'uppercase',
            fontSize: '20px'
        }
    },
    subtitle: {
        style: {
            color: '#E0E0E3',
            textTransform: 'uppercase'
        }
    },*/
    xAxis: {
        gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
            style: {
                color: '#A0A0A3'

            }
        }
    },
    yAxis: {
        gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
            style: {
                color: '#A0A0A3'
            }
        }
    },
    tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
            color: '#F0F0F0',
        }
    },
    plotOptions: {
        series: {
            dataLabels: {
                color: '#B0B0B3'
            },
            marker: {
                lineColor: '#333'
            }
        },
        boxplot: {
            fillColor: '#505053'
        },
        candlestick: {
            lineColor: 'white'
        },
        errorbar: {
            color: 'white'
        }
    },
    legend: {
        itemStyle: {
            color: '#E0E0E3'
        },
        itemHoverStyle: {
            color: '#FFF'
        },
        itemHiddenStyle: {
            color: '#606063'
        }
    },
    credits: {
        style: {
            color: '#666'
        }
    },
    labels: {
        style: {
            color: '#707073'
        }
    },

    drilldown: {
        activeAxisLabelStyle: {
            color: '#F0F0F3'
        },
        activeDataLabelStyle: {
            color: '#F0F0F3'
        }
    },

    navigation: {
        buttonOptions: {
            symbolStroke: '#DDDDDD',
            theme: {
                fill: '#505053'
            }
        }
    },




    // special colors for some of the
    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);
    

    
}
/* Función que nos devuelve la información de los 12 meses  */
function fnValueData(value) {

    $.ajax({
        type: "POST",
        url: "valoresGrafica",
        dataType: "json",
        async: false,
        data: {
            "value": value,
            "estado_agresion": $("#estado_agresion").val(),
            "tipo_incidente": $("#tipo_incidente").val(),
            "year": $("#year").val(),
            "filtro": $("#filtro").val()
        },
        success: function(data) {
            let dataInfo = data.message
            let plural = 'S';

            for (let info in dataInfo) {
                let arrayInfo = dataInfo[info]
                arrayDatosValue = []

                for (let key in arrayInfo) {

                    let contador = arrayInfo[key]["contador"]
                    let mes = arrayInfo[key]["mes"]
                    if (mes > 0) {
                        arrayDatosValue.push(parseInt(contador));
                        conteoData += parseInt(contador);

                    } else {
                        arrayDatosValue.push(0);
                    }
                }
            }
            if (conteoData == 1)
                plural = ""

            document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1>" + conteoData + " CASO" + plural + "</h1></div>";

        },
        error: function(data) {
            console.log("Error en información")
        }
    });

}
/* Funcuón que devuelve el nombre */
function fnName(filtro, valor) {
    name = ''
    if (filtro == 'Orientación Sexual') {

        switch (valor) {
            case '1':
                name = "Heterosexual"
                break;
            case '2':
                name = "Homosexual/ Gay"
                break;
            case '3':
                name = "Lesbiana"
                break;
            case '4':
                name = "Bisexual"
                break;
            case '5':
                name = "Otro"
                break;
            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;
        }
    }

    if (filtro == 'Por Edad') {

        switch (valor) {
            case '1':
                name = "5-9"
                break;
            case '2':
                name = "10-14"
                break;
            case '3':
                name = "15-19"
                break;
            case '4':
                name = "20-24"
                break;
            case '6':
                name = "25-29"
                break;
            case '7':
                name = "30-34"
                break;
            case '8':
                name = "35-39"
                break;
            case '5':
                name = "25-29"
                break;
            case '9':
                name = "40-44"
                break;
            case '10':
                name = "45-49"
                break;
            case '11':
                name = "50-54"
                break;
            case '12':
                name = "55-59"
                break;
            case '13':
                name = "60-64"
                break;
            case '14':
                name = "65-69"
                break;
            case '15':
                name = "70-74"
                break;
            case '16':
                name = "75-79"
                break;
            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;

        }
    }

    if (filtro == 'Por Identidad de Género') {

        switch (valor) {
            case '1':
                name = "Hombre"
                break;
            case '2':
                name = "Mujer"
                break;
            case '3':
                name = "Mujer Trans"
                break;
            case '4':
                name = "Homnbe Trans"
                break;
            case '5':
                name = "Otro"
                break;
            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;

        }
    }

    if (filtro == 'Por Impunidad') {

        switch (valor) {
            case '1':
                name = "En Proceso"
                break;
            case '2':
                name = "Abandonado"
                break;
            case '3':
                name = "Concluido"
                break;

            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;

        }
    }


    if (filtro == 'Tipo de Arma') {

        switch (valor) {
            case '1':
                name = "Arma de fuego"
                break;
            case '2':
                name = "Arma punzo cortante"
                break;
            case '3':
                name = "Lapidación"
                break;
            case '4':
                name = "Objeto constrictor"
                break;
            case '5':
                name = "Otro"
                break;
            case '6':
                name = "Mecanismos asfixiante"
                break;
            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;

        }
    }

    if (filtro == 'Por Migración') {

        switch (valor) {
            case '1':
                name = "Visa humanitaria"
                break;
            case '2':
                name = "En proceso de refugio"
                break;
            case '3':
                name = "Refugiados"
                break;
            case '4':
                name = "Residente"
                break;
            case '6':
                name = "Regular"
                break;
            case '7':
                name = "Irregular"
                break;
            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;


        }
    }
    
    
    if (filtro == 'Defensores DH') {

        switch (valor) {
            case '1':
                name = "SI"
                break;
            case '2':
                name = "NO"
                break;

            case '98':
                name = "No se específica / Sin información"
                break;
            case '99':
                name = "No fue posible identificarlo"
                break;

        }
    }


    return name;
}

/* Mapa sin filtro seleccionado */
function fnListaGraficaGral() {
    // Se obtiene la iformación que se reflejara en los pins del mapa
    var divInformacion = " <div id='iconIformation' class= 'icon-information' align ='center' > "
    divInformacion += " <table class='panel-marcadores'>"

    $.ajax({
        type: "POST",
        url: "listaGrafica",
        dataType: "json",
        data: {
            "estado_agresion": $("#estado_agresion").val(),
            "tipo_incidente": $("#tipo_incidente").val(),
            "filtro": $("#filtro").val(),
            "year": $("#year").val()
        },
        success: function(data) {
            let dataInfo = data.message
            let causa = ''
            let conteoData = 0
            let plural = 'S'
            let divAsesinato = 0
            let divDesaparicion = 0
            let divNoEspe = 0
            for (let info in dataInfo) {
                let arrayInfo = dataInfo[info]
                for (let key in arrayInfo) {
                    let contadorCrimen = arrayInfo[key]["contador_crimen"]
                    let tipoCrimen = arrayInfo[key]["tipo_crimen"]
                    let latitud = parseInt(arrayInfo[key]["latitud"])
                    let longitud = parseInt(arrayInfo[key]["longitud"])
                    let estado = arrayInfo[key]["estado"]
                    let random = Math.random() * 1;

                    if (tipoCrimen == 1) {
                        divAsesinato++
                        causa = "Asesinato"
                        L.marker([latitud, longitud + random], { icon: myIconBlue }).addTo(mymap)
                            .bindPopup("<b>Causa: " + causa + "(" + contadorCrimen + ")</b>").openPopup();
                        conteoData = conteoData + parseInt(contadorCrimen)
                        if (divAsesinato == 1) {
                            divInformacion += "<tr><td width='1em'> <img src='" + myIconBlue.options.iconUrl + "'></td><td>" + causa + "</td></tr>"
                            divAsesinato++
                        }

                    }
                    if (tipoCrimen == 2) {
                        divDesaparicion++
                        causa = "Desaparición"
                        L.marker([latitud, longitud + random], { icon: myIconRed }).addTo(mymap)
                            .bindPopup("<b>Causa: " + causa + "(" + contadorCrimen + ")</b>").openPopup();
                        conteoData = conteoData + parseInt(contadorCrimen)
                        if (divDesaparicion == 1) {
                            divInformacion += "<tr><td width='1em'> <img src='" + myIconRed.options.iconUrl + "'></td><td>" + causa + "</td></tr>"
                            divDesaparicion++
                        }
                    }
                    if (tipoCrimen == 98) {
                        divNoEspe++
                        causa = "No se específica / Sin información"
                        L.marker([latitud, longitud + random], { icon: myIconYellow }).addTo(mymap)
                            .bindPopup("<b>Causa: " + causa + "(" + contadorCrimen + ")</b>").openPopup();
                        conteoData = conteoData + parseInt(contadorCrimen)
                        if (divNoEspe == 1) {
                            divInformacion += "<tr><td> <img src='" + myIconYellow.options.iconUrl + "'></td><td>" + causa + "</td></tr>"
                            divNoEspe++
                        }
                    }
                }
            }
            if (conteoData == 1)
                plural = ""

            divInformacion += "</table> </div>"
            document.getElementById('pingDescription').innerHTML = divInformacion
            document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1>" + conteoData + " CASO" + plural + "</h1></div>";
            $("#iconIformation").show()
        },
        error: function(data) {
            console.log("Error en información")
        }
    });
}

function fnListaGrafica() {

    $.ajax({
        type: "POST",
        url: "estadosGrafica",
        dataType: "json",
        data: {
            "estado_agresion": $("#estado_agresion").val(),
            "tipo_incidente": $("#tipo_incidente").val(),
            "year": $("#year").val(),
            "tipo_mapa": $("#tipo_mapa").val(),
            "filtro": $("#filtro").val()
        },
        success: function(data) {
            let dataInfo = data.message
            var name = '';
            var arrayData = []
            if ($("#filtro").val() == 'Tipo de Arma') {
                fnMapaTipoArma(dataInfo);
                return
            }
            for (let info in dataInfo) {
                let arrayInfo = dataInfo[info]
                var arrayDatos = []
                if (arrayInfo.length > 0) {
                    for (let key in arrayInfo) {

                        let contador = arrayInfo[key]["contador"]
                        let valor = arrayInfo[key]["valor"]
                        let estado = arrayInfo[key]["estado"]
                        let latitud = arrayInfo[key]["latitud"]
                        let longitud = arrayInfo[key]["longitud"]


                        name = fnName($("#filtro").val(), valor)
                        arrayData[key] = { name: name, contador: contador, latitud: latitud, longitud: longitud, estado: estado }

                    }
                } else {
                    fnListaGraficaGral()
                    document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1> 0 CASOS</h1></div>";
                }
            }

            mostrarPins(arrayData);
        },
        error: function(data) {
            console.log("Error en información")
        }
    });


}

function mostrarPins(arrayData) {

    let conteoData = 0
    var arrayPins = []
    var icons = ''
    var arrayicons = []

    var divInformacion = " <div id='iconIformation' class= 'icon-information' align ='center' > "
    divInformacion += " <table class='panel-marcadores'>"

    for (let key in arrayData) {


        let contador = arrayData[key]["contador"]
        let name = arrayData[key]["name"]
        let estado = arrayData[key]["estado"]
        let latitud = parseInt(arrayData[key]["latitud"])
        let longitud = parseInt(arrayData[key]["longitud"])
        let random = Math.random() * 1;
        var plural = 'S'
        let causa = name
        if (causa == '')
            causa = 'No se específica / Sin información'

        icons = namePin(causa)

        var valIcons = ''
        L.marker([latitud, longitud + random], { icon: icons }).addTo(mymap)
            .bindPopup("<b>Causa: " + causa + "(" + contador + ")</b>").openPopup();
        valIcons = arrayicons.indexOf(causa)
        arrayicons.push(causa)

        if ($("#filtro").val() == 'Por Edad') {
            if (causa != 'No se específica / Sin información') {
                causa = causa + ' años de edad';
            }
        }

        if (valIcons == -1) {
            divInformacion += "<tr><td width='1em'> <img src='" + icons.options.iconUrl + "'></td><td>" + causa + "</td></tr>"
        }
        conteoData = conteoData + parseInt(contador)
    }
    if (conteoData == 1)
        plural = ""

    document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1>" + conteoData + " CASO" + plural + "</h1></div>";

    divInformacion += "</table> </div>"
    document.getElementById('pingDescription').innerHTML = divInformacion
    $("#iconIformation").show()


}

function namePin(causa) {

    var inconReturn = ''
    switch (causa) {
        case 'Heterosexual':
            inconReturn = myIconOrange
            break;
        case 'Homosexual/ Gay':
            inconReturn = myIconBlue
            break;
        case 'Lesbiana':
            inconReturn = myIconYellow
            break;
        case 'Bisexual':
            ninconReturname = myIconPink
            break;
        case 'Otro':
            inconReturn = myIconltMoztaza
            break;
        case 'No se específica / Sin información':
            inconReturn = myIconPurple
            break;
        case 'No fue posible identificarlo':
            inconReturn = myIconGreen
            break;
        case '5-9':
            inconReturn = myIconltCoral
            break;
        case '10-14':
            inconReturn = myIconltPurpleC
            break;
        case '15-19':
            inconReturn = myIconltMoztaza
            break;
        case '20-24':
            inconReturn = myIconlOrangeC
            break;
        case '25-29':
            inconReturn = myIconltBlack
            break;
        case '30-34':
            inconReturn = myIconltTurquesa
            break;
        case '35-39':
            inconReturn = myIconltTurquesaC
            break;

        case '40-44':
            inconReturn = myIconltGreenL
            break;
        case '45-49':
            inconReturn = myIconOrange
            break;
        case '50-54':
            inconReturn = myIconBlue
            break;
        case '55-59':
            inconReturn = myIconYellow
            break;
        case '60-64':
            inconReturn = myIconPink
            break;
        case '65-69':
            inconReturn = myIconBlue
            break;
        case '70-74':
            inconReturn = myIconRed
            break;
        case '75-79':
            inconReturn = myIconPurple
            break;

        case 'Hombre':
            inconReturn = myIconOrange
            break;
        case 'Mujer':
            inconReturn = myIconltBlue
            break;
        case 'Mujer Trans':
            inconReturn = myIconYellow
            break;
        case 'Homnbe Trans':
            inconReturn = myIconPink
            break;
        case 'En Proceso':
            inconReturn = myIconlOrangeC
            break;
        case 'Abandonado':
            inconReturn = myIconltBlueC
            break;
        case 'Concluido':
            inconReturn = myIconYellow
            break;
        case 'Arma de fuego':
            inconReturn = myIconlOrangeC
            break;
        case 'Arma punzo cortante':
            inconReturn = myIconltBlueM
            break;
        case 'Lapidación':
            inconReturn = myIconYellow
            break;
        case 'Objeto constrictor':
            inconReturn = myIconPink
            break;
        case 'Mecanismos asfixiante':
            inconReturn = myIconRed
            break;
        case 'Visa humanitaria':
            inconReturn = myIconOrange
            break;
        case 'En proceso de refugio':
            inconReturn = myIconltBlue
            break;
        case 'Refugiados':
            inconReturn = myIconltGreenL
            break;
        case 'Residente':
            inconReturn = myIconltTurquesa
            break;
        case 'Regular':
            inconReturn = myIconRed
            break;
        case 'Irregular':
            inconReturn = myIconPurpleC
            break;

        case 'SI':
            inconReturn = myIconOrange
            break;
        case 'NO':
            inconReturn = myIconBlue
            break;    

    }

    return inconReturn;
}

function fnProcesoTipoArma(dataInfo) {
    var arrayTipoArma
    var tipoArmaRepetido

    var arrayDatoTipoArma = []
    var arrayData = []
    var totalData = 0
    for (let info in dataInfo) {
        let arrayInfo = dataInfo[info]
        var arrayDatos = []
        if (arrayInfo.length > 0) {
            for (let key in arrayInfo) {

                let valor = arrayInfo[key]["valor"]
                let cuesti = parseInt(arrayInfo[key]["cuesti"])
                totalData = totalData + cuesti
                arrayTipoArma = valor.split(",")
                for (let tipoArma in arrayTipoArma) {
                    tipoArmaRepetido = arrayDatoTipoArma.indexOf(arrayTipoArma[tipoArma])
                    if (tipoArmaRepetido == -1) {
                        arrayDatoTipoArma.push(arrayTipoArma[tipoArma])
                    }

                }

            }
            for (let key in arrayDatoTipoArma) {

                let valor = arrayDatoTipoArma[key]
                name = fnName($("#filtro").val(), valor)
                fnValueData(valor);

                arrayData[key] = { name: name, data: arrayDatosValue }
                fnGrafica(arrayData)
            }

            document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1> " + totalData + " CASOS</h1></div>";
        } else {
            fnGrafica(arrayData)
            document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1> 0 CASOS</h1></div>";
        }

    }
}

function fnMapaTipoArma(dataInfo) {
    var arrayData = []
    for (let info in dataInfo) {
        let arrayInfo = dataInfo[info]


        if (arrayInfo.length > 0) {
            var contadorArray = 0;
            for (let key in arrayInfo) {
                let arrayInfoKey = arrayInfo[key]
                for (let keyInfo in arrayInfoKey) {
                    contadorArray++
                    var contador = arrayInfoKey[keyInfo]["contador"]
                    var valor = arrayInfoKey[keyInfo]["valor"]
                    var estado = arrayInfoKey[keyInfo]["estado"]
                    var latitud = arrayInfoKey[keyInfo]["latitud"]
                    var longitud = arrayInfoKey[keyInfo]["longitud"]
                    var cuesti = arrayInfoKey[keyInfo]["cuesti"]

                    name = fnName($("#filtro").val(), valor)
                    arrayData[contadorArray] = { name: name, contador: contador, latitud: latitud, longitud: longitud, estado: estado }

                }
            }
        } else {
            fnListaGraficaGral()
            document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1> 0 CASOS</h1></div>";
        }
    }
    mostrarPins(arrayData);
    document.getElementById('contenedorCasos').innerHTML = "<div id='conteoCasos'><h1>" + cuesti + " CASOS</h1></div>";
}
function fnGetyear( ){
    $.ajax({
        type: "POST",
        url: "getYear",
        dataType: "json",
        data: {

            "year": $("#year").val()
        },
        success: function(data) {
        },
        error: function(data) {
            console.log("Error en información")
        }
    });
}