// initialise plugins Para el menú
jQuery(function(){
  jQuery('#menuPrincipal').superfish({
    //useClick: true
  });
});

$(document).ready(function() {
    //=========================
    //      CARRUSEL
    //=========================
    var current = 0;
    var numImages = 14; //Numero de imagen del carrusel
    function moverSlide(){
          if (numImages > current + 7) {
              current = current+1;
          } else {
              current = 0;
          }
          $(".carrusel").animate({"left": -($('#logo_org_'+current).position().left)}, 2000);
    }
    setInterval(moverSlide, 9000);
 });
