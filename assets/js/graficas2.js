 Highcharts.chart('graficaPie', {
	    
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Pie point CSS',
            style: {
                color: '#ffffff',

            },
        },

        subtitle: {
            style: {
                color: '#ffffff',

            },
            text: 2019
        },

        credits: {
            enabled: false
        },

        xAxis: {
            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
	        /*backgroundColor: 'rgba(0, 0, 0, 0.85)',*/
	        style: {
	            color: '#F0F0F0',
	        }
    	},
        series: [{
	        name: 'Brands',
	        colorByPoint: true,
	        data: [{
		            name: 'Chrome',
		            y: 61.41,
		            sliced: true,
		            selected: true
		        }, {
		            name: 'Internet Explorer',
		            y: 11.84
		        }, {
		            name: 'Firefox',
		            y: 10.85
		        }, {
		            name: 'Edge',
		            y: 4.67
		        }, {
		            name: 'Safari',
		            y: 4.18
		        }, {
		            name: 'Other',
		            y: 7.05
	        }]
	        
	    }],
        
        plotOptions: {
            pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: true,
	                style: {
	            		color: '#ffffff',
	        		}
	            },
	            showInLegend: true
		            
        	},
        },
        legend: {
	        itemStyle: {
	            color: '#E0E0E3'
	        },
	        itemHoverStyle: {
	            color: '#FFF'
	        },
	        itemHiddenStyle: {
	            color: '#606063'
	        }
    	},
    	navigation: {
	        buttonOptions: {
	            symbolStroke: '#DDDDDD',
	            theme: {
	                fill: '#505053'
	            }
	        }
    	},
    });
    
    //Highcharts.theme = {

