$("#btnSubmit").on("click",function (e){
  e.preventDefault();
  var expreg = /^(?=.*\d)(?=.*[\-@_])(?=.*[A-Z])(?=.*[a-z])\S{8,16}$/;
  password = $("#password").val();
  password_verify = $("#password_verify").val();
  if(password.length >= 8 && password === password_verify){
    if(expreg.test(password)){
      $("#form_resetpassword").submit();
    } else {
      login.showNotification("top","left","La contraseña debe tener mínimo una minúscula, una mayúscula, un dígito y uno de los siguientes caracteres especiales: \"-,@,_\"","warning");
      $("#password, #password_verify").val("");
    }
  } else {
    if(password.length<8){
      login.showNotification("top","left","La contraseña debe tener mínimo una longitud de 8","warning");
    }
    if(password !== password_verify){
      login.showNotification("top","left","Las contraseñas no coinciden, verifique","warning");
      $("#password").val("");
      $("#password_verify").val("");
    }
  }
});
