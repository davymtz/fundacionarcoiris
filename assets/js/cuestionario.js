$( document ).ready(function() {

	  	//Inicializamos tabs
      	$('#tabCuestionario a').on('click', function (e) {
	  		e.preventDefault()
	  		$(this).tab('show')
    	});

    	//Inicializamos calendario
    	new Pikaday(
	    {
	        field: document.getElementById('fecha_hecho'),
	        firstDay: 1,
	        i18n: {
	            previousMonth : 'Anterior',
	            nextMonth     : 'Siguiente',
	            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	            weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	            weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb']
	        },
	        format: 'DD/MM/YYYY',
	        theme: 'triangle-theme',
	        minDate: new Date('01/01/2014')
	    });

			$('#listCuestionario').DataTable({
				//"bSort" : false,
				"columns": [
				    { "orderable": true },
				    { "orderable": true },
				    { "orderable": true },
				    { "orderable": false },
				    { "orderable": false },
				  ],
		    	"language": {
		            "lengthMenu": "Registros _MENU_",
		            "zeroRecords": "No hay informacion disponible",
		            "info": "Página _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay informacion disponible",
		            "infoFiltered": "(filtered from _MAX_ total records)",
		            "search":         "Búsqueda:",
		            "paginate": {
			            "first":      "First",
						"last":       "Last",
				        "next":       "Siguiente",
				        "previous":   "Anterior"
			        },

			    }
	    	});

			//====================================================================
			//                    REGLAS PARA INFORMACIÓN GUARDADA
			//====================================================================
				//alert($('#localidad_hecho').val());
				if($('#localidad_hecho').length > 0)
				{
					/*console.log($('#localidad_hecho').val().trim()+"___");*/
					if ($('#localidad_hecho').val().trim() !== '99' &&  $('#localidad_hecho').val().trim() !== '98') {
							$( "input[name='municipio_hecho']" ).prop('readonly',false);
					}
			          //Si solo es un option lo ponemos predeterminado
			            //console.log($('#localidad_hecho option').length+"___");
			            if( $('#localidad_hecho option').length == 3){
			                $('#localidad_hecho option[value="98"]').remove();
			                $('#localidad_hecho option[value="99"]').remove();
			                $( "input[name='municipio_hecho']" ).prop('readonly',false);
			            }
				}

				if($('#nacionalidad_mexicana').length > 0)
				{
					if ($('#nacionalidad_mexicana').val().trim() !== '99' &&  $('#nacionalidad_mexicana').val().trim() !== '98') {
							//$( "#localidad_nacimiento" ).prop('disabled',false);
					}

					if($('#nacionalidad_mexicana').val() == 1 || $('#nacionalidad_mexicana').val() == 2){
						$( "#localidad_nacimiento" ).prop('disabled',false);
					}
					if($('#nacionalidad_mexicana').val() == 3){
						$( "#pais_nacimiento" ).prop('disabled',false);
					}

				}
				if($('#localidad_nacimiento').length > 0)
				{
					if ($('#localidad_nacimiento').val().trim() !== '99' &&  $('#localidad_nacimiento').val().trim() !== '98') {
							$( "input[name='municipio_nacimiento']" ).prop('readonly',false);
					}
				}

				$( ".valida-otro" ).each(function() {
						var nombreCampo = this.name;
						var valSelecc   = this.value;
				  	if(valSelecc === '5'){
								$( "input[name='otro_"+nombreCampo+"']" ).prop('readonly',false);
						}
				});

				$( ".valida-otro-multiple" ).each(function() {
						var nombreCampo = this.name;
						var valSelecc   = this.value;
						if(valSelecc === '5'){
								$( "input[name='otro_"+nombreCampo+"']" ).prop('readonly',false);
						}
				});

		        //Si hay un archivo guardado ocultamos el boton de subir y ponemos link de descarga
		        if($('#nombre_archivo_comunicacion').length > 0){
		            if($('#nombre_archivo_comunicacion').val() != ''){
		                $('#div-input').css('display','none');
		            }else{
		                $('#div-input').css('display','');
		                $('#div-download-archivo').css('display','none');
		            }
		        }

		//=========================================================================
	    //            Comportamiento elementos dependientes
	    //=========================================================================

      $('.cerrarArchivo').click(function(){
          $('#div-download-archivo').css('display','none');
          $('#div-input').css('display','');
          $('.nomArchivoUpload').html('<i class="ti-upload"></i> &nbsp; Adjuntar archivo'); //cambiamos el texto del boton
          //limpiamos el campo file
          $('#archivo_comunicacion').val('');
      });



	    //-- Combo Orientacion sexual, Identidad genero, Origen etnico, Estado civil, nivel_estudio
	    $('.valida-otro').on('change',function(){
		    var nombreCampo = this.name;
		    $( "input[name='otro_"+nombreCampo+"']" ).val('');
		    if(this.value == 5){ // Si es igual a otro
			    $( "input[name='otro_"+nombreCampo+"']" ).prop('readonly',false);
		    }else{
			    $( "input[name='otro_"+nombreCampo+"']" ).prop('readonly',true);
		    }
	    });

	    //FUNCIONAMIENTO PARA TIPO DE ARMA
	    $('.valida-otro-multiple').on('change',function(){
		    $( this ).each(function() {
			    	var seleccionado = false;
						$( "#tipo_arma option:selected" ).each(function() {
								if( this.value === '5' ){
									nombreCampo  = this.name;
									seleccionado = true;
								}
    					});
    				if(seleccionado){
	    				$( "input[name='otro_tipo_arma']" ).prop('readonly',false);
    				}else{
	    				$( "input[name='otro_tipo_arma']" ).prop('readonly',true);
    				}

				});
	    });


	    $('#localidad_nacimiento').on('change',function(){
		    if( this.value != 98 && this.value != 99 ){ // Si es igual a otro
			    $( "input[name='municipio_nacimiento']" ).prop('readonly',false);
		    }else{
			    $( "input[name='municipio_nacimiento']" ).prop('readonly',true);
		    }
	    });

	    $('#localidad_hecho').on('change',function(){
		    if(this.value != 98){ // Si es igual a otro
			    $( "input[name='municipio_hecho']" ).prop('readonly',false);
		    }else{
			    $( "input[name='municipio_hecho']" ).val('');
			    $( "input[name='municipio_hecho']" ).prop('readonly',true);
		    }
	    });

	    //-- Combo la victima era mexicana
	    $('#nacionalidad_mexicana').on('change',function()
	    {
		    switch(this.value) {
			  case '1':
			  	case '2':
			    	$( "#localidad_nacimiento" ).prop('disabled',false);
			    	$( "#pais_nacimiento" ).prop('disabled','disabled');
			    	$( "input[name='municipio_nacio']" ).val('');
			        $( "input[name='municipio_nacio']" ).prop('readonly',true);
			  break;
			  case '3':
			  		$( "#pais_nacimiento" ).prop('disabled',false);
			  		$( "#localidad_nacimiento").prop('selectedIndex', 0);
			  		$( "#localidad_nacimiento" ).prop('disabled','disabled');
			        $( "input[name='municipio_nacimiento']" ).val('');
			        $( "input[name='municipio_nacimiento']" ).prop('readonly',true);
			  break;
			  default:
			    $("#localidad_nacimiento").prop('selectedIndex', 0);
			    $( "#localidad_nacimiento" ).prop('disabled','disabled');
			    $( "input[name='municipio_nacimiento']" ).val('');
			    $( "input[name='municipio_nacimiento']" ).prop('readonly',true);
			}
	    });

	    //-- radio botones
	    $("input[name='tipo_crimen']").on('click',function(){
		    if(this.value == 2){ // Si DESAPARICION
			    	$( "select[name='relacion_victima_agresor']" ).prop('disabled','disabled');
		     		$( "select[name='relacion_victima_agresor']" ).prop('selectedIndex', 0);
		     		$( "input[name='otro_relacion_victima_agresor']" ).val('');
		        $( "input[name='otro_relacion_victima_agresor']" ).prop('readonly',true);

		        $("input[name='utilizo_arma']").prop('disabled','disabled');

			    	$( "select[name='tipo_arma']" ).prop('disabled','disabled');
		     		$( "select[name='tipo_arma']" ).prop('selectedIndex', 0);
		     		$( "input[name='otro_tipo_arma']" ).val('');
		        $( "input[name='otro_tipo_arma']" ).prop('readonly',true);

		        $( "select[name='causa_muerte']" ).prop('disabled','disabled');
		     		$( "select[name='causa_muerte']" ).prop('selectedIndex', 0);

		     		$( "select[name='presencia_agresiones']" ).prop('disabled','disabled');
		     		$( "select[name='presencia_agresiones']" ).prop('selectedIndex', 0);

						$( "select[name='lugar_cuerpo_hallado']" ).prop('disabled','disabled');
		     		$( "select[name='lugar_cuerpo_hallado']" ).prop('selectedIndex', 0);
						$( "input[name='otro_lugar_cuerpo_hallado']" ).val('');
		        $( "input[name='otro_lugar_cuerpo_hallado']" ).prop('readonly',true);

						$( "select[name='quien_hallo_victima']" ).prop('disabled','disabled');
		     		$( "select[name='quien_hallo_victima']" ).prop('selectedIndex', 0);
						$( "input[name='otro_quien_hallo_victima']" ).val('');
		        $( "input[name='otro_quien_hallo_victima']" ).prop('readonly',true);

		    }else{
			    	$( "select[name='relacion_victima_agresor']" ).prop('disabled','');
		     		$( "select[name='relacion_victima_agresor']" ).prop('selectedIndex', 0);

		     		$( "input[name='otro_relacion_victima_agresor']" ).val('');
		        $("input[name='utilizo_arma']").prop('disabled','');
			    	$( "select[name='tipo_arma']" ).prop('disabled','');
		     		$( "input[name='otro_tipo_arma']" ).val('');
		        $( "select[name='causa_muerte']" ).prop('disabled','');
		     		$( "select[name='presencia_agresiones']" ).prop('disabled','');

						$( "select[name='lugar_cuerpo_hallado']" ).prop('disabled','');
						$( "input[name='otro_lugar_cuerpo_hallado']" ).prop('readonly',false);

						$( "select[name='quien_hallo_victima']" ).prop('disabled','');
						$( "input[name='otro_quien_hallo_victima']" ).prop('readonly',false);
		    }
		})

		$("input[name='utilizo_arma']").on('click',function(){
		    if(this.value == 1){ // Si
			    $( "select[name='tipo_arma']" ).prop('disabled','');
			    $( "select[name='tipo_arma']" ).prop('selectedIndex', 0);
			    $( "input[name='otro_tipo_arma']" ).val('');
			    $( "input[name='otro_tipo_arma']" ).prop('readonly',true);
		    }
		    else{
			    if(this.value == 2){ // NO
			     	$( "select[name='tipo_arma']" ).prop('disabled','disabled');
			     	$( "select[name='tipo_arma']" ).prop('selectedIndex', 0);
			     	$( "input[name='otro_tipo_arma']" ).val('');
			        $( "input[name='otro_tipo_arma']" ).prop('readonly',true);
			        $( "select[name='causa_muerte']" ).prop('disabled','');
		     	$( "select[name='presencia_agresiones']" ).prop('disabled','');
			    }else
			    {
				    $( "select[name='tipo_arma']" ).prop('disabled','disabled');
			     	$( "select[name='tipo_arma']" ).prop('selectedIndex', 0);
			     	$( "input[name='otro_tipo_arma']" ).val('');
			        $( "input[name='otro_tipo_arma']" ).prop('readonly',true);

			        $( "select[name='causa_muerte']" ).prop('disabled','disabled');
			     	$( "select[name='causa_muerte']" ).prop('selectedIndex', 0);

			     	$( "select[name='presencia_agresiones']" ).prop('disabled','disabled');
			     	$( "select[name='presencia_agresiones']" ).prop('selectedIndex', 0);
			    }
		    }
		});

      //$( "#edad" ).keyup(function() {
      $('#edad').on('input', function () {
            this.value    = this.value.replace(/[^0-9]/g,''); //validamos solo números
            var valorEdad =  this.value;
            $( "select[name='rango_edad']" ).prop('selectedIndex', 0);
            if(this.value.length > 0){
                $( "#rango_edad option" ).each(function() {
                      var textoCampo  = $(this).text();
                      var tmp         = textoCampo.split("-");
                      if( (parseInt(tmp[0]) <= parseInt(valorEdad)) && (parseInt(valorEdad) <= parseInt(tmp[1])) ){
                          $( this ).attr('selected', true);
                      }
        				});
            }
      });

      $( "#rango_edad" ).change(function() {
            var textoCampo  = $('select[name="rango_edad"] option:selected').text();
            var tmp         = textoCampo.split("-");
            var valorEdad   = $('#edad').val();
            if(valorEdad.length > 0)
            {
                if( (parseInt(tmp[0]) <= parseInt(valorEdad)) && (parseInt(valorEdad) <= parseInt(tmp[1])) ){}else{
                    $('#edad').val('');
                }
            }
      });

    //==================================
		//GUARDAR CUESTIONARIO
    //==================================
		$("#cuestionario").submit(function(event)
		{
					var cambia_img_cuestionario = false;
					event.preventDefault(); //detenemos la accion
					var post_url       = $(this).attr("action"); //get form action url
					var request_method = $(this).attr("method"); //get form GET/POST method
					var form_data      = $(this).serialize(); //Encode form elements for submission
					var newForData     = new FormData( this ); //Para que nos envie el input file

					$.ajax({
							 type:request_method,
							 enctype: 'multipart/form-data',
						     processData: false,  // Important!
						     contentType: false,
						     cache: false,
							 url:post_url,
							 data:newForData,
							 dataType: "json",
							 beforeSend: function(){
									 $('#guardarCuestionario').val("Guardando..."); // Para input de tipo button
									 $('#guardarCuestionario').prop("disabled","disabled");
							 },
							 complete:function(){
									 $('#guardarCuestionario').val("Guardar"); // Para input de tipo button
									 $('#guardarCuestionario').prop("disabled","");
							 },
							 success: function(data){

                      //Validamos la sesion
                      if(typeof data.session != 'undefined' && data.session == 'TIMEOUT'){
                            logOutFundacionArcoiris();
                      }

											$('#idCuestionario').val( data.cuestionario );
											$('#folio_ficha').val( pad( data.cuestionario, 10, 0) ); // quitamos los ceros a la izquierda
											if(data.archivo != ''){
												cambia_img_cuestionario = true;
						                      	$('#nombre_archivo_comunicacion').val( data.archivo);
						                      	$("#descargaArchivo").attr("name", data.archivo);
						                         $('#descargaArchivo').val( data.archivo );
						                     }


                      if(data.msgErr != ''){
                          notificacion('top','left',data.msgErr, 4, "ti-alert");
                      }else{

	                      if(cambia_img_cuestionario)
	                      {
	                          //Se muestra el link de descarga y se oculta el botón
	                          $('#div-download-archivo').css('display','');
	                          $('#div-input').css('display','none');
	                      }

                          notificacion('top','left','La información se ha guardado correctamente', 2, "ti-check");
                      }
								 },
							 error: function(data){
								 console.log(data);
									 notificacion('top','left','Hay un problema al guardar la información', 4, "ti-alert");
							 }
					 });
			});

  			//ELIMINAR CUESTIONARIO
  			$('#listCuestionario tbody').on( 'click', '.eliminaRegistro', function () {

  				var rowDeleted = $(this).parents('tr');
  				if(confirm("¿Esta seguro de eliminar el registro?"))
  				{
  						$.ajax({
  								 type:'POST',
  								 url:"eliminaCuestionario",
  								 data:{'folioCuestionario': this.id},
  								 beforeSend: function(){},
  								 complete:function(){},
  								 success: function(data){

  												notificacion('top','left','El cuestionario se ha eliminado correctamente', 2, "ti-check");

  												var table = $('#listCuestionario').DataTable();
  												table
  									        .row( rowDeleted )
  									        .remove()
  									        .draw();
  									 },
  								 error: function(data){
  										 notificacion('top','left','Hay un problema al eliminar la información', 4, "ti-alert");
  								 }
  						 });
  				}
  			});

            jQuery('input[type=file]').change(function()
            {
             	var filename = jQuery(this).val().split('\\').pop();
             	$('.nomArchivoUpload').html( '<i class="ti-upload"></i> &nbsp; ' + filename );
            });

        //LIMPIAMOS LOS CAMPOS TEXTO DEL VALOR 98
        $('.limpiarCampo').each(function (){
            if(this.value == '98')
            {
              this.value = '';
            }
        });


        $("#descargaArchivo").click(function(event){//function descargaArchivo(){
              var archivo    = this.name;
              var nomArchivo = "articulo."+archivo.split('.').pop();
              $.ajax({
                   type:'POST',
                   url:"descargaArchivo",
                   xhrFields: {
                       responseType: 'blob'
                   },
                   data:{'archivo': archivo},
                   success: function(data){
                           var a = document.createElement('a');
                           var url = window.URL.createObjectURL(data);
                           a.href = url;
                           a.download = nomArchivo;
                           a.click();
                           window.URL.revokeObjectURL(url);
                     },
                   error: function(data){
                       notificacion('top','left','Hay un problema al eliminar la información', 4, "ti-alert");
                   }
               });
        });




        if($('#anios_observatorios').length > 0)
    		{
    			$( "#anios_observatorios" ).change(function() {
    		        var nuevoAnio = $(this).val();
    		        $("#anioFiltro").attr("href", "/fundacionArcoiris/excelCuestionario/"+nuevoAnio);

                //Mostramos los registros correspondientes a ese año
                setTimeout(function(){ location.href="/fundacionArcoiris/cuestionario/"+nuevoAnio }, 2000);


    		   });
    		}


  });
  //===============================================================
  //      FUNCIONES
  //===============================================================
			//Funcion para rellenar los ceros a la izquierda
			function pad(input, length, padding) {
				var str = input + "";
				return (length <= str.length) ? str : pad(padding+str, length, padding);
			}
