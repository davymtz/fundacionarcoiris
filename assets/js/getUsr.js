$( document ).ready(function() {

  $('#listUser').DataTable({
  "columns": [
      { "orderable": true },
      { "orderable": true },
      { "orderable": true },
      { "orderable": true },
      { "orderable": true },
      { "orderable": false }
    ],
  	"language": {
          "lengthMenu": "Registros _MENU_",
          "zeroRecords": "No hay informacion disponible",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay informacion disponible",
          "infoFiltered": "(filtered from _MAX_ total records)",
          "search":         "Búsqueda:",
          "paginate": {
            "first":      "First",
  		"last":       "Last",
          "next":       "Siguiente",
          "previous":   "Anterior"
        },

    }
  });
});

$(".btnEditUser").on("click",function (event){
  event.preventDefault();
  valuebtn = $(this).data("valueedit");
  estatusbtn = $(this).data("valueestatus");
  $.ajax({
    type: "POST",
    url: "getAjaxInfoUsuario",
    dataType: "json",
    data: {"datausr":valuebtn,"dataestatus":estatusbtn}
  }).done(function (data){
    console.log(data);
    if(data.Error){
      $.notify({
        message: data.message
      },{
          type: "danger",
          timer: 4000,
          placement: {
              from: "top",
              align: "left"
          }
      });
    } else {
      $("input[name='idu']").val(valuebtn);
      $("#perfilEdit").val(data.message.perfil);
      $("#estadoEdit").val(data.message.estado);
      $("#edit_email").val(data.message.correo);
      $(".spanUsuario").text(data.message.usuario);
    }
  }).fail(function (fail){
    console.log("Fail",fail);
  });
});
$(".btnCancelar").on("click",function (event){
  event.preventDefault();
  $("input[name='idu']").val("");
  $("#perfilEdit").val("");
  $("#estadoEdit").val("");
  $("#edit_email").val("");
  $(".spanUsuario").text("");
});
$(".btnActualizar").on("click",function (event){ event.preventDefault(); $("#actualizarAdminUsuario").submit(); });
$(".btnEraseUser").on("click",function (event){
  event.preventDefault();
  var rowDeleted = $(this).parents('tr');
  valuebtn = $(this).data("valueerase");
  var result = confirm("Estás seguro que deseas eliminar al usuario? \nNo podrá recuperarlo posteriormente");
  if(result){
    $.ajax({
      type: "POST",
      url: "getDeleteUsuario",
      dataType: "json",
      data: {"datausr":valuebtn}
    }).done(function (data){
      $.notify({
        message: data.message
      },{
          type: data.typeMessage,
          timer: 4000,
          placement: {
              from: "top",
              align: "left"
          }
      });
      if(!data.Error){
        var table = $('#listUser').DataTable();
        table.row( rowDeleted ).remove().draw();
      }
    }).fail(function (fail){
      console.log("Fail",fail);
    });
  }
});
