$( document ).ready(function() {
    $( document ).on( "click", ".btn-mas-noticia", function() {
	    console.log('enviando');
      var f = document.createElement("form");
      f.setAttribute('method',"POST");
      f.setAttribute('action',"masNoticias");
      var i = document.createElement("input"); //input element, text
      i.setAttribute('type',"hidden");
      i.setAttribute('name','noticia');
      i.setAttribute('value',this.id);
      document.body.appendChild(f);
      f.appendChild(i);
      f.submit();
    });
  });



// Eventos
if($("#paginator_view").length > 0 && $("#pagination-noticias").length > 0){
  var $paginator_view = $("#paginator_view");
  var defaultOptionsEvento  = {
    totalPages: 20
  }
  $paginator_view.twbsPagination(defaultOptionsEvento);
  $.ajax({
    method: "POST",
    url: "getAjaxInfoPaginador",
    dataType: "json",
    data: { page: 1 }
  }).done(function (data){
    if(data.results.length == 0) {
      $paginator_view.twbsPagination('destroy');
      $($paginator_view).hide();
      return false;
    }

    var currentPage = $paginator_view.twbsPagination('getCurrentPage');
    $paginator_view.twbsPagination('destroy');
    $paginator_view.twbsPagination($.extend({}, defaultOptionsEvento, {
        startPage: currentPage,
        totalPages: data.count,
        onPageClick: function (event,page){
          $.ajax({
            method: "POST",
            url: "getAjaxInfoPaginador",
            dataType: "json",
            data: { page: page }
          }).done(function (data){
            //console.log(data);
            if(data.results.length > 0)
              build_eventCard(data);
          }).fail(function (fail){
            console.log(fail);
          });
        }
    }));
    if(data.results.length > 0)
      build_eventCard(data,1);
  }).fail(function (fail){
    console.log(fail);
  });

  //Noticias
  var $pagination_noticias = $("#pagination-noticias");
  var defaultOptionsNoticia  = {
    totalPages: 5
  }
  $pagination_noticias.twbsPagination(defaultOptionsNoticia);
  $.ajax({
    method: "POST",
    url: "getAjaxNoticiaPaginador",
    dataType: "json",
    data: { page: 1 }
  }).done(function (info){
    if(info.results.length == 0) {
      $pagination_noticias.twbsPagination('destroy');
      $($pagination_noticias).hide();
      return false;
    }

    var currentPage = $pagination_noticias.twbsPagination('getCurrentPage');
    $pagination_noticias.twbsPagination('destroy');
    $pagination_noticias.twbsPagination($.extend({}, defaultOptionsNoticia, {
        startPage: currentPage,
        totalPages: info.count,
        onPageClick: function (event,page){
          $.ajax({
            method: "POST",
            url: "getAjaxNoticiaPaginador",
            dataType: "json",
            data: { page: page }
          }).done(function (info){
            //console.log(data);
            if(info.results.length > 0)
              build_noticeCard(info);
          }).fail(function (fail){
            console.log(fail);
          });
        }
    }));
    if(info.results.length > 0)
      build_noticeCard(info);
  }).fail(function (fail){
    console.log(fail);
  });
}

// Funciones eventos y noticias
function build_eventCard(data,id_selected=""){
  $("#publish_eventos").empty();
  $.each(data.results, function (k, v){
    $("#publish_eventos").append(
      '<div class="box box-eventos">'+
        '<div class="panel panel-eventos">'+
          '<div class="title">'+
            '<h1>'+v.titulo+'</h1>'+
            '<h2 style="widht:auto;height:2rem;">Fecha del evento: '+formateaFechaEventoPublicado(v.fecha_evento)+'</h2>'+
          '</div>'+
          '<div class="content">'+
            '<p style="widht:auto;height:2rem;">'+v.descripcion+'</p>'+
          '</div>'+
        '</div>'+
      '</div>');
  });
}

function formateaFechaEventoPublicado(fecha){
  meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
  date_split = fecha.split(" ");
  c_split = date_split[0].split("-");

  h_split = date_split[1].split(":");
  return (c_split[2]+" de "+meses[c_split[1]-1]+" de "+c_split[0]+", a las "+h_split[0]+":"+h_split[1]+" hrs.");
}

//================================================================
//================================ NOTICIAS
//================================================================

function build_noticeCard(data,id_selected="")
{
  //console.log(data);
  //limpiamos el div
  $("#publish_noticias").empty();
  $.each(data.results, function (k, v){
   // console.log('noticias');
    $("#publish_noticias").append(
      '<div class="panel">'+
      		'<div class="title">'+
			'<h1>'+v.titulo+'</h1>'+
				'<h2>'+formateaFechaEventoPublicado(v.fecha_publicacion)+'</h2>'+
		'</div>'+
		'<div class="content">'+
          		'<img src="assets/img/puno.jpg" width="190px" alt="" />'+
			'<p class="resumen">'+cortarTextoConPuntos(v.descripcion, 850)+'<br /></p>'+
			'<p></p><br />'+
			'<button type="button" role="button" class="button floatLeft btn-mas-noticia" id="'+v.idnoticia+'">'+
             'M&aacute;s'+
       '</button>'+
		'</div>'+
       '</div>');
  });
}

function cortarTextoConPuntos(texto, limite)
{
  var puntosSuspensivos = "...";
  if(texto.length > limite)
  {
    texto = texto.substring(0,limite) + puntosSuspensivos;
  }
    return texto;
}
