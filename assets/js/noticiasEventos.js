$( document ).ready(function() {

  $('#listNoticias').DataTable({
    //"bSort" : false,
    "columns": [
        { "orderable": true },
        { "orderable": true },
        { "orderable": true },
        { "orderable": false },
      ],
      "language": {
            "lengthMenu": "Registros _MENU_",
            "zeroRecords": "No hay informacion disponible",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay informacion disponible",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "search":         "Búsqueda:",
            "paginate": {
              "first":      "First",
        "last":       "Last",
            "next":       "Siguiente",
            "previous":   "Anterior"
          },

      }
    });

  $(".btnEraseEvento").on("click", function (event){
    event.preventDefault();
    value = $(this).data("valueerase");
    rowDeleted = $(this).parents('tr');
    confirmDialog = confirm("¿Estas seguro que desea eliminar este registro?");
    if(confirmDialog){
      $.ajax({
        "url": "eliminarEvento",
        "method": "POST",
        dataType: "json",
        data: {ideliminar: value}
      }).done(function (data){
        console.log(data);
        $.notify({
          message: data.message
        },{
            type: data.typeMessage,
            timer: 4000,
            placement: {
                from: "top",
                align: "left"
            }
        });
        if(!data.Error){
          var table = $('#listNoticias').DataTable();
          table.row( rowDeleted ).remove().draw();
        }
      }).fail(function (error){
        console.log(error);
      });
    }
  });

    //Inicializamos calendario
    	new Pikaday(
	    {
	        field: document.getElementById('fecha_publicacion_n'),
	        //field: $('.fecha_publicacion_n'),
	        firstDay: 1,
	        i18n: {
	            previousMonth : 'Anterior',
	            nextMonth     : 'Siguiente',
	            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	            weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	            weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb']
	        },
	        format: 'DD/MM/YYYY',
	        theme: 'triangle-theme'
	    });

	    new Pikaday(
	    {
	        field: document.getElementById('fecha_eventoE'),
	        //field: $('.fecha_publicacion_n'),
	        firstDay: 1,
	        i18n: {
	            previousMonth : 'Anterior',
	            nextMonth     : 'Siguiente',
	            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	            weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	            weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb']
	        },
	        format: 'DD/MM/YYYY',
	        theme: 'triangle-theme'
	    });

	    new Pikaday(
	    {
	        field: document.getElementById('fecha_publicacionE'),
	        //field: $('.fecha_publicacion_n'),
	        firstDay: 1,
	        i18n: {
	            previousMonth : 'Anterior',
	            nextMonth     : 'Siguiente',
	            months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
	            weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	            weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb']
	        },
	        format: 'DD/MM/YYYY',
	        theme: 'triangle-theme'
	    });
});
