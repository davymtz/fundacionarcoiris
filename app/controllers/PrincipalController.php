<?php
class PrincipalController extends Controller
{

  	function __construct()
	  {
	    parent::__construct();
      $this->grafica      = new GraficaModel($this->db);
      $this->EstadoModel  = new EstadoModel($this->db);
      $this->noticias     = new NoticiaModel($this->db);
      $this->evento_model = new EventoModel($this->db);

	  }
	  function beforeroute(){

		  $this->f3->set('header', 'header.html' );
      $this->f3->set('footer', 'footer.html' );
      $this->f3->set('carrusel', 'footer.html' );


	  }

	  function afterroute(){}

	  function renderTemplate( $template ){
	    echo Template::instance()->render( $template );
	  }

    function graficas(){
      $anio    =  date('Y');
      $anio    =  $anio*1;
      $finAnio = 2014;
      $anios   = array();
      $year    = $this->getYear(); 

      for ( $x= $finAnio;  $x <= $anio; $x++ ){
        array_push($anios, $x);
      }
      rsort($anios);
      $this->f3->set('year', $anios);
      $this->f3->set('casos', $this->grafica->countCuestionario($year));
      $this->f3->set('estados', $this->EstadoModel->all() );
      $this->f3->set('activeG', 'active' );
      $this->f3->set('content', 'graficas.html' );
      $this->renderTemplate( 'template.html' );
    }
    function listaGrafica(){

      echo json_encode(array("Error"=>false,"message"=>array(
        $this->grafica->all( ) )),JSON_UNESCAPED_UNICODE);
    }

    function estadosGrafica(){
			$tipo_mapa   = $_POST["tipo_mapa"];
			if ( $tipo_mapa == 1  ){
				echo json_encode(array("Error"=>false,"message"=>array(
					$this->grafica->mapaFiltro ())),JSON_UNESCAPED_UNICODE);
			}
			if ( $tipo_mapa == 2  ){
				echo json_encode(array("Error"=>false,"message"=>array(
					$this->grafica->estadosGrafica ())),JSON_UNESCAPED_UNICODE);
			}

    }
    function tipoCrimenGral (){
      $filtro         = $_POST["filtro"];

      echo json_encode(array("Error"=>false,"message"=>array(
        $this->grafica->tipoCrimenGral() )),JSON_UNESCAPED_UNICODE);
    }
    function valoresGrafica (){
			echo json_encode(array("Error"=>false,"message"=>array(
				$this->grafica->valoresGrafica() )),JSON_UNESCAPED_UNICODE);
    }

    function getAjaxInfoPaginador(){
      $pagina = $this->f3->get("POST.page");
      $result_page = $this->evento_model->paginate_evento($pagina-1);
      echo json_encode($result_page,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    function getAjaxNoticiaPaginador(){
      $pagina = $this->f3->get("POST.page");
      $result_page = $this->noticias->paginate_noticia($pagina-1);
      echo json_encode($result_page,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    }

    function masNoticias(){
        $idNoticia = $this->f3->get("POST.noticia");
        $this->noticias->infoNoticia( $idNoticia );
		    $this->f3->set( 'infoNoticia',  $this->noticias);
		    $this->f3->set( 'fechaCastellano',
		        function ( $fecha )
		        {
		          $fecha = substr($fecha, 0, 10);
		          $numeroDia = date('d', strtotime($fecha));
		          $dia = date('l', strtotime($fecha));
		          $mes = date('F', strtotime($fecha));
		          $anio = date('Y', strtotime($fecha));
		          $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
		          $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
		          $nombredia = str_replace($dias_EN, $dias_ES, $dia);
		          $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		          $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		          $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
		          return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;
		        }
		    );

      $this->f3->set('activeN', 'active' );
      $this->f3->set('content', 'masNoticias.html' );
      $this->renderTemplate( 'template.html' );

    }

		function principal(){
		    $this->f3->set('activeN', 'active' );
		    $this->f3->set('content', 'noticias.html' );
		    $this->renderTemplate( 'template.html' );
  }
  function getYear(){
    $year =  $_POST["year"];
    return $year; 
  }
}
