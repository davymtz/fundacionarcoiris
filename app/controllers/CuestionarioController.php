<?php
class CuestionarioController extends Controller
{
	  private $user;
      private $edo;
      private $pais;
      private $cuestionario;
	  public  $typesArchivos = array('png','pdf','jpg','jpeg','bmp');
	  public  $nombreArchivo = '';

	  private $situacionDelCaso = array(   98 => 'No se específica / Sin información'
		  									 , 1  => 'En proceso'
		  									 , 2  => 'Abandonado'
		  									 , 3  => 'Concluido'
		                                   , 99 => 'No fue posible identificarlo');

       public $sinInfo = array( '98' => 'No se específica / Sin información',
       							 '99' => 'No fue posible identificarlo');

	  function __construct()
	  {
	    parent::__construct();
	    $this->user         = new User($this->db);
	    $this->edo          = new EstadoModel($this->db);
	    $this->pais         = new PaisModel($this->db);
	    $this->cuestionario = new CuestionarioModel($this->db);

	    $this->f3->set('nombreUsuario',$this->f3->get("SESSION.user")['name']." ".$this->f3->get("SESSION.user")['apPatern']." ".$this->f3->get("SESSION.user")['apMatern']);//USUARIO
	    $this->f3->set('idUsuario',$this->f3->get("SESSION.user")['idusuario']);
	    $this->f3->set('edoUsuario',$this->f3->get("SESSION.user")['idestado']);
	    $this->f3->set('perfilUsuario',$this->f3->get("SESSION.user")['idperfil']);
        $this->f3->set('seleccionadoC','active');
	    $this->f3->set('header', 'admin/header.html' );
	    $this->f3->set('footer', 'admin/footer.html' );
	    $this->f3->set('nav', 'admin/nav.html' );
	  }

	  function  msgSinInfo( $key ){
		  if(!empty($this->sinInfo[ $key ]))
		  {
		  	return  $this->sinInfo[ $key ];
		  }else{
			  return $key;
		  }

	  }

		function setnameArch($val)
		{
			 $this->nombreArchivo = $val;
		}

		//Obtiene la extension de un archivo
		function extensionArch($str)
	  {
			  $pos = strripos($str,".");
			  if($pos === false){
				  $ext =  "error";
			  }else{
				  $ext   = substr($str,($pos+1));
			  }
			  $ext = strtolower($ext);
			  return $ext;
	  }


		function guardarArchivos( $infoFiles )
		{
				if(!empty($infoFiles["tmp_name"]))
				{
							$dir = 'archivos/articulos';
						  //verificamos si no hay error de tamaño en el archivo
						  if($infoFiles["error"] == 2)
						  {
							  return "ERR_TAM";
						  }
						  //verificamos si no hay error de tamaño en el archivo
						  if($infoFiles["error"] == 4)
						  {
							  return "ERR_ARCH";
						  }

							$ext = $this->extensionArch($infoFiles["name"]);
							if(!in_array( $ext, $this->typesArchivos )){
									return "ERR_EXT";
							}

					    $tmp_name = $infoFiles["tmp_name"];
							//generamos un nombre nuevo
							$date     = date_create();
					    $archivo  = date_timestamp_get($date).rand(1,3000).".".$ext;

							$this->setnameArch($archivo);
					    $movido = move_uploaded_file($tmp_name, "$dir/$archivo");

					    if(!$movido) return "error";
				}

					 	  return "OK";
		}

		//Limpia las variables de espacios
		//y si viene vacio coloca un 98
	  function sanitize_post(&$val, $key)
	  {
				//var_dump($val);
			  if( $key != 'folio_ficha' &&  $key != 'nombre_archivo_comunicacion' &&  $key != 'tipo_arma'  &&  $key != 'causa_muerte')
				{
						if(!is_array($val))
						{
							$val = ( empty( trim($val) ) ? 98 : trim($val) );
						}
				}

	  }

	  public function nombreEstado( $idEstado )
	  {
		  $this->edo->getByIdEstado( $idEstado );

		  if(!$this->edo->dry()){
			  	return $this->edo->estado;
		  }else{
			  return $this->msgSinInfo( $idEstado );
		  }
	  }


	  //Listado de cuestionarios registrados
	  public function listaCuestionario()
	  {

		  	    if( $this->f3->get('perfilUsuario') == 2){
			  	    
			  		$this->f3->set( 'listaCuestionario',$this->cuestionario->byIdEdoCuestionario( $this->f3->get('edoUsuario') ) );
			  		
				}else{
					$this->f3->set( 'listaCuestionario',$this->cuestionario->allCuestionario(  ) );
				}

				$this->f3->set('formateaFolio',function ( $accion, $idFolio ){return $this->formateaFolio( $accion, $idFolio );});
				$this->f3->set('nombreEstado',function ( $idEstado ){ return $this->nombreEstado( $idEstado );}); //nombre estado
				$this->f3->set('estatus',function ( $idSituacion ){ return $this->situacionDelCaso[ $idSituacion ]; }); //situacion_del_caso
				$this->f3->set('numDenuncia', function ( $numDenuncia ){ return $this->msgSinInfo( $numDenuncia );}); //NUMERO DE DENUNCIA


				if($this->f3->exists("SESSION.message_flash")) {
			      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
			      $this->f3->clear("SESSION.message_flash");
			    }
			    $this->f3->set('content', 'admin/listaCuestionario.html' );
			    $this->renderTemplate( 'admin/template.html' );
	  }

	  //Template de nuevo cuestionario
	  public function nuevoCuestionario()
	  {
	    	$this->f3->set( 'estados', $this->edo->all() );
			$this->f3->set( 'paises' , $this->pais->all() );
			$this->f3->set( 'fechaHecho' , date('d/m/Y') );
			if( $this->f3->get('perfilUsuario') == 2){
				 $this->f3->set( 'estadosHecho', $this->edo->getByIdEstado( $this->f3->get('edoUsuario')) );
			}
			else{
				$this->f3->set( 'estadosHecho', $this->edo->all()) ;
			}
			$this->f3->set('tipo_de_arma', array() );
			$this->f3->set('causa_de_muerte',array() );

			$this->f3->set('content', 'admin/cuestionario.html' );
			$this->renderTemplate( 'admin/template.html' );
	  }


	  //Template de edita cuestionario
	  public function editaCuestionario(  )
	  {
		        $idCuestionario = $this->f3->get("POST.idCuestionario");
		     	$this->cuestionario->infoCuestionario( $idCuestionario );

		      	$this->f3->set( 'estados', $this->edo->all() );
			  	$this->f3->set( 'paises' , $this->pais->all() );
			  	$this->f3->set( 'info',    $this->cuestionario );
			    $this->f3->set( 'folio',   $this->formateaFolio( 1 ,$this->cuestionario->idcuestionario )); //Asignamos los ceros
			    $otro  = $this->cuestionario->infoCuestionarioOtro( $idCuestionario );
			    $this->f3->set( 'otroInfo', $otro );
			    $this->f3->set( 'fechaHecho' , date('d/m/Y', strtotime($this->cuestionario->fecha_hecho)) );
			    if( $this->f3->get('perfilUsuario') == 2){
				    $this->f3->set( 'estadosHecho', $this->edo->getByIdEstado( $this->f3->get('edoUsuario')) );
			    }
			    else{
				    $this->f3->set( 'estadosHecho', $this->edo->all()) ;
			    }
			    //TIPO ARMA
			    $tipoA = explode(',',$this->cuestionario->tipo_arma);
			    if( count( $tipoA ) > 1)
			    	$this->f3->set('tipo_de_arma', $tipoA );
			    else
			    	$this->f3->set('tipo_de_arma',array($this->cuestionario->tipo_arma));
			    //CAUSA MUERTE
			    $tipoM = explode(',',$this->cuestionario->causa_muerte);
			    if(count( $tipoM ) > 1)
			    	$this->f3->set('causa_de_muerte',$tipoM );
			    else
			    	$this->f3->set('causa_de_muerte',array($this->cuestionario->causa_muerte));

			  //  die();


		    $this->f3->set('content', 'admin/cuestionario.html' );
		    $this->renderTemplate( 'admin/template.html' );
	  }

	  // Agrega/elimina los ceros a la izquerda
	  // del folio   $accion = 1 agregar
	  // $accion = 2 eliminar
	  private function formateaFolio( $accion, $idFolio )
	  {
		  	//La longitud del número debe ser minimo 10 digitos
			if( $accion == 2 ){ //eliminamos ceros a la izquiera
				$idFolio = (int)$idFolio; //ltrim
			}
			else{  //agregamos ceros a la izquierda
			   $idFolio    = str_pad( $idFolio, 10, "0" , STR_PAD_LEFT);
			}

		    return $idFolio;
	  }

	  //Guardado //Edición por ajax
	  public function guardarCuestionario()
	  {
						//validamos la session
						$this->validaSession();
	  	     //Limpia de espacios y coloca el valor 98 a aquellos que no tenga valor
		  	   array_walk($_POST, array($this, 'sanitize_post'));

				    //Verificar esto porque si las variables no estan seleccionadas se envian como nulas
				      $tmpF = explode( '/', $_POST['fecha_hecho'] );
					  $_POST['fecha_hecho']           = date('Y-m-d', strtotime( $tmpF[2]."-".$tmpF[1]."-".$tmpF[0] ) );
					  $_POST['sexo']                  = (empty($_POST['sexo']) ? 98 : $_POST['sexo']);
					  $_POST['tipo_crimen']           = (empty($_POST['tipo_crimen']) ? 98 : $_POST['tipo_crimen']);

						$_POST['tipo_arma']             = (empty($_POST['tipo_arma']) ? array(98) : $_POST['tipo_arma']);
						$_POST['causa_muerte']          = (empty($_POST['causa_muerte']) ? array(98) : $_POST['causa_muerte']);

					  $_POST['utilizo_arma']          = (empty($_POST['utilizo_arma']) ? 98 : $_POST['utilizo_arma']);
					  $_POST['persona_detenida'] 	  = (empty($_POST['persona_detenida']) ? 98 : $_POST['persona_detenida']);
					  $_POST['trato_adecuado']   	  = (empty($_POST['trato_adecuado']) ? 98 : $_POST['trato_adecuado']);
					  $_POST['localidad_nacimiento']  = (empty($_POST['localidad_nacimiento']) ? 98 : $_POST['localidad_nacimiento']);
					  $_POST['pais_nacimiento']       = (empty($_POST['pais_nacimiento']) ? 98 : $_POST['pais_nacimiento']);
					  $_POST['medio_comunicacion']    = (empty($_POST['medio_comunicacion']) ? 98 : $_POST['medio_comunicacion']);
					  //TIPO DE ARMA
					  $tipoArma = '';
						if(!empty($_POST['tipo_arma'])){
					  	foreach( $_POST['tipo_arma'] as $info ){
									$tipoArma .= $info.",";
					  	}
						}
					  $_POST['tipo_arma'] = substr($tipoArma, 0, -1);
					  //CAUSA DE MUERTE
					  $causaMuerte = '';
						if(!empty($_POST['causa_muerte'])){
						  foreach( $_POST['causa_muerte'] as $info ){
							$causaMuerte .= $info.",";
						  }
						}
					  $_POST['causa_muerte'] = substr($causaMuerte, 0, -1);


					$nomArchivo = '';
					$msgErr     = '';
					$resArchivo = $this->guardarArchivos( $_FILES['archivo_comunicacion'] );
					switch($resArchivo)
					{
						case 'OK':
									$nomArchivo = $this->nombreArchivo;
							break;
						case 'ERR_TAM':
										$msgErr = 'El archivo supera el tamaño permitido 4MB';
								break;
						case 'ERR_ARCH':
										$msgErr = 'Hay un error con el archivo';
								break;
						case 'ERR_EXT':
										$msgErr = 'La extensión ingresada no es correcta, solo puede subir imagenes o pdf';
								break;
						default:
								break;
					}

					if(!empty($_POST['nombre_archivo_comunicacion']) && $_POST['nombre_archivo_comunicacion'] != 98){
						$nomArchivo = $_POST['nombre_archivo_comunicacion'];
					}

					$idCuestionario = $this->cuestionario->guardaCuestionario( $_POST, $nomArchivo );
					$idFolio        = $this->formateaFolio( 1 ,$idCuestionario ); //Asignamos los ceros
					//==============BITACORA
					$idMovBitacora = (!empty($_POST['folio_ficha'])) ? 5 : 4;
					$params = array(":idusuario_fk" => $this->f3->get('idUsuario'), ":idmovimiento_fk" => $idMovBitacora, ":fecha" => date("Y-m-d H:i:s"), ":meta_data" => json_encode($_POST) );
					$this->bitacora( $params );
					//==============BITACORA

					echo json_encode(array("Error"=>false,"cuestionario"=>$idCuestionario, "archivo" => $nomArchivo, "msgErr" => $msgErr),JSON_UNESCAPED_UNICODE);
	  }

	    public function eliminaCuestionario()
		  {
			  $idCuestionario = $_POST['folioCuestionario'];
			  $this->cuestionario->eliminaCuestionario($idCuestionario);
				//==============BITACORA
				$params = array(":idusuario_fk" => $this->f3->get('idUsuario'), ":idmovimiento_fk" => 6, ":fecha" => date("Y-m-d H:i:s"), ":meta_data" => json_encode($_POST) );
				$this->bitacora( $params );
				//==============BITACORA
			  $this->f3->reroute("cuestionario");
	    }

	    public function excelCuestionario(){

		      $fechaIni    = "2014-01-01";
	          $fechaFin    = date('Y')."-12-31";

	          $paises  =  $this->pais->infoPaises(  );
			  $estados = $this->edo->infoEstados();

	          $layCuestionario = $this->cuestionario->infoCuestionarioBetween( $fechaIni, $fechaFin );
	          if(!empty($layCuestionario))
	          {
	        	$idCuestionarios = implode(",",array_keys($layCuestionario));
						$layOtrosCuestionarios = $this->cuestionario->infoOtrosPorIdCuestionario( $idCuestionarios );
	          }
		    include("xlsInforme.php");
		    //$this->f3->reroute("cuestionario");
	    }

			public function descargaArchivo(){
			    	//echo "aqui estamos";
						$nombreArchivo = $_POST['archivo'];
						include_once('descargarArchivo.php');
			}

}
