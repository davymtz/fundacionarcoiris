<?php
class NoticiaController extends Controller
{

	private $meses = array('01' => 'Enero', '02' => 'Febrero',  '03' => 'Marzo',  '04' => 'Abril',  '05' => 'Mayo',  '06' => 'Junio'
	                      ,'07' => 'Julio', '08' => 'Agosto',  '09' => 'Septiembre',  '10' => 'Octubre',  '11' => 'Noviembre',  '12' => 'Diciembre');

	function __construct()
	{
	    parent::__construct();
	    $this->user     = new User($this->db);
	    $this->edo      = new EstadoModel($this->db);
	    $this->noticia  = new NoticiaModel($this->db);

	    $this->f3->set('nombreUsuario',$this->f3->get("SESSION.user")['name']." ".$this->f3->get("SESSION.user")['apPatern']." ".$this->f3->get("SESSION.user")['apMatern']);//USUARIO
	    $this->f3->set('idUsuario',$this->f3->get("SESSION.user")['idusuario']);
	    $this->f3->set('edoUsuario',$this->f3->get("SESSION.user")['idestado']);
	    $this->f3->set('perfilUsuario',$this->f3->get("SESSION.user")['idperfil']);
		$this->f3->set('seleccionadoN','active');
	    $this->f3->set('header', 'admin/header.html' );
	    $this->f3->set('footer', 'admin/footer.html' );
	    $this->f3->set('nav', 'admin/nav.html' );
	}



	public function nombreEstado( $idEstado )
	{
		$this->edo->getByIdEstado( $idEstado );

		if(!$this->edo->dry()){
			return $this->edo->estado;
		}else{
			return $this->msgSinInfo( $idEstado );
		}
	}


	//Listado de cuestionarios registrados
	public function lista()
	{
		if( $this->f3->get('perfilUsuario') == 2){
			$this->f3->set( 'listaNoticia',$this->noticia->byIdEdoNoticia( $this->f3->get('edoUsuario') ) );
		}else{
			$this->f3->set( 'listaNoticia',$this->noticia->allNoticia(  ) );
		}

		$this->f3->set('nombreEstado',function ( $idEstado ){ return $this->nombreEstado( $idEstado );}); //nombre estado

		$this->f3->set('AnioFiltro', date('Y'));
		$this->f3->set('AnioFiltroAct', date('Y'));

		/*if($this->f3->exists("SESSION.message_flash")) {
		    $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
			$this->f3->clear("SESSION.message_flash");
		}*/
		$this->f3->set('content', 'admin/listaNoticia.html' );
	    $this->renderTemplate( 'admin/template.html' );
	}


	//Template de nuevo cuestionario
	public function nuevaNoticia()
	{
	    	if( $this->f3->get('perfilUsuario') == 2){
				 $this->f3->set( 'estados', $this->edo->getByIdEstado( $this->f3->get('edoUsuario')) );
			}
			else{
				$this->f3->set( 'estados', $this->edo->all()) ;
			}

	    	$this->f3->set( 'fechaPublicacion' , date('d/m/Y') );

			$this->f3->set('content', 'admin/noticia.html' );
			$this->renderTemplate( 'admin/template.html' );
	}


  //Template de edita cuestionario
  public function editaNoticia(  )
  {
	        $idNoticia = $this->f3->get("POST.idNoticia");
	     	$this->noticia->infoNoticia( $idNoticia );

	      	$this->f3->set( 'estados', $this->edo->all() );
		  	$this->f3->set( 'info',    $this->noticia );
		    $this->f3->set( 'fechaPublicacion' , date('d/m/Y', strtotime($this->noticia->fecha_publicacion)) );

		    if( $this->f3->get('perfilUsuario') == 2){
			    $this->f3->set( 'estados', $this->edo->getByIdEstado( $this->f3->get('edoUsuario')) );
		    }
		    else{
			    $this->f3->set( 'estados', $this->edo->all()) ;
		    }

			$this->f3->set('content', 'admin/noticia.html' );
			$this->renderTemplate( 'admin/template.html' );
  }


  //Guardado //Edición por ajax
  public function guardarNoticia()
  {
				//validamos la session
				$this->validaSession();
				
	  		$msgErr    = '';
  	    $tmpF      = explode( '/', $_POST['fecha_publicacion'] );
				$_POST['fecha_publicacion'] = date('Y-m-d', strtotime( $tmpF[2]."-".$tmpF[1]."-".$tmpF[0] ) );
        $idNoticia = $this->noticia->guardaNoticia( $_POST );

				//==============BITACORA
				$idMovBitacora = (!empty($_POST['idNoticia'])) ? 8 : 7;
				$params = array(":idusuario_fk" => $this->f3->get('idUsuario'), ":idmovimiento_fk" => $idMovBitacora, ":fecha" => date("Y-m-d H:i:s"),":meta_data" => json_encode($_POST) );
				$this->bitacora( $params );
				//==============BITACORA

				echo json_encode(array("Error"=>false,"noticia"=>$idNoticia,"msgErr" => $msgErr),JSON_UNESCAPED_UNICODE);
  }

	public function eliminaNoticia()
	{
		$idNoticia = $_POST['idNoticia'];
		$this->noticia->eliminaNoticia($idNoticia);

		//==============BITACORA
		$params = array(":idusuario_fk" => $this->f3->get('idUsuario'), ":idmovimiento_fk" => 9, ":fecha" => date("Y-m-d H:i:s"),":meta_data" => json_encode($_POST) );
		$this->bitacora( $params );
		//==============BITACORA

		$this->f3->reroute("noticias");
	}

}
