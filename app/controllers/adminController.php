<?php
class adminController extends Controller
{

  private $user;

  function __construct()
  {
    parent::__construct();
    $this->user = new User($this->db);
    $this->f3->set('nombreUsuario',$this->f3->get("SESSION.user")['name']." ".$this->f3->get("SESSION.user")['apPatern']." ".$this->f3->get("SESSION.user")['apMatern']);   //USUARIO
    $this->f3->set('header', 'admin/header.html' );
    $this->f3->set('footer', 'admin/footer.html' );
    $this->f3->set('nav', 'admin/nav.html' );
  }

  //public function beforeroute(){}
  //public function afterroute(){}

  public function logout(){
    $params = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 13, ":fecha" => date("Y-m-d H:i:s"),":meta_data" => " Nota: el usuario se deslogueó " );
    $this->bitacora($params);
    $this->f3->clear('SESSION'); // destroys the user SESSION
    $this->f3->clear('COOKIE'); // removes all cookie
    $this->f3->clear('CACHE'); // clears all cache contents
    $this->f3->reroute("login");
  }

  public function timeout(){
      $this->f3->clear('SESSION'); // destroys the user SESSION
      $this->f3->clear('COOKIE'); // removes all cookie
      $this->f3->clear('CACHE'); // clears all cache contents
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"danger","message"=>"Su sesión ha expirado"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("login");
  }

  public function adminHome()
  {
    $this->f3->set('content', 'admin/bienvenida.html' );
    $this->renderTemplate( 'admin/template.html' );
  }

  public function modificarperfil(){
    if($this->f3->exists("SESSION.message_flash")) {
      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
      $this->f3->clear("SESSION.message_flash");
    }
    $this->user->getById($this->f3->get("SESSION.user")["idusuario"]);
    $this->f3->set("usuario",$this->user);
    $this->f3->set('content', 'admin/gestion_usuario/perfilUsuario.html' );
    $this->renderTemplate( 'admin/template.html' );
  }

  public function actionmodificarperfil(){
    $this->user->getById($this->f3->get("POST.idu"));
    if(!$this->user->dry()){
      $this->user->nombre = $this->f3->get("POST.name");
      $this->user->apellidopat = $this->f3->get("POST.appaterno");
      $this->user->apellidomat = $this->f3->get("POST.apmaterno");
      $this->user->email = $this->f3->get("POST.email");
      $this->user->organizacion = $this->f3->get("POST.organizacion");

      if(!$this->user->existsEmail($this->f3->get("POST.idu"),$this->f3->get("POST.email"))) {
        $this->user->update();
        $this->f3->get("SESSION.user")["name"] = $this->user->nombre;
        $this->f3->get("SESSION.user")["apPatern"] = $this->user->apellidopat;
        $this->f3->get("SESSION.user")["apMatern"] = $this->user->apellidomat;
        // $this->logout();
      } else {
        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"El correo ya existe, intente con otro"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("modificarperfil");
      }
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"Usuario modificado"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("modificarperfil");
    } else {
      echo "El usuario seleccionado no existe";
    }
  }

}
