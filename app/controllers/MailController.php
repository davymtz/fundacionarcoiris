<?php
  require 'PHPMailer/src/PHPMailer.php';
  require 'PHPMailer/src/Exception.php';
  require 'PHPMailer/src/SMTP.php';

  class MailController{
    protected $f3;
    private $mail;

    function __construct($f3){
      $this->f3 = $f3;
      $this->mail = new PHPMailer\PHPMailer\PHPMailer(true);
      try {
        $this->mail->isSMTP();
        $this->mail->SMTPDebug = 3;
        $this->mail->SMTPAuth = true;
        $this->mail->Host = $this->f3->get("P_HOST");
        $this->mail->Username = $this->f3->get("P_USER");
        $this->mail->Password = $this->f3->get("P_PWD");
        $this->mail->SMTPSecure = $this->f3->get("P_SMTPSECURE");
        $this->mail->Port = $this->f3->get("P_PORT");
        $this->mail->setFrom('notificaciones@fundacionarcoiris.org.mx',"Notificaciones Fundación Arcoiris");
      } catch(Exception $e) {
        $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$e->getMessage(),$this->f3->get("IP"));
        return false;
      } catch (\Exception $e) { //The leading slash means the Global PHP Exception class will be caught
          $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$e->getMessage(),$this->f3->get("IP"));
          return false;
      }
    }

    public function enviarCorreoRecuperarPWD($recipent,$data){
      try {
        $templateEmail = file_get_contents("app/views/emailTemplate.html");
        $templateEmail = str_replace('%BASE%', $this->f3->get("BASE"), $templateEmail);
        $templateEmail = str_replace('%USUARIO%', $data["usuario"], $templateEmail);
        $templateEmail = str_replace('%IDUSUARIO%', bin2hex(base64_encode($data["idusuario"])), $templateEmail);
        $templateEmail = str_replace('%NOMBREUSUARIO%', $data["nombreusuario"], $templateEmail);
        $templateEmail = str_replace('%CORREO%', $data["usuario"], $templateEmail);
        $templateEmail = str_replace('%HOST%', $this->f3->get("HOST"), $templateEmail);
        $templateEmail = str_replace('%TOKEN%', base64_encode($data["token"]), $templateEmail);
        $this->mail->addAddress($recipent, "");
        $this->mail->isHTML(true);
        $this->mail->msgHTML($templateEmail);
        $this->mail->AddEmbeddedImage('assets/img/LogoObservatorio.png', 'logo_2u');
        //$this->mail->Body = "Esto es un texto de prueba";
        $this->mail->Subject = "Recuperación de contraseña";
        $this->mail->CharSet = "utf-8";
        $this->mail->send();
        return true;
      } catch(Exception $e){
        $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$e->getMessage(),$this->f3->get("IP"));
        return false;
      }
    }

    public function notificacionCrearNuevoUsuario($recipent,$data){
      try{
        $templateEmail = file_get_contents("app/views/emailTemplate0.html");
        $templateEmail = str_replace('%CORREO%', $recipent, $templateEmail);
        $templateEmail = str_replace('%USUARIO%', $data["usuario"], $templateEmail);
        $templateEmail = str_replace('%PERFIL%', $data["nombrePerfil"], $templateEmail);
        $templateEmail = str_replace('%ESTADO%', $data["nombreEstado"], $templateEmail);
        $templateEmail = str_replace('%BASE%', $this->f3->get("BASE"), $templateEmail);
        $templateEmail = str_replace('%HOST%', $this->f3->get("HOST"), $templateEmail);
        $templateEmail = str_replace('%TOKEN%', base64_encode($data["token"]), $templateEmail);
        $this->mail->addAddress($recipent, ""); // Test
        $this->mail->isHTML(true);
        $this->mail->msgHTML($templateEmail);
        $this->mail->AddEmbeddedImage('assets/img/LogoObservatorio.png', 'logo_2u');
        //$this->mail->Body = "Esto es un texto de prueba";
        $this->mail->Subject = "Recuperación de contraseña";
        $this->mail->CharSet = "utf-8";
        $this->mail->send();
        return true;
      } catch(Exception $msg) {
        $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$msg->getMessage(),$this->f3->get("IP"));
        return false;
      }
    }

    private function generaLogs($usuario,$accion,$origen){
			$hora = str_pad(date("H:i:s"),10," ");
			$usuario = strtoupper(str_pad($usuario,15," "));
			$accion = strtoupper(str_pad($accion,50," "));
			$cadena = $hora.$usuario.$accion.$origen;
			$filename = "log_".date("dmY");
			$f = fopen("logs/$filename.txt","a");
			fputs($f,$cadena."\r\n") or die("No se pudo crear o insertar el fichero");
			fclose($f);
		}
  }
