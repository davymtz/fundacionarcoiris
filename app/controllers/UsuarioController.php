<?php
class UsuarioController extends Controller {
  private $user;
  private $estado;

  function __construct(){
    parent::__construct();
    $this->user = new User($this->db);
    $this->estado = new EstadoModel($this->db);
    $this->f3->set('nombreUsuario',$this->f3->get("SESSION.user")['name']." ".$this->f3->get("SESSION.user")['apPatern']." ".$this->f3->get("SESSION.user")['apMatern']);   //USUARIO
    $this->f3->set('seleccionadoU','active');
    $this->f3->set('header', 'admin/header.html' );
    $this->f3->set('footer', 'admin/footer.html' );
    $this->f3->set('nav', 'admin/nav.html' );
  }

  function beforeroute(){}

  function afterroute(){}

  private function isAdmin(){
    if($this->f3->get("SESSION.user")['idperfil'] != 1){
      $this->f3->reroute("admin");
    }
  }

  public function adminHome()
  {
    $this->isAdmin();
    if($this->f3->exists("SESSION.message_flash")) {
      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
      $this->f3->clear("SESSION.message_flash");
    }
    $resultUsers = $this->user->getInfoUsers( $this->f3->get("SESSION.user")["idusuario"] );
    $this->f3->set("usuarios",$resultUsers);
    $this->f3->set('perfiles',$this->user->getAllCatPerfil());
    $this->f3->set('estados',$this->estado->all());
    $this->f3->set('content','admin/gestion_usuario/index.html');
    $this->renderTemplate( 'admin/template.html' );
  }

  public function crearNuevoUsuario(){
    $email = filter_var($this->f3->get("POST.email"), FILTER_SANITIZE_EMAIL);
    if(filter_var($email, FILTER_VALIDATE_EMAIL) && !empty($this->f3->get("POST.perfil")) && !empty($this->f3->get("POST.email"))){
      $this->user->getByEmail($email);
      if($this->user->dry()){
        try{
          $token_generate = $this->generate_token(30);
          $params = array(
            ":idperfil_fk" => $this->f3->get("POST.perfil"),
            ":idestado_fk" => $this->f3->get("POST.estado"),
            ":usuario" => explode("@",$email)[0],
            ":estatus" => "pendiente",
            ":email" => $email,
            ":fecha" => date("Y-n-j"),
            ":token_password" => password_hash($token_generate,PASSWORD_DEFAULT)
          );
          $nombrePerfil = $this->db->exec("SELECT DESCRIPCION FROM cat_perfil WHERE IDPERFIL=?",$this->f3->get("POST.perfil"));
          $nombreEstado = $this->db->exec("SELECT ESTADO FROM cat_estado WHERE IDESTADO=?",$this->f3->get("POST.estado"));
          $this->db->begin();
          $this->db->exec("INSERT INTO usuario VALUES (default,:idperfil_fk,:idestado_fk,:usuario,default,default,default,default,:estatus,:email,:fecha,default,:token_password)",$params);

          $mailController = new MailController($this->f3);
          $result = $mailController->notificacionCrearNuevoUsuario($email,array("nombrePerfil"=>$nombrePerfil[0]["DESCRIPCION"],"nombreEstado"=>$nombreEstado[0]["ESTADO"],"usuario"=>$this->user->usuario,"token"=>$token_generate));
          if($result) {
            $this->db->commit();
            $parametros = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 1, ":fecha" => date("Y-m-d H:i:s"),
              ":meta_data" => json_encode(array("idperfil"=>$this->f3->get("POST.perfil"),"id_estado"=>$this->f3->get("POST.estado"),"email"=>$email)) );
            $this->bitacora($parametros);
            $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"La confirmación de correo del nuevo usuario fue enviado satisfactoriamente"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
            $this->f3->reroute("usuario");
          } else {
            $this->db->rollback();
            $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"Ocurrió un error al crear el nuevo usuario"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
            $this->f3->reroute("usuario");
          }
        } catch(\PDOException $msg) {
          //echo $msg->getMessage();
          $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$msg->getMessage(),$this->f3->get("IP"));
          $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"danger","message"=>"Ocurrió un error mientras se creaba el usuario"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
          $this->f3->reroute("usuario");
        }
      } else {
        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"Ya existe este correo en la base de datos, por favor proporcione otro al administrador"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("usuario");
      }
    } else {
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"La dirección de correo es incorrecta y/o no lleno algunos campos"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("usuario");
    }
  }

  public function activarUsuarioPanel(){
    $token_email = base64_decode($this->f3->get("PARAMS.token"));
    $correo = $this->f3->get("PARAMS.correo");
    $this->user->getByEmail($correo,"pendiente");
    if(!$this->user->dry() && password_verify($token_email,$this->user->token_password)){
        $this->f3->set("valor_campos",$this->db->exec("SELECT U.IDUSUARIO,P.DESCRIPCION,E.ESTADO,U.USUARIO,U.EMAIL
        FROM usuario U
        INNER JOIN cat_perfil P ON P.IDPERFIL = U.IDPERFIL_FK
        INNER JOIN cat_estado E ON E.IDESTADO = U.IDESTADO_FK
        WHERE U.EMAIL=? AND U.ESTATUS=?",array(1=>$this->user->email,2=>"pendiente"))[0]);
      $this->f3->set("btnroute","");
      $this->f3->set("content",'admin/gestion_usuario/activarusuario.html');
      $this->renderTemplate( 'login/loginTemplate.html' );
    } else {
      //echo "No coincide";
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"danger","message"=>"Usted no tiene acceso a la página que intento acceder, favor de contactarse con el administrador"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("/panel");
    }
  }

  public function accionActivarUsuario(){
    $expreg = "/(?=.*\d)(?=.*[\-@_])(?=.*[A-Z])(?=.*[a-z])\S{8,16}/";
    // Verificar expresión regular
    if(preg_match($expreg,$this->f3->get("POST.password")) === false){
      $this->f3->set("SESSION.message_flash",json_encode(array(
        "render"=>true,
        "type"=>"danger",
        "message"=>"Las contraseñas no siguen el patrón de contraseña segura"),
        JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)
      );
      $this->f3->reroute("login");
    } // termina verificar contraseña segura
    $this->user->getById($this->f3->get("POST._num_ramdon"),"pendiente");
    if(!$this->user->dry()){
      similar_text($this->f3->get("POST.password"),$this->f3->get("POST.password_verify"),$percent);
      if($percent == 100){
        $this->user->usuario = $this->f3->get("POST.usuario");
        $this->user->nombre = $this->f3->get("POST.nombre");
        $this->user->apellidopat = $this->f3->get("POST.apPatern");
        $this->user->apellidomat = $this->f3->get("POST.apMatern");
        $this->user->organizacion = $this->f3->get("POST.organizacion");
        $this->user->password = password_hash($this->f3->get("POST.password_verify"),PASSWORD_DEFAULT);
        $this->user->estatus = "activo";
        $this->user->token_password = null;
        $this->user->update();

        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"Usuario modificado con éxito!!!"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("/login");
      } else {
        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"Las contraseñas no coinciden"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("/panel");
      }
    } else {
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"Hubo un error al accesar a la página, comuniquese con su Administrador"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("/panel");
    }
  }

  public function getAjaxInfoUsuario(){
    $this->user->reset();
    $this->user->getById($this->f3->get("POST.datausr"),$this->f3->get("POST.dataestatus"));
    if(!$this->user->dry()){
      echo json_encode(array("Error"=>false,"message"=>array(
        "perfil"=>$this->user->idperfil_fk,
        "estado"=>$this->user->idestado_fk,
        "correo"=>$this->user->email,
        "usuario"=>$this->user->usuario)),JSON_UNESCAPED_UNICODE);
    } else {
      echo json_encode(array("Error"=>true,"message"=>"Ocurrió un error al obtener la información del usuario"),JSON_UNESCAPED_UNICODE);
    }
  }

  public function actualizarAdminUsuario(){
    $this->user->getById($this->f3->get("POST.idu"));
    if(!$this->user->dry()){
      if($this->user->idperfil_fk !== $this->f3->get("POST.perfil"))
        $this->user->idperfil_fk = $this->f3->get("POST.perfil");
      if($this->user->idestado_fk !== $this->f3->get("POST.estado"))
        $this->user->idestado_fk = $this->f3->get("POST.estado");

      $this->user->update();

      $parametros = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 2, ":fecha" => date("Y-m-d H:i:s"),
        ":meta_data" => json_encode(array("idusuario"=>$this->user->idusuario,"idperfil"=>$this->f3->get("POST.perfil"),"id_estado"=>$this->f3->get("POST.estado"))) );
      $this->bitacora($parametros);

      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"Usuario actualizado con éxito!!!"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("/usuario");
    } else {
      $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],"No se encontró un registro con el ID enviado",$this->f3->get("IP"));
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"El usuario no se pudo actualizar"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("/usuario");
    }
  }

  public function getDeleteUsuario(){
    $result = $this->db->exec("select idusuario,idperfil_fk,estatus,email from usuario where idusuario=?",$this->f3->get("POST.datausr"))[0];
    if($result != null){
      if($result["estatus"] == "activo"){
          $this->db->exec("update usuario set estatus='eliminado' where idusuario=?",$this->f3->get("POST.datausr"));
      } else if($result["estatus"] == "pendiente") {
        $this->db->exec("delete from usuario where idusuario=?",$this->f3->get("POST.datausr"));
      }
      $params = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 3, ":fecha" => date("Y-m-d H:i:s"),
        ":meta_data" => json_encode(array("idusuario"=>$result["idusuario"],"idperfil"=>$result["idperfil_fk"],"estatus"=>$result["estatus"],"email"=>$result["email"])) );
      $this->bitacora($params);
      echo json_encode(array("Error"=>false,"typeMessage"=>"info","message"=>"El usuario se elimino correctamente."),JSON_UNESCAPED_UNICODE);
    } else {
      $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],"No se encontró ningún usuario con el ID mandado, verifique",$this->f3->get("IP"));
      echo json_encode(array("Error"=>true,"typeMessage"=>"info","message"=>"Ocurrió un error al eliminar el usuario"),JSON_UNESCAPED_UNICODE);
    }
  }

  private function generate_token($longitud){
    if($longitud<4) {
      $longitud = 4;
    }
    return bin2hex(openssl_random_pseudo_bytes(($longitud - ($longitud % 2)) / 2));
  }
}
