<?php

class EventoController extends Controller {

  private $estado_model;
  private $evento_model;

  function __construct()
  {
    parent::__construct();
    $this->estado_model = new EstadoModel($this->db);
    $this->evento_model = new EventoModel($this->db);

    $this->f3->set('nombreUsuario',$this->f3->get("SESSION.user")['name']." ".$this->f3->get("SESSION.user")['apPatern']." ".$this->f3->get("SESSION.user")['apMatern']);//USUARIO
    $this->f3->set('idUsuario',$this->f3->get("SESSION.user")['idusuario']);
    $this->f3->set('edoUsuario',$this->f3->get("SESSION.user")['idestado']);
    $this->f3->set('perfilUsuario',$this->f3->get("SESSION.user")['idperfil']);
    $this->f3->set('seleccionadoE','active');
    $this->f3->set('header', 'admin/header.html' );
    $this->f3->set('footer', 'admin/footer.html' );
    $this->f3->set('nav', 'admin/nav.html' );
  }

  public function lista() {
    if($this->f3->exists("SESSION.message_flash")) {
      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
      $this->f3->clear("SESSION.message_flash");
    }
    $this->f3->set('formateaFecha',function ( $fecha ){return $this->formateaFecha( $fecha );});
    if($this->f3->get("SESSION.user")['idperfil'] != 1){
      $idusuario = $this->f3->get("SESSION.user")["idusuario"];
      $this->f3->set("listaEvento",$this->evento_model->getByIdUsuario($idusuario));
    } else {
      $this->f3->set("listaEvento",$this->evento_model->all());
    }
    $this->f3->set('content', 'admin/listaEventos.html' );
    $this->renderTemplate( 'admin/template.html' );
  }

  public function nuevoEvento(){
    if($this->f3->get("SESSION.user")['idperfil'] != 1){
      $this->f3->set("name_estado",$this->db->exec("SELECT ce.idestado,ce.estado
        FROM usuario u, cat_estado ce where ce.idestado = u.idestado_fk and u.idusuario = ?",$this->f3->get("SESSION.user")['idusuario']));
    } else {
      $estado_all = $this->estado_model->all();
      $this->f3->set("name_estado",$estado_all);
    }
    $this->f3->set('content', 'admin/eventos.html' );
    $this->renderTemplate( 'admin/template.html' );
  }

  public function editaEvento(){
    if($this->f3->exists("SESSION.message_flash")) {
      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
      $this->f3->clear("SESSION.message_flash");
    }
    $idevento = null;
    if(isset($this->f3->get("SESSION.user")["idevento"])) {
      $idevento = $this->f3->get("SESSION.user")["idevento"];
      unset($_SESSION["user"]["idevento"]);
    } else if($this->f3->exists("POST")){
      $idevento = $this->f3->get("POST.btnAccionEdit");
    }
    $this->evento_model->getById($idevento);
    if(!$this->evento_model->dry()){
      if($this->f3->get("SESSION.user")['idperfil'] != 1){
        $this->f3->set("name_estado",$this->db->exec("SELECT ce.idestado,ce.estado
          FROM usuario u, cat_estado ce WHERE ce.idestado = u.idestado_fk and u.idusuario = ?",$this->f3->get("SESSION.user")['idusuario']));
      } else {
        $estado_all = $this->estado_model->all();
        $this->f3->set("name_estado",$estado_all);
      }
      $this->f3->set("data_evento",$this->evento_model);
      $this->f3->set('content', 'admin/edita_evento.html' );
      $this->renderTemplate( 'admin/template.html' );
    } else {
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"No existe el evento con ese identificador"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("eventos");
    }
  }

  public function eliminarAjaxEvento(){
    $ide = $this->f3->get("POST.ideliminar");
    $this->evento_model->getById($ide);
    if(!$this->evento_model->dry()){
      try{
        $parametros = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 12, ":fecha" => date("Y-m-d H:i:s"),
          ":meta_data" => json_encode(array("idevento"=>$this->evento_model->idevento,"titulo"=>$this->evento_model->titulo,"fecha_publicacion"=>$this->evento_model->fecha_publicacion,"fecha_evento"=>$this->evento_model->fecha_evento,"descripcion"=>$this->evento_model->descripcion,"estado"=>$this->evento_model->idestado_fk),JSON_UNESCAPED_UNICODE) );
        $this->bitacora($parametros);
        $this->evento_model->erase();
        echo json_encode(array("Error"=>false,"typeMessage"=>"info","message"=>"El evento se eliminó satisfactoriamente."),JSON_UNESCAPED_UNICODE);
      } catch(\PDOException $e) {
        $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],"Ocurrió un error, ".$e->getMessage(),$this->f3->get("IP"));
        echo json_encode(array("Error"=>true,"typeMessage"=>"warning","message"=>"Ocurrio un error al eliminar el evento."),JSON_UNESCAPED_UNICODE);
      }
    } else {
      echo json_encode(array("Error"=>true,"typeMessage"=>"danger","message"=>"No se encontró el evento."),JSON_UNESCAPED_UNICODE);
    }
  }

  public function actualizaEvento(){
    $idevento = $this->f3->get("POST.idevento");
    $this->evento_model->getById($idevento);
    if(!$this->evento_model->dry()){
      try {
        $this->evento_model->idestado_fk = $this->test_input($this->f3->get("POST.estado"));
        $this->evento_model->titulo = $this->test_input($this->f3->get("POST.tituloEvento"));
        $this->evento_model->fecha_evento = $this->test_input($this->f3->get("POST.fechaEvento"));
        $this->evento_model->fecha_publicacion  = $this->test_input($this->f3->get("POST.fechapublicacion"));
        $this->evento_model->descripcion = $this->test_input($this->f3->get("POST.descripcion_evento"));
        $this->evento_model->update();
        $_SESSION["user"]["idevento"] = $this->evento_model->idevento;

        $parametros = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 11, ":fecha" => date("Y-m-d H:i:s"),
          ":meta_data" => json_encode(array("titulo"=>$this->evento_model->titulo,"fechaPublicacion"=>$this->evento_model->fecha_publicacion,"fechaEvento"=>$this->evento_model->fecha_evento,"descripcion"=>$this->evento_model->descripcion,"idestado"=>$this->evento_model->idestado_fk),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES) );
        $this->bitacora($parametros);

        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"El evento se actualizó satisfactoriamente"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("editaEvento");
      } catch (\PDOException $e) {
        $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],"Ocurrió un error, ".$e->getMessage(),$this->f3->get("IP"));
        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"Ocurrió un error en la actualización de datos"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("editaEvento");
      }
    } else  {
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"Ocurrió un error al obtener evento para su modificación"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("eventos");
    }

  }

  public function guardarEvento(){
    try {
      $this->evento_model->reset();
      $this->evento_model->idevento = "default";
      $this->evento_model->idestado_fk = $this->test_input($this->f3->get("POST.estado"));
      $this->evento_model->idusuario_fk = $this->f3->get("SESSION.user")['idusuario'];
      $this->evento_model->titulo = $this->test_input($this->f3->get("POST.tituloEvento"));
      $this->evento_model->fecha_evento = $this->f3->get("POST.fechaEvento");
      $this->evento_model->fecha_publicacion = $this->f3->get("POST.fechapublicacion");
      $this->evento_model->descripcion = $this->test_input($this->f3->get("POST.descripcion_evento"));
      $this->evento_model->save();
      $_SESSION["user"]["idevento"] = $this->evento_model->idevento;
      $parametros = array(":idusuario_fk" => $this->f3->get("SESSION.user")["idusuario"], ":idmovimiento_fk" => 10, ":fecha" => date("Y-m-d H:i:s"),
        ":meta_data" => json_encode($this->f3->get("POST"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES) );
      $this->bitacora($parametros);
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"El evento se guardó satisfactoriamente"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("editaEvento");
    } catch(\PDOException $e){
      $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],"Ocurrió un error, ".$e->getMessage(),$this->f3->get("IP"));
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"Ocurrió un error en la inserción de datos"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("eventos");
    }
  }

  private function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

  private function formateaFecha($fecha) {
    return date("d/m/Y - H:i \h\\r\s",strtotime($fecha));
  }

}
