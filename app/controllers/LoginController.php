<?php
class LoginController extends Controller
{
  private $user;

  function __construct(){
    parent::__construct();
    $this->user = new User($this->db);
  }
  function beforeroute(){}

  private function verifica_session(){
    if($this->f3->exists("SESSION.user")){
      $this->f3->reroute("/cuestionario");
    }
  }

  public function login()
  {
    $this->verifica_session();
    if($this->f3->exists("SESSION.message_flash")) {
      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
      $this->f3->clear("SESSION.message_flash");
    }
    $this->f3->set("btnroute","<a href='".$this->f3->get('BASE')."/panel'>Regresar</a>");
    $this->f3->set("content",'login/login.html');
    $this->renderTemplate( 'login/loginTemplate.html' );
  }

  public function forgotPassword(){
    $this->verifica_session();
    if($this->f3->exists("SESSION.message_flash")) {
      $this->f3->set("show_message",json_decode($this->f3->get("SESSION.message_flash")));
      $this->f3->clear("SESSION.message_flash");
    }
    $this->f3->set("btnroute","<a href=\"login\">Regresar</a>");
    $this->f3->set("content",'login/forgotPassword.html');
    $this->renderTemplate( 'login/loginTemplate.html' );
  }

  public function sendEmail() {
    if($this->f3->exists("POST.email")){
      $email = filter_var($this->f3->get("POST.email"), FILTER_SANITIZE_EMAIL);
      if(filter_var($email, FILTER_VALIDATE_EMAIL)){
        $this->user->getByEmail($email);
        if(!$this->user->dry()){
          $token_generate = $this->generate_token(30);
          $this->user->token_password = password_hash($token_generate,PASSWORD_DEFAULT);
          $this->user->update();
          $mailController = new MailController($this->f3);
          $mailController->enviarCorreoRecuperarPWD($email,array("nombreusuario"=>$this->user->nombre,"usuario"=>$this->user->usuario,"token"=>$token_generate,"idusuario"=>$this->user->idusuario));
          $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"info","message"=>"¡Enviado!, dirigete a tu correo, para crear tu nueva contraseña"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
          $this->f3->reroute("login");
        } else {
          $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"El correo no existe, verifique!!!"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
          $this->f3->reroute("forgotPassword");
        }
      } else {
        $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"warning","message"=>"La dirección de correo es incorrecta"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->f3->reroute("forgotPassword");
      }
    }
  }

  public function resetPasword(){
    $token_email = base64_decode($this->f3->get("PARAMS.token"));
    $iduser = base64_decode(hex2bin($this->f3->get("PARAMS.user")));
    $this->user->getById($iduser);
    if(!$this->user->dry() && password_verify($token_email,$this->user->token_password)){
      $this->f3->set("content",'login/resetPassword.html');
      $this->f3->set("value_user",$this->f3->get("PARAMS.user"));
      $this->renderTemplate( 'login/loginTemplate.html' );
      /*$this->user->token_password = null;
      $this->user->update();*/
    } else {
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"danger","message"=>"Usted tiene acceso restringido a este portal, favor de contactarse con el administrador"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("/login");
    }
  }

  public function actionResetPassword(){
    if(!$this->f3->exists("POST")){
      $this->f3->reroute("/panel");
    }
    $expreg = '/(?=.*\d)(?=.*[\-@_])(?=.*[A-Z])(?=.*[a-z])\S{8,16}/';
    if(preg_match($expreg,$this->f3->get("POST.password")) === false){
      $this->f3->set("SESSION.message_flash",json_encode(array(
        "render"=>true,
        "type"=>"danger",
        "message"=>"Las contraseñas no siguen el patrón de contraseña segura"),
        JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)
      );
      $this->f3->reroute("login");
    }
    similar_text($this->f3->get("POST.password"),$this->f3->get("POST.password_verify"),$percent);
    if($percent == 100){
      try{
        $iduser = base64_decode(hex2bin($this->f3->get("POST.id_user")));
        $new_password  = password_hash($this->f3->get("POST.password_verify"),PASSWORD_DEFAULT);
        $this->user->getById($iduser);
        $this->user->password = $new_password;
        $this->user->token_password = null;
        $this->user->update();
        $this->f3->set("SESSION.message_flash",json_encode(array(
          "render"=>true,
          "type"=>"success",
          "message"=>"Credenciales modificadas, gracias"),
          JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)
        );
        $this->f3->reroute("login");
      } catch(Exception $message) {
        //"Error:".$message->getMessage()
        $this->f3->set("SESSION.message_flash",json_encode(array(
          "render"=>true,
          "type"=>"danger",
          "message"=>"Usted tiene acceso restringido a este portal"),
          JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)
        );
        $this->f3->reroute("login");
      }
    }
  }

  public function authenticate(){
    $username = $this->f3->get("POST.username");
    $pass = $this->f3->get("POST.pass");
    $this->user->getByUsuario($username);

    if(!$this->user->dry() && password_verify($pass,$this->user->password)){
      $this->f3->set("SESSION.user",
        array("idusuario"=>$this->user->idusuario,
          "idperfil"=>$this->user->idperfil_fk,
          "idestado"=>$this->user->idestado_fk,
          "name"=>$this->user->nombre,
          "apPatern"=>$this->user->apellidopat,
          "apMatern"=>$this->user->apellidomat
        )
      );
      $params = array(":idusuario_fk" => $this->user->idusuario, ":idmovimiento_fk" => 13, ":fecha" => date("Y-m-d H:i:s"),":meta_data" => " -- " );
      $this->bitacora($params);
      //$anioActual = date("Y");
      //$this->f3->route("GET /cuestionario/@AnioFiltro2", "CuestionarioController->listaCuestionario");
      $this->f3->reroute("/cuestionario/");
    } else {
      $this->f3->set("SESSION.message_flash",json_encode(array("render"=>true,"type"=>"danger","message"=>"Usuario y/o contraseña incorrectos, verifique!!!"),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
      $this->f3->reroute("login");
    }
  }

  private function generate_token($longitud){
    if($longitud<4) {
      $longitud = 4;
    }
    return bin2hex(openssl_random_pseudo_bytes(($longitud - ($longitud % 2)) / 2));
  }

}
