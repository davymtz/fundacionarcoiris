<?php
class Controller
{
	  protected $f3;
	  protected $db;
		protected $PHPMailer;
		protected $logger;

		function __construct(){
			date_default_timezone_set("America/Mexico_City");
			$this->f3 = Base::instance();
			$this->f3->set('AnioFiltro2', date("Y"));
			/*var_dump($this->f3->f3->get("SESSION"));
			var_dump($this->f3->ip()." - ".$this->f3->get("IP"));
			var_dump($this->f3->agent()." - ".$this->f3->get("AGENT"));*/
			try {
				$db = new DB\SQL(
		      $this->f3->get('db_dns'),
		      $this->f3->get('db_user'),
		      $this->f3->get('db_pass'),
		      array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION )
		    );
				$this->db = $db;
			} catch (PDOException $msg){
				echo "<pre>";
				echo utf8_encode($msg->getMessage());
				echo "</pre>";
				exit();
			}
	  }

	  function beforeroute(){
			$this->validateSessionRender();
		}

	  function afterroute(){}

		function generaLogs($usuario,$accion,$origen){
			$hora = str_pad(date("H:i:s"),10," ");
			$usuario = strtoupper(str_pad($usuario,15," "));
			$accion = strtoupper(str_pad($accion,50," "));
			$cadena = $hora.$usuario.$accion.$origen;
			$filename = "log_".date("dmY");
			$f = fopen("logs/$filename.txt","a");
			fputs($f,$cadena."\r\n") or die("No se pudo crear o insertar el fichero");
			fclose($f);
			//exit();
		}
		//$idUsuario, $idMovimiento, $consulta
		function bitacora( $params ){
				$this->db->exec("INSERT INTO bitacora VALUES (default,:idusuario_fk,:idmovimiento_fk,:fecha,:meta_data, '".$this->f3->get('IP')."')", $params );
		}

		function validateSessionRender(){
			if($this->f3->get("ERROR") === null){
				if($this->f3->get("SESSION.user") === null) {
					$this->f3->reroute("login");
				}
			} else {
				$this->error();
			}
		}

	  function renderTemplate( $template ){
	    echo Template::instance()->render( $template );
	  }

		public function error(){
		  switch($this->f3->get("ERROR.code")) {
			 case "404":
				if($this->f3->get("SESSION.user") === null) {
					$this->f3->reroute("panel");
				} else {	
				  		$this->renderTemplate('errorHtml/error404.html');
				}  		
				break;

			  default:
					echo $this->f3->get("ERROR.code");
					echo $this->f3->get("ERROR.text");
					echo $this->f3->get("ERROR.status");
				break;
		  }
	  }

		function validaSession(){
	      if(!$this->f3->exists("SESSION.user")) {
	          echo json_encode(array("Error"=>false,"session"=>"TIMEOUT","msgErr" => '' ),JSON_UNESCAPED_UNICODE);
	          exit;
	      }
	  }
}
