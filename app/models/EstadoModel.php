<?php
class EstadoModel extends DB\SQL\Mapper
{
	  public function __construct( DB\SQL $db ){
		  parent::__construct($db,'cat_estado');
	
	  }
	  public function all( ){
	    	$this->load();
	        return $this->query;
	  }
	  
	  public function getByIdEstado( $idEstado ){
		  $this->load(array('idestado=?',$idEstado));
          return $this->query;
	  }
	  
	  public function infoEstados(  )
	  {
		  	  $resultado = array();

			  $sql = "SELECT idestado,estado FROM cat_estado";
			  $res =  $this->db->exec( $sql );

		      if($res)
		      {
		          foreach( $res as $key => $info)
		          {
		              $resultado[$info['idestado']] = $info['estado'];
		          }
		      }
		      return $resultado;
	  }
}
