<?php
  class EventoModel extends DB\SQL\Mapper{
    public function __construct( DB\SQL $db ){
		  parent::__construct($db,'evento');
	  }

    public function all(){
	    $this->load();
	    return $this->query;
	  }

    public function showEvent(){
      $this->load(array("now() >= fecha_publicacion"));
      return $this->query;
    }

    public function getById($id) {
      $this->load(array('idevento=?',$id));
      return $this->query;
    }

    public function getByIdUsuario($idusuario) {
      $this->load(array('idusuario_fk=?',$idusuario));
      return $this->query;
    }

    public function paginate_evento($pos = 0,$size = 4){
      $this->db->exec("select titulo,fecha_evento,descripcion from evento where now() > fecha_publicacion order by fecha_evento");
      $total = $this->db->count();
      $count=ceil($total/$size);
      return [
        "results" => ($pos<$count)?$this->db->exec("select titulo,fecha_evento,descripcion from evento where now() > fecha_publicacion order by fecha_evento limit ? offset ?",array(1=>$size,2=>($pos*$size))):[],
        "total" => $total, // El total de resultados que arrojo la BD
        "limit" => $size, // Cuantos elementos se mostrarán
        "count" => $count, // numero de paginadores habilitados
        "pos" => ($pos<$count)?$pos:0 // La Posicion del paginador
      ];
    }

  }
