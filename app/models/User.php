<?php
  class User extends DB\SQL\Mapper {
    public function __construct(DB\SQL $db){
      parent::__construct($db,"usuario");
    }

    public function all(){
      $this->load();
      return $this->query;
    }

    public function getInfoUsers( $idUsuario ){
        return $this->db->exec("SELECT U.IDUSUARIO,U.IDPERFIL_FK,P.DESCRIPCION,U.IDESTADO_FK,E.ESTADO,U.USUARIO,U.EMAIL,U.ESTATUS
          FROM usuario U
          INNER JOIN cat_perfil P ON U.IDPERFIL_FK = P.IDPERFIL
          INNER JOIN cat_estado E ON U.IDESTADO_FK = E.IDESTADO
          WHERE ESTATUS NOT LIKE 'eliminado' AND U.IDUSUARIO NOT LIKE ?", $idUsuario);
    }

    public function existsEmail($idu,$email){
      $result = $this->db->exec("SELECT count(*) contador from usuario where email=? and idusuario != ?",array($email,$idu));
      if($result[0]["contador"] > 0){
        return true;
      } else {
        return false;
      }
    }

    public function getAllCatPerfil(){
      return $this->db->exec("SELECT idperfil,descripcion FROM cat_perfil");
    }

    public function getByEmail($email, $estatus="activo"){
      $this->load(array('email=? and estatus=?',$email,$estatus));
      return $this->query;
    }

    public function getById($id,$estatus="activo") {
      $this->load(array('idusuario=? and estatus=?',$id,$estatus));
      return $this->query;
    }

    public function getByUsuario($usuario,$estatus="activo") {
      $this->load(array('usuario=? and estatus=?',$usuario,$estatus));
      return $this->query;
    }

    public function add(){
      /*$this->copyFrom('POST');
      $this->save();*/
    }

    public function edit($id) {
      /*$this->load(array('id=?',$id));
      $this->update();*/
    }

    public function delete($id) {
      $this->load(array('id=?',$id));
      $this->erase();
    }

  }
?>
