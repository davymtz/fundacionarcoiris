<?php
class CuestionarioModel extends DB\SQL\Mapper
{
  private $f3;
  public function __construct( DB\SQL $db ){
	  parent::__construct($db,'cuestionario');
    $this->f3 = Base::instance();
  }

  function generaLogs($usuario,$accion,$origen){
    $hora = str_pad(date("H:i:s"),10," ");
    $usuario = strtoupper(str_pad($usuario,15," "));
    $accion = strtoupper(str_pad($accion,50," "));
    $cadena = $hora.$usuario.$accion.$origen;
    $filename = "log_".date("dmY");
    $f = fopen("logs/$filename.txt","a");
    fputs($f,$cadena."\r\n") or die("No se pudo crear o insertar el fichero");
    fclose($f);
    //exit();
  }

	public function allCuestionario( ){
			$this->load();

				return $this->query;
	}

  public function byYearCuestionario( $anioFiltro ){
			$this->load( array( 'YEAR(fecha_hecho) = ?', $anioFiltro ));

				return $this->query;
	}

	public function byIdEdoCuestionario( $idEstado ){
			$this->load( array('localidad_hecho = ?',$idEstado ));

				return $this->query;
	}

	// Obtenemos información pará gráfica de mapas
	public function all(){
	}

	//Obtenemos la información de un cuestionario
	public function infoCuestionario( $idCuestionario )
	{
			$this->load(array('idcuestionario = ?',$idCuestionario ));
				return $this->query;
	}

	  //Obtenemos la información de cuestionario_otro
	  public function infoCuestionarioOtro( $idCuestionario )
	  {
		  	  $resultado = array();

			  $sql = "SELECT nombre_campo,descripcion FROM cuestionario_otro WHERE idcuestionario_fk = ".$idCuestionario;
			  $res =  $this->db->exec( $sql );

		      if($res)
		      {
		          foreach( $res as $key => $info)
		          {
		              $resultado[$info['nombre_campo']] = $info['descripcion'];
		          }
		      }
		      return $resultado;
	  }


	  public function infoCuestionarioBetween( $fechaI, $fechaF )
	  {
		    $resultado = array();

		    $sql = "SELECT * FROM cuestionario WHERE fecha_hecho BETWEEN '{$fechaI}' AND '{$fechaF}'";
			$res =  $this->db->exec( $sql );

			if($res)
			{
				foreach( $res as $key => $info){
					$resultado[ $info['idcuestionario']] = $info;
				}
			}
			return $resultado;
	  }

	  public function infoOtrosPorIdCuestionario( $layIdCuestionario )
	  {
		  $resultado = array();

		  $sql = "SELECT * FROM cuestionario_otro WHERE idcuestionario_fk IN ({$layIdCuestionario})";
		  $res =  $this->db->exec( $sql );

		  if( $res )
		  {
			foreach( $res as $key => $info){
				$resultado[ $info['idcuestionario_fk'] ][$info['nombre_campo']] =  $info['descripcion'];
		    }
		  }
		  return $resultado;
	  }



	  public function guardaCuestionario( $info, $nomArchivo = '' )
	  {
		  $folio = ltrim(trim($info['folio_ficha']));

		  if( empty( $folio )){
			  $idCuestionario =  $this->insertaCuestionario( $info, $nomArchivo );
		  }else
		  {
			  $idCuestionario = $this->editarCuestionario(  $info, $nomArchivo );
		  }

		  return $idCuestionario;
	  }



	  public function insertaCuestionario( $info, $nomArchivo = '' )
	  {

			  $id = '';
		  	$sql = "INSERT INTO cuestionario(
									  `idusuario_fk`,
									  `fecha_hecho`,
									  `nombre_social`,
									  `nombre_legal`,
									  `sexo`,
									  `edad`,
									  `rango_edad`,
									  `orientacion_sexual`,
									  `identidad_genero`,
									  `origen_etnico`,
									  `estado_civil`,
									  `numero_hijos`,
									  `nacionalidad_mexicana`,
									  `localidad_nacimiento`,
									  `municipio_nacimiento`,
									  `pais_nacimiento`,
									  `situacion_migratoria`,
									  `estado_residencia`,
									  `nivel_estudios`,
									  `actividad_laboral`,
									  `trabajo_de_defensa`,
									  `trabajo_de_periodismo`,
									  `pertenece_a_organizacion`,
									  `espacio_lgbt`,
									  `localidad_hecho`,
									  `municipio_hecho`,
									  `tipo_crimen`,
									  `relacion_victima_agresor`,
									  `utilizo_arma`,
									  `tipo_arma`,
									  `causa_muerte`,
									  `presencia_agresiones`,
									  `lugar_sucedio_hecho`,
									  `victima_acompanada`,
									  `lugar_cuerpo_hallado`,
									  `quien_hallo_victima`,
									  `numero_denuncia`,
									  `persona_detenida`,
									  `interpuso_denuncia`,
									  `quien_interpuso_denuncia`,
									  `situacion_del_caso`,
									  `resultado_proceso`,
									  `abandono_proceso`,
									  `medio_comunicacion`,
                    `nota_sobre_medio_comunicacion`,
									  `nombre_archivo_comunicacion`,
									  `comentario_medio_comunicacion`,
									  `trato_adecuado`,
                    `porque_se_trato_adecuadamente`,
									  `divulgacion_noticia`)
						VALUES(
							 ".$info['usuario']."
							, '".$info['fecha_hecho']."'
							, '".addslashes($info['nombre_social'])."'
							, '".addslashes($info['nombre_legal'])."'
							, ".$info['sexo']."
							, ".$info['edad']."
							, ".$info['rango_edad']."
							, ".$info['orientacion_sexual']."
							, ".$info['identidad_genero']."
							, ".$info['origen_etnico']."
							, ".$info['estado_civil']."
							, ".$info['numero_hijos']."
							, ".$info['nacionalidad_mexicana']."
							, ".$info['localidad_nacimiento']."
							, '".mb_strtoupper($info['municipio_nacimiento'],'UTF-8')."'
							, '".$info['pais_nacimiento']."'
							, ".$info['situacion_migratoria']."
							, ".$info['estado_residencia']."
							, ".$info['nivel_estudios']."
							, ".$info['actividad_laboral']."
							, ".$info['trabajo_de_defensa']."
							, ".$info['trabajo_de_periodismo']."
							, ".$info['pertenece_a_organizacion']."
							, ".$info['espacio_lgbt']."
							, ".$info['localidad_hecho']."
							, '".mb_strtoupper($info['municipio_hecho'],'UTF-8')."'
							, ".$info['tipo_crimen']."
							, ".$info['relacion_victima_agresor']."
							, ".$info['utilizo_arma']."
							, '".$info['tipo_arma']."'
							, '".$info['causa_muerte']."'
							, ".$info['presencia_agresiones']."
							, ".$info['lugar_sucedio_hecho']."
							, ".$info['victima_acompanada']."
							, ".$info['lugar_cuerpo_hallado']."
							, ".$info['quien_hallo_victima']."
							, '".mb_strtoupper($info['numero_denuncia'],'UTF-8')."'
							, ".$info['persona_detenida']."
							, ".$info['interpuso_denuncia']."
							, ".$info['quien_interpuso_denuncia']."
							, ".$info['situacion_del_caso']."
							, ".$info['resultado_proceso']."
							, ".$info['abandono_proceso']."
							, ".$info['medio_comunicacion']."
							, '".addslashes($info['nota_sobre_medio_comunicacion'])."'
							, '".$nomArchivo."'
							, '".addslashes($info['comentario_medio_comunicacion'])."'
							, ".$info['trato_adecuado']."
              , '".addslashes($info['porque_se_trato_adecuadamente'])."'
							, ".$info['divulgacion_noticia'].")";

              try {
                $res = $this->db->exec( $sql );
              } catch(PDOException $msg){
                $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$msg->getMessage()." YOLO fAtAlItY: ".$sql,$this->f3->get("IP"));
              }

		    if($res){
		    	$idCuestionario = $this->db->lastInsertId();
		    	//Insertamos en tabla otros
		    	$this->insertarOtros( $idCuestionario, $info );
		    }

		    //var_dump($res);
		    //echo "======".$id;

		    return $idCuestionario;
	  }

	  public function insertarOtros( $idCuestionario, $info = "")
	  {
        if(!empty($info))
        {
    		    foreach( $info as $key => $valor )
    		    {
      			    $strBusca = 'otro_';
      			  	$pos      = stripos( $key, $strBusca );
      			  	$valor    = trim( $valor );
      			  	$nomCampo = str_replace( $strBusca, '', $key ); //Obtenemos el nombre del campo

      			  	if( $pos !== false && !empty($valor))
      			  	{
          					$sql = "INSERT INTO cuestionario_otro(
          													  idcuestionario_fk
          													, nombre_campo
          													, descripcion
          												)VALUES(
          													  ".$idCuestionario."
          													, '".$nomCampo."'
          													, '".addslashes($valor)."'
          												)";
          					$this->db->exec( $sql );
      				  }
    		    }
        }
	  }

	  public function eliminaCuestionario( $idCuestionario )
	  {
		    $res  = $this->eliminarOtros( $idCuestionario );
			  $sql  = "DELETE FROM cuestionario WHERE idcuestionario = ".$idCuestionario;
			  $this->db->exec( $sql );

	  }

	  public function eliminarOtros( $idCuestionario )
	  {
		  $sql  = "DELETE FROM cuestionario_otro WHERE idcuestionario_fk = ".$idCuestionario;
		  $this->db->exec( $sql );
	  }

	  public function editarCuestionario( $info, $nomArchivo = '' )
	  {
      	  $idCuestionario  = $info['idCuestionario'];

		  $sql = "UPDATE cuestionario SET
									  `fecha_hecho` =  '".$info['fecha_hecho']."'
									  , `nombre_social` =  '".addslashes($info['nombre_social'])."'
									  , `nombre_legal` =  '".addslashes($info['nombre_legal'])."'
									  , `sexo` = ".$info['sexo']."
									  , `edad` = ".$info['edad']."
									  , `rango_edad` = ".$info['rango_edad']."
									  , `orientacion_sexual` = ".$info['orientacion_sexual']."
									  , `identidad_genero` = ".$info['identidad_genero']."
									  , `origen_etnico` = ".$info['origen_etnico']."
									  , `estado_civil` = ".$info['estado_civil']."
									  , `numero_hijos` = ".$info['numero_hijos']."
									  , `nacionalidad_mexicana` =  ".$info['nacionalidad_mexicana']."
									  , `localidad_nacimiento` =  ".$info['localidad_nacimiento']."
									  , `municipio_nacimiento` = '".mb_strtoupper($info['municipio_nacimiento'],'UTF-8')."'
									  , `pais_nacimiento` =  '".$info['pais_nacimiento']."'
									  , `situacion_migratoria` = ".$info['situacion_migratoria']."
									  , `estado_residencia` = ".$info['estado_residencia']."
									  , `nivel_estudios` = ".$info['nivel_estudios']."
									  , `actividad_laboral` = ".$info['actividad_laboral']."
									  , `trabajo_de_defensa` = ".$info['trabajo_de_defensa']."
									  , `trabajo_de_periodismo` = ".$info['trabajo_de_periodismo']."
									  , `pertenece_a_organizacion` = ".$info['pertenece_a_organizacion']."
									  , `espacio_lgbt` = ".$info['espacio_lgbt']."
									  , `localidad_hecho` = ".$info['localidad_hecho']."
									  , `municipio_hecho` = '".mb_strtoupper($info['municipio_hecho'],'UTF-8')."'
									  , `tipo_crimen` =  ".$info['tipo_crimen']."
									  , `relacion_victima_agresor` = ".$info['relacion_victima_agresor']."
									  , `utilizo_arma` =  ".$info['utilizo_arma']."
									  , `tipo_arma` = '".$info['tipo_arma']."'
									  , `causa_muerte` = '".$info['causa_muerte']."'
									  , `presencia_agresiones` = ".$info['presencia_agresiones']."
									  , `lugar_sucedio_hecho` = ".$info['lugar_sucedio_hecho']."
									  , `victima_acompanada` = ".$info['victima_acompanada']."
									  , `lugar_cuerpo_hallado` = ".$info['lugar_cuerpo_hallado']."
									  , `quien_hallo_victima` = ".$info['quien_hallo_victima']."
									  , `numero_denuncia` = '".mb_strtoupper($info['numero_denuncia'],'UTF-8')."'
									  , `persona_detenida` = ".$info['persona_detenida']."
									  , `interpuso_denuncia` = ".$info['interpuso_denuncia']."
									  , `quien_interpuso_denuncia` = ".$info['quien_interpuso_denuncia']."
									  , `situacion_del_caso` = ".$info['situacion_del_caso']."
									  , `resultado_proceso` = ".$info['resultado_proceso']."
									  , `abandono_proceso` = ".$info['abandono_proceso']."
									  , `medio_comunicacion` = ".$info['medio_comunicacion']."
									  , `nota_sobre_medio_comunicacion` = '".addslashes($info['nota_sobre_medio_comunicacion'])."'
									  , `nombre_archivo_comunicacion` = '".$nomArchivo."'
									  , `comentario_medio_comunicacion` = '".addslashes($info['comentario_medio_comunicacion'])."'
									  , `trato_adecuado` = ".$info['trato_adecuado']."
                    , `porque_se_trato_adecuadamente` = '".addslashes($info['porque_se_trato_adecuadamente'])."'
									  , `divulgacion_noticia`  = ".$info['divulgacion_noticia']."
					WHERE idcuestionario = ".$idCuestionario;

          //echo "<pre>";echo $sql; echo "</pre>";
          //die();

          try {
            $res = $this->db->exec( $sql );
          } catch(PDOException $msg){
            $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$msg->getMessage()." YOLO fAtAlItY: ".$sql,$this->f3->get("IP"));
          }

          //var_dump($res);
          if( $res ){
                $var1 = $this->eliminarOtros( $idCuestionario );
                $var2 = $this->insertarOtros( $info['idCuestionario'], $info );
          }


          return $idCuestionario;


	  }

}
