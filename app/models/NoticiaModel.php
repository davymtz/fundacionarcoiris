<?php
class NoticiaModel extends DB\SQL\Mapper
{
  private $f3;
  public function __construct( DB\SQL $db ){
	  parent::__construct($db,'noticia');
    $this->f3 = Base::instance();

  }

	public function allNoticia( ){
		$this->load();

		return $this->query;
	}

	public function byIdEdoNoticia( $idEstado ){
			$this->load( array('idestado_fk = ?',$idEstado ));

				return $this->query;
	}


	//Obtenemos la información de una noticia
	public function infoNoticia( $idNoticia )
	{
			$this->load(array('idnoticia = ?',$idNoticia ));
				return $this->query;
	}

	public function guardaNoticia( $info )
	{
		$idnoticia = trim($info['idNoticia']);

		if( empty( $idnoticia )){
		  	$idnoticia =  $this->insertaNoticia( $info );
	  	}else
	  	{
		  	$idnoticia = $this->editarNoticia(  $info );
	  	}

	  	return $idnoticia;
   }



	  public function insertaNoticia( $info )
	  {
			  $idNoticia = '';
		  	$sql = "INSERT INTO noticia(
									  `idusuario_fk`,
									  `idestado_fk`,
									  `titulo`,
									  `descripcion`,
									  `fecha_publicacion`)
						VALUES(
							 ".$info['usuario']."
							, '".$info['estado']."'
							, '".addslashes($info['titulo'])."'
							, '".addslashes($info['descripcion'])."'
							, '".$info['fecha_publicacion']."')";

              try {
                $res = $this->db->exec( $sql );
              } catch(PDOException $msg){
                $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$msg->getMessage()." YOLO fAtAlItY: ".$sql,$this->f3->get("IP"));
              }

		    if($res){
		    	$idNoticia = $this->db->lastInsertId();
		    }
		    return $idNoticia;
	  }

	  public function editarNoticia( $info )
	  {
     	  $idNoticia  = $info['idNoticia'];

		  $sql = "UPDATE noticia SET
									  `idusuario_fk`        =  '".$info['usuario']."'
									  , `idestado_fk`       =  '".$info['estado']."'
									  , `titulo`            =  '".addslashes($info['titulo'])."'
									  , `descripcion`       =  '".addslashes($info['descripcion'])."'
									  , `fecha_publicacion` =  '".$info['fecha_publicacion']."'
					WHERE idnoticia = ".$idNoticia;

          try {
            $res = $this->db->exec( $sql );
          } catch(PDOException $msg){
            $this->generaLogs($this->f3->get("SESSION.user")["idusuario"],$msg->getMessage()." YOLO fAtAlItY: ".$sql,$this->f3->get("IP"));
          }

          if( $res ){
	          return $idNoticia;
          }
	  }

	  public function eliminaNoticia( $idNoticia ){
		    $sql  = "DELETE FROM noticia WHERE idnoticia = ".$idNoticia;
		    $this->db->exec( $sql );
	  }

	  public function paginate_noticia($pos = 0,$size = 4)
	  {
	      $this->db->exec("select titulo,descripcion from noticia");
	      $total = $this->db->count();
	      $count=ceil($total/$size);
	      return [
	        "results" => ($pos<$count)?$this->db->exec("select idnoticia,titulo,descripcion,fecha_publicacion from noticia order by fecha_publicacion DESC limit ? offset ?",
	        array(1=>$size,2=>($pos*$size))):[],
	        "total" => $total,
	        "limit" => $size,
	        "count" => $count,
	        "pos" => ($pos<$count)?$pos:0
	      ];
    }

}
