<?php
class PaisModel extends DB\SQL\Mapper
{
  public function __construct( DB\SQL $db ){
	  parent::__construct($db,'cat_pais');

  }
  public function all( ){
    	$this->load();
        return $this->query;
  }
  
   public function infoPaises(  )
	  {
		  	  $resultado = array();

			  $sql = "SELECT clave_pais,descripcion FROM cat_pais";
			  $res =  $this->db->exec( $sql );

		      if($res)
		      {
		          foreach( $res as $key => $info)
		          {
		              $resultado[$info['clave_pais']] = $info['descripcion'];
		          }
		      }
		      return $resultado;
	  }
  
}
