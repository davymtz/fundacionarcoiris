<?php
class GraficaModel extends DB\SQL\Mapper
{
  public function __construct( DB\SQL $db ){
	  parent::__construct($db,'cuestionario');

  }

	public function allCuestionario( ){
			$this->load();

				return $this->query;
	}

	public function byIdEdoCuestionario( $idEstado ){
			$this->load( array('localidad_hecho = ?',$idEstado ));

				return $this->query;
	}
	//obtenemos el total de cuestionarios en el año
	public function countCuestionario($year){
		$where = ''; 
		if ( $year == 0)
			$where = ' 1 = 1 ';
		else{
			$where = " YEAR(fecha_hecho)= YEAR(NOW()) ";
		}
		$res  = $this->db->exec("SELECT COUNT(*) AS conteo
		FROM cuestionario
		WHERE $where
			 ");
		if( $res )
		{
		foreach( $res as $key => $info){
			$resultado  = $info['conteo'];
			}
		}
		return $resultado;
	}
	// Obtenemos información pará gráfica de mapas
	public function all(){

		$sqlWhere = '';
		$year     = $_POST["year"];
		$estadoAgresion		= isset($_POST["estado_agresion"])? $_POST["estado_agresion"] :'';
		$tipoIncidente		= isset($_POST["tipo_incidente"])? $_POST["tipo_incidente"] :'';
		if ( $year == 0){
			$sqlWhere .= " 1 = 1 ";
		}else{
			$sqlWhere .= " YEAR(fecha_hecho)= $year ";
		}
		
		if ( $estadoAgresion != '' ){
			$sqlWhere .= " AND localidad_hecho = $estadoAgresion ";
		}
		if ( $tipoIncidente != '' ){
			$sqlWhere .= " AND tipo_crimen = $tipoIncidente ";
		}
		return $this->db->exec("SELECT
		localidad_hecho,tipo_crimen, COUNT( tipo_crimen) AS contador_crimen,estado, latitud, longitud
			FROM cuestionario
			INNER JOIN
				cat_estado ON idestado = localidad_hecho
			WHERE
				 $sqlWhere
			GROUP BY
				localidad_hecho, tipo_crimen,estado, latitud, longitud
			ORDER BY
				tipo_crimen DESC
			");
	}

	public function estadosGrafica(){
		// Variables
		$sqlWhere = '';
		$filtro   = $_POST["filtro"];
		$year     = $_POST["year"];
		$campo    = '';
		$estadoAgresion		= isset($_POST["estado_agresion"])? $_POST["estado_agresion"] :'';
		$tipoIncidente		= isset($_POST["tipo_incidente"])? $_POST["tipo_incidente"] :'';

		// Campo dependiendo el filtro

		switch ($filtro) {
			case 'Orientación Sexual':
				$campo    = 'orientacion_sexual';
				break;
			case 'Por Edad':
				$campo    = 'rango_edad';
				break;
			case 'Por Identidad de Género':
				$campo    = 'identidad_genero';
				break;
			case 'Por Impunidad':
				$campo    = 'situacion_del_caso';
				break;
			case 'Por Migración':
				$campo    = 'situacion_migratoria';
				break;
			case 'Tipo de Arma':
				$campo    = 'tipo_arma';
				break;
			case 'Defensores DH':
				$campo    = 'trabajo_de_defensa';
				break;		

		}

		// Sé construye el where
		if ( $year == 0){
			$sqlWhere .= " 1 = 1 ";
		}else{
			$sqlWhere .= " YEAR(fecha_hecho)= $year ";
		}

		if ( $estadoAgresion != '' ){
			$sqlWhere .= " AND localidad_hecho = $estadoAgresion ";
		}
		if ( $tipoIncidente != '' ){
			$sqlWhere .= " AND tipo_crimen = $tipoIncidente ";
		}

    $consulta = "	SELECT   $campo  as valor, count(tipo_arma)  as cuesti
		FROM cuestionario INNER JOIN cat_estado ON cuestionario.localidad_hecho = cat_estado.idestado 
		WHERE $sqlWhere
		GROUP BY $campo
		ORDER BY  $campo ";

		return $this->db->exec( $consulta );

	}
	/* Mapa con filtro */
	public function mapaFiltro(){
		// Variables
		$sqlWhere = '';
		$filtro   = $_POST["filtro"];
		$year     = $_POST["year"];
		$campo    = '';
		$valorWhere= ''; 
		$arrayTipoArma      = []; 
		$estadoAgresion		= isset($_POST["estado_agresion"])? $_POST["estado_agresion"] :'';
		$tipoIncidente		= isset($_POST["tipo_incidente"])? $_POST["tipo_incidente"] :'';
		$sumatoriaCuesti    = 0; 
		// Campo dependiendo el filtro

		switch ($filtro) {
			case 'Orientación Sexual':
				$campo    = 'orientacion_sexual';
				break;
			case 'Por Edad':
				$campo    = 'rango_edad';
				break;
			case 'Por Identidad de Género':
				$campo    = 'identidad_genero';
				break;
			case 'Por Impunidad':
				$campo    = 'situacion_del_caso';
				break;
			case 'Por Migración':
				$campo    = 'situacion_migratoria';
				break;
			case 'Tipo de Arma':
				$campo    = 'tipo_arma';
				break;
			case 'Defensores DH':
				$campo    = 'trabajo_de_defensa';
				break;	

		}

		// Sé construye el where
		if ( $year == 0){
			$sqlWhere .= " 1 = 1 ";
		}else{
			$sqlWhere .= " YEAR(fecha_hecho)= $year ";
		}
		if ( $estadoAgresion != '' ){
			$sqlWhere .= " AND localidad_hecho = $estadoAgresion ";
		}
		if ( $tipoIncidente != '' ){
			$sqlWhere .= " AND tipo_crimen = $tipoIncidente ";
		}
		if ($campo == 'tipo_arma' ){
			$estadosGrafica  =  $this->estadosGrafica() ;
			$arrayEstadoGraf = []; 
			foreach ($estadosGrafica as $key => $value) {
				$valorTipodeArma    = $value["valor"] ; 
				$cuesti				= $value["cuesti"]; 
				$sumatoriaCuesti    = $sumatoriaCuesti + $cuesti; 
				$arrayValorTipoArma = explode(",", $valorTipodeArma);
				foreach ($arrayValorTipoArma as $valorTipo ){
					if (!in_array ( $valorTipo, $arrayTipoArma  )){
						array_push($arrayTipoArma,$valorTipo ); 
						$consultaTipoAma = $this->tipoArmaEstado($valorTipo ,$sqlWhere,$sumatoriaCuesti );
						array_push( $arrayEstadoGraf, $consultaTipoAma); 
					}
				}
			}  
			return $arrayEstadoGraf; 
		}
		return $this->db->exec("	SELECT
				COUNT($campo )AS contador, $campo as valor, localidad_hecho,latitud, longitud,estado
			FROM
				cuestionario INNER JOIN cat_estado ON idestado = localidad_hecho
			WHERE $sqlWhere
			GROUP BY $campo, localidad_hecho,latitud,longitud
			ORDER BY estado,$campo");

	}
	public function tipoArmaEstado($valorTipo ,$sqlWhere,$cuesti){
		$sqlWhere .= "  AND  FIND_IN_SET ( $valorTipo,tipo_arma) ";

		return $this->db->exec("	SELECT
		COUNT(tipo_arma )AS contador, $valorTipo as valor, localidad_hecho,latitud, longitud,estado, $cuesti as cuesti
			FROM
				cuestionario INNER JOIN cat_estado ON idestado = localidad_hecho
			WHERE $sqlWhere
			GROUP BY  localidad_hecho
			ORDER BY estado,tipo_arma");

	}
	public function tipoCrimenGral(){
		$sqlWhere = '';
		$filtro   = $_POST["filtro"];
		$year     = $_POST["year"];
		$campo    = '';
		$estadoAgresion		= isset($_POST["estado_agresion"])? $_POST["estado_agresion"] :'';
		$tipoIncidente		= isset($_POST["tipo_incidente"])? $_POST["tipo_incidente"] :'';

		// Sé construye el where
		if ( $year == 0){
			$sqlWhere .= " 1 = 1 ";
		}else{
			$sqlWhere .= " YEAR(fecha_hecho)= $year ";
		}
		if ( $estadoAgresion != '' ){
			$sqlWhere .= " AND localidad_hecho = $estadoAgresion ";
		}
		if ( $tipoIncidente != '' ){
			$sqlWhere .= " AND tipo_crimen = $tipoIncidente ";
		}

		return $this->db->exec("SELECT   tipo_crimen
		FROM cuestionario
		WHERE $sqlWhere
		GROUP BY tipo_crimen
		ORDER BY  tipo_crimen");
	}

  public  function valoresGrafica (){

		$value    = $_POST["value"];
		$sqlWhere = '';
		$filtro   = $_POST["filtro"];
		$year     = $_POST["year"];
		$campo    = '';
		$estadoAgresion		= isset($_POST["estado_agresion"])? $_POST["estado_agresion"] :'';
		$tipoIncidente		= isset($_POST["tipo_incidente"])? $_POST["tipo_incidente"] :'';

		// Sé construye el where
		if ( $year == 0){
			$sqlWhere .= " 1 = 1 ";
		}else{
			$sqlWhere .= " YEAR(fecha_hecho)= $year ";
		}
		if ( $estadoAgresion != '' ){
			$sqlWhere .= " AND localidad_hecho = $estadoAgresion ";
		}
		if ( $tipoIncidente != '' ){
			$sqlWhere .= " AND tipo_crimen = $tipoIncidente ";
		}


		if ( $filtro == ''  ){
			$sqlWhere .= "AND tipo_crimen = $value";
			return $this->db->exec("SELECT IFNULL(contador,0) AS contador,  IFNULL(tabla.mes,0) AS mes   FROM mes_grafica m LEFT JOIN

			( SELECT  COUNT(tipo_crimen)AS contador,  MONTH(fecha_hecho) AS mes
			FROM  cuestionario  inner join cat_estado  on cuestionario.localidad_hecho = cat_estado.idestado
			WHERE $sqlWhere
			GROUP BY tipo_crimen,MONTH(fecha_hecho)
			ORDER BY MONTH(fecha_hecho) ASC, tipo_crimen) tabla ON m.mes = tabla.mes ORDER BY m.mes ");
		}else{

			switch ($filtro) {
				case 'Orientación Sexual':
					$campo    = 'orientacion_sexual';
					break;
				case 'Por Edad':
					$campo    = 'rango_edad';
					break;
				case 'Por Identidad de Género':
					$campo    = 'identidad_genero';
					break;
				case 'Por Impunidad':
					$campo    = 'situacion_del_caso';
					break;
				case 'Tipo de Arma':
					$campo    = 'tipo_arma';
					break;
				case 'Por Migración':
					$campo    = 'situacion_migratoria';
					break;
			}
		
		if ( $campo == 'tipo_arma'){
			$sqlWhere .= "  AND FIND_IN_SET ($value,$campo )  ";
			
			$consulta = "SELECT IFNULL(contador,0) AS contador,  IFNULL(tabla.mes,0) AS mes   FROM mes_grafica m LEFT JOIN
			( SELECT  COUNT($campo )AS contador,  MONTH(fecha_hecho) AS mes
					FROM  cuestionario inner join cat_estado  on cuestionario.localidad_hecho = cat_estado.idestado
					WHERE $sqlWhere
					GROUP BY MONTH(fecha_hecho)
					ORDER BY MONTH(fecha_hecho) ASC, $campo ) tabla ON m.mes = tabla.mes ORDER BY m.mes";
		}else{
			$sqlWhere .= " AND $campo in ( $value) ";
			$consulta = "SELECT IFNULL(contador,0) AS contador,  IFNULL(tabla.mes,0) AS mes   FROM mes_grafica m LEFT JOIN
			( SELECT  COUNT($campo )AS contador,  MONTH(fecha_hecho) AS mes
					FROM  cuestionario inner join cat_estado  on cuestionario.localidad_hecho = cat_estado.idestado
					WHERE $sqlWhere
					GROUP BY $campo, MONTH(fecha_hecho)
					ORDER BY MONTH(fecha_hecho) ASC, $campo ) tabla ON m.mes = tabla.mes ORDER BY m.mes";
		}
		//secho $consulta; exit; 
			return $this->db->exec($consulta);

		}
	}
}
